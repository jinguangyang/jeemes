package org.jeecg.common.util;

import cn.hutool.core.util.IdUtil;
import lombok.extern.log4j.Log4j2;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataBom;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataBomitem;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataMateriel;
import org.jeecg.modules.mes.order.entity.ExpApplyBill;
import org.jeecg.modules.mes.order.entity.ImpApplyBill;

import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Log4j2
public class ApplyBillDBUtil {

    private static final String driverName="net.sourceforge.jtds.jdbc.Driver";//驱动
    private static final String url="jdbc:jtds:sqlserver://192.168.10.234/idb";//连接SQL数据库信息及服务器IP信息
    private static final String username="sa";//用户名
    private static final String password = "yzy20170907";//密码

    /**
     * 进口申请单插入数据库
     * @param bean
     */
    public static void insertSqlServer(ImpApplyBill bean) {
        try {
            try {
                // 加载sqlserver的驱动类
                Class.forName(driverName);
                Connection conn = DriverManager.getConnection(url, username, password);
                System.out.print("insertSqlServer 连接数据库成功");
                conn.setAutoCommit(false);
                Statement stmt = conn.createStatement();
                String sql = "";
                PreparedStatement ps = null;
                sql = "INSERT INTO ImpApplyBillDBTemp (id,invoiceNo,imgExgFlag,itemNo,material,optLock,sectionNo,companyCode,uploadDate,price,amount,curr,companyMergerKey,invoiceDate,palletNum,pcs,wrap,containerNo,qty,netWeight,grossWeight,scmCoc) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
                ps = conn.prepareStatement(sql);
                ps.setString(1,bean.getGwId() );//id
                ps.setString(2, bean.getInvoiceNo());//进货单号
                ps.setString(3, bean.getImgExgFlag());//物料标记
                ps.setInt(4, bean.getItemNo());//项号
                ps.setString(5, bean.getMaterial());//料号
                ps.setInt(6, bean.getOptLock());//乐观锁
                ps.setString(7, bean.getSectionNo());//批次号
                ps.setString(8, bean.getCompanyCode());//公司编码
                DateFormat df2 = DateFormat.getDateTimeInstance();
                ps.setString(9, df2.format(bean.getUploadDate()));//上传时间
                ps.setString(10, bean.getPrice().toString());//企业单件
                ps.setString(11, bean.getAmount().toString());//企业总价
                ps.setString(12, bean.getCurr());//币制
                ps.setString(13,bean.getCompanyMergerKey());//企业关键合并key
                ps.setString(14, bean.getInvoiceDate());//票据日期
                ps.setDouble(15, bean.getPalletNum());//板数
                ps.setString(16, bean.getPcs().toString());//件数
                ps.setString(17, bean.getWrap());//包装种类
                ps.setString(18, bean.getContainerNo());//箱号
                ps.setDouble(19, bean.getQty());//企业数量
                ps.setDouble(20, bean.getNetWeight());//净重
                ps.setDouble(21, bean.getGrossWeight());//毛重
                ps.setString(22, bean.getScmCoc());//客户/供应商
                ps.executeUpdate();
                conn.commit();

                if (ps != null) { // 关闭声明
                    try {
                        ps.close();
                    } catch (SQLException e) {
//						 e.printStackTrace();
                        log.error(e.getMessage(),e);
                    }
                }
                if (stmt != null) { // 关闭声明
                    try {
                        stmt.close();
                    } catch (SQLException e) {
//										 e.printStackTrace();
                        log.error(e.getMessage(),e);
                    }
                }
                if (conn != null) { // 关闭连接对象
                    try {
                        conn.close();
                    } catch (SQLException e) {
//						e.printStackTrace();
                        log.error(e.getMessage(),e);
                    }
                }
            } catch (SQLException se) {
                log.error(se.getMessage(),se);
            }
        } catch (ClassNotFoundException e) {
            log.error(e.getMessage(),e);
        }
    }
    /**
     * 查询sqlserver 进口单单号
     * @param impApplyBill
     * @return
     */
    public static List<ImpApplyBill> queryNum(ImpApplyBill impApplyBill){
        List<ImpApplyBill> list = new ArrayList<>();
        try
        {
            Class.forName(driverName);//启动驱动
            Connection conn=DriverManager.getConnection(url, username, password);//连接数据库
            String sql="select * from ImpApplyBillDBTemp where invoiceNo='"+impApplyBill.getInvoiceNo()+"'";
            System.out.print("queryNum 连接数据库成功! sql:"+sql);
            PreparedStatement statement=null;
            statement=conn.prepareStatement(sql);
            ResultSet res=null;
            res=statement.executeQuery();
            while(res.next()){
                ImpApplyBill bean = new ImpApplyBill();
                bean.setInvoiceNo(res.getString("invoiceNo"));//进货单号
                bean.setImgExgFlag(res.getString("imgExgFlag"));//物料标记
                bean.setItemNo(res.getInt("itemNo"));//项号
                bean.setMaterial(res.getString("material"));//料号
                bean.setGwId(res.getString("id"));//id
                bean.setOptLock(res.getInt("optLock"));//乐观锁
                bean.setSectionNo(res.getString("sectionNo"));//批次号
                bean.setCompanyCode(res.getString("companyCode"));//公司编码
                bean.setUploadDate(res.getDate("uploadDate"));//上传时间
                bean.setPrice(res.getDouble("price"));//企业单价
                bean.setAmount(res.getDouble("amount"));//企业总价
                bean.setCurr(res.getString("curr"));//币制
                bean.setCompanyMergerKey(res.getString("companyMergerKey"));//企业关键合并key
                bean.setInvoiceDate(res.getString("invoiceDate"));//票据日期
                bean.setPalletNum(res.getInt("palletNum"));//板数
                bean.setPcs(res.getInt("pcs"));//件数
                bean.setWrap(res.getString("wrap"));//包装种类
                bean.setContainerNo(res.getString("containerNo"));//箱号
                bean.setQty(res.getDouble("qty"));//企业数量
                bean.setNetWeight(res.getDouble("netWeight"));//净重
                bean.setGrossWeight(res.getDouble("grossWeight"));//毛重
                bean.setScmCoc(res.getString("scmCoc"));//客户/供应商
                list.add(bean);
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
            System.out.print("连接失败");
        }
        return list;
    }

    /**
     * 出口申请单插入数据库
     * @param bean
     */
    public static void insertSqlServer(ExpApplyBill bean) {
        try {

            try {
                // 加载sqlserver的驱动类
                Class.forName(driverName);
                Connection conn = DriverManager.getConnection(url, username, password);
                System.out.print("insertSqlServer 连接数据库成功");
                conn.setAutoCommit(false);
                Statement stmt = conn.createStatement();
                String sql = "";
                PreparedStatement ps = null;
                sql = "INSERT INTO ExpApplyBillDBTemp (id,outBillNo,imgExgFlag,itemNo,material,optLock,sectionNo,companyCode,uploadDate,price,amount,curr,companyMergerKey,invoiceDate,palletNum,pcs,wrap,containerNo,qty,netWeight,grossWeight,scmCoc,zextendItemField1,destinationCountry) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
                ps = conn.prepareStatement(sql);
                ps.setString(1,bean.getGwId() );//id
                ps.setString(2, bean.getOutBillNo());//出货单号
                ps.setString(3, bean.getImgExgFlag());//物料标记
                ps.setInt(4, bean.getItemNo());//项号
                ps.setString(5, bean.getMaterial());//料号
                ps.setInt(6, bean.getOptLock());//乐观锁
                ps.setString(7, bean.getSectionNo());//批次号
                ps.setString(8, bean.getCompanyCode());//公司编码
                DateFormat df2 = DateFormat.getDateTimeInstance();
                ps.setString(9, df2.format(bean.getUploadDate()));//上传时间
                ps.setString(10, bean.getPrice().toString());//企业单件
                ps.setString(11, bean.getAmount().toString());//企业总价
                ps.setString(12, bean.getCurr());//币制
                ps.setString(13,bean.getCompanyMergerKey());//企业关键合并key
                ps.setString(14, bean.getInvoiceDate());//票据日期
                ps.setDouble(15, bean.getPalletNum());//板数
                ps.setString(16, bean.getPcs().toString());//件数
                ps.setString(17, bean.getWrap());//包装种类
                ps.setString(18, bean.getContainerNo());//箱号
                ps.setDouble(19, bean.getQty());//企业数量
                ps.setDouble(20, bean.getNetWeight());//净重
                ps.setDouble(21, bean.getGrossWeight());//毛重
                ps.setString(22, bean.getScmCoc());//客户/供应商
                ps.setString(23, bean.getZextendItemField1());//立方数
                ps.setString(24, bean.getDestinationCountry());//目的国
                ps.executeUpdate();
                conn.commit();

                if (ps != null) { // 关闭声明
                    try {
                        ps.close();
                    } catch (SQLException e) {
//						 e.printStackTrace();
                        log.error(e.getMessage(),e);
                    }
                }
                if (stmt != null) { // 关闭声明
                    try {
                        stmt.close();
                    } catch (SQLException e) {
//										 e.printStackTrace();
                        log.error(e.getMessage(),e);
                    }
                }
                if (conn != null) { // 关闭连接对象
                    try {
                        conn.close();
                    } catch (SQLException e) {
//						e.printStackTrace();
                        log.error(e.getMessage(),e);
                    }
                }
            } catch (SQLException se) {
                log.error(se.getMessage(),se);
            }
        } catch (ClassNotFoundException e) {
            log.error(e.getMessage(),e);
        }
    }

    /**
     * 查询sqlserver 出口单单号
     * @param expApplyBill
     * @return
     */
    public static List<ExpApplyBill> queryNum(ExpApplyBill expApplyBill){
        List<ExpApplyBill> list = new ArrayList<>();
        try
        {
            Class.forName(driverName);//启动驱动
            Connection conn=DriverManager.getConnection(url, username, password);//连接数据库
            String sql="select * from ExpApplyBillDBTemp where invoiceNo='"+expApplyBill.getOutBillNo()+"'";
            System.out.print("queryNum 连接数据库成功! sql:"+sql);
            PreparedStatement statement=null;
            statement=conn.prepareStatement(sql);
            ResultSet res=null;
            res=statement.executeQuery();
            while(res.next()){
                ExpApplyBill bean = new ExpApplyBill();
                bean.setOutBillNo(res.getString("outBillNo"));//进货单号
                bean.setImgExgFlag(res.getString("imgExgFlag"));//物料标记
                bean.setItemNo(res.getInt("itemNo"));//项号
                bean.setMaterial(res.getString("material"));//料号
                bean.setGwId(res.getString("id"));//id
                bean.setOptLock(res.getInt("optLock"));//乐观锁
                bean.setSectionNo(res.getString("sectionNo"));//批次号
                bean.setCompanyCode(res.getString("companyCode"));//公司编码
                bean.setUploadDate(res.getDate("uploadDate"));//上传时间
                bean.setPrice(res.getDouble("price"));//企业单价
                bean.setAmount(res.getDouble("amount"));//企业总价
                bean.setCurr(res.getString("curr"));//币制
                bean.setCompanyMergerKey(res.getString("companyMergerKey"));//企业关键合并key
                bean.setInvoiceDate(res.getString("invoiceDate"));//票据日期
                bean.setPalletNum(res.getInt("palletNum"));//板数
                bean.setPcs(res.getInt("pcs"));//件数
                bean.setWrap(res.getString("wrap"));//包装种类
                bean.setContainerNo(res.getString("containerNo"));//箱号
                bean.setQty(res.getDouble("qty"));//企业数量
                bean.setNetWeight(res.getDouble("netWeight"));//净重
                bean.setGrossWeight(res.getDouble("grossWeight"));//毛重
                bean.setScmCoc(res.getString("scmCoc"));//客户/供应商
                bean.setZextendItemField1(res.getString("zextendItemField1"));//立方数
                bean.setDestinationCountry(res.getString("destinationCountry"));//目的国
                list.add(bean);
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
            System.out.print("连接失败");
        }
        return list;
    }

    /**
     * bom插入数据库
     * @param bomitem bom子项目
     */
    public static Result<?> insertBOMSqlServer(MesChiefdataBom bom, MesChiefdataBomitem bomitem) {
        try {

            try {
                Class.forName(driverName);//启动驱动
                Connection conn=DriverManager.getConnection(url, username, password);//连接数据库
                conn.setAutoCommit(false);
                Statement stmt = conn.createStatement();
                String sql = "";
                PreparedStatement ps = null;
                sql = "INSERT INTO CoBomDBTemp (id,materialType,exgPtNo,bomVersion,imgPtNo,unitConsume,wastRate,intangibleRate,expiryDate,optLock,sectionNo,companyCode,uploadDate) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?);";
                ps = conn.prepareStatement(sql);
                ps.setString(1, IdUtil.simpleUUID());//id
                ps.setString(2, bom.getMaterialType());//类别
                ps.setString(3, bom.getMachinesortCode());//成品料号
                ps.setString(4, bom.getEdition());//版本
                ps.setString(5, bomitem.getMaterielCode());//料件料号
                ps.setString(6, bomitem.getQuantity());//净耗=用量
                ps.setString(7, bomitem.getWastageRate());//有形损耗率
                ps.setString(8, bomitem.getIntangibleRate());//无形损耗率
                ps.setString(9, bom.getExpiryDate());//BOM有效期
                ps.setInt(10, 0);//乐观锁
                SimpleDateFormat Dateformat = new SimpleDateFormat("yyyyMMdd");
                String dateTime = Dateformat.format(new Date());
                ps.setString(11, dateTime + "001");//批次号
                ps.setString(12, "500666002U");//公司编码
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date = formatter.format(new java.util.Date());
                ps.setString(13, date);//上传时间
                ps.executeUpdate();
                conn.commit();

                if (ps != null) { // 关闭声明
                    try {
                        ps.close();
                    } catch (SQLException e) {
//										e.printStackTrace();
                        log.error(e.getMessage(), e);
                    }
                }
                if (stmt != null) { // 关闭声明
                    try {
                        stmt.close();
                    } catch (SQLException e) {
//										e.printStackTrace();
                        log.error(e.getMessage(), e);
                    }
                }
                if (conn != null) { // 关闭连接对象
                    try {
                        conn.close();
                    } catch (SQLException e) {
//										e.printStackTrace();
                        log.error(e.getMessage(), e);
                    }
                }

                System.err.println("BOM数据导入完成---"+bomitem.getMaterielCode());
                log.info("BOM数据导入完成---{}", bomitem.getMaterielCode());
                return Result.OK();
            } catch (SQLException se) {
//								se.printStackTrace();
                log.error(se.getMessage(), se);
                return Result.error("关务系统连接失败！请联系管理员检查！");
            }
        } catch (ClassNotFoundException e) {
            log.error(e.getMessage(), e);
            return Result.error("找不到驱动程序类 ，加载驱动失败！");
        }
    }

    /**
     * 物料主表插入数据库
     * @param materiel bom子项目
     */
    public static Result<?> importMaterielSqlServer(MesChiefdataMateriel materiel) {
        try {
            try {
                Class.forName(driverName);//启动驱动
                Connection conn=DriverManager.getConnection(url, username, password);//连接数据库
                conn.setAutoCommit(false);
                Statement stmt = conn.createStatement();
                String sql = "";
                PreparedStatement ps = null;
                sql = "INSERT INTO MaterialDBTemp (id,materialType,ptNo,ptName,ptNameModel,ptUnit,unitWeight,optLock,sectionNo,companyCode,uploadDate) VALUES (?,?,?,?,?,?,?,?,?,?,?);";
                ps = conn.prepareStatement(sql);
                ps.setString(1,IdUtil.simpleUUID());//id
                ps.setString(2, materiel.getMaterielType());//类别
                ps.setString(3, materiel.getMaterielCode());//料号
                ps.setString(4, materiel.getProductName());//货物名称
                ps.setString(5, materiel.getGauge());//货物规格型号
                ps.setString(6, materiel.getUnit());//企业单位
                ps.setString(7, materiel.getUnitWeight());//单重
                ps.setInt(8, 0);//乐观锁
                SimpleDateFormat Dateformat = new SimpleDateFormat("yyyyMMdd");
                String dateTime = Dateformat.format(new Date());
                ps.setString(9, dateTime+"001");//批次号
                ps.setString(10, "500666002U");//公司编码
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String date = formatter.format(new java.util.Date());
                ps.setString(11,date);//上传时间
                ps.executeUpdate();
                conn.commit();

                if (ps != null) { // 关闭声明
                    try {
                        ps.close();
                    } catch (SQLException e) {
                        log.error(e.getMessage(),e);
//								 e.printStackTrace();
                    }
                }
                if (stmt != null) { // 关闭声明
                    try {
                        stmt.close();
                    } catch (SQLException e) {
                        log.error(e.getMessage(),e);
//								 e.printStackTrace();
                    }
                }
                if (conn != null) { // 关闭连接对象
                    try {
                        conn.close();
                    } catch (SQLException e) {
                        log.error(e.getMessage(),e);
//								 e.printStackTrace();
                    }
                }
                System.err.println("物料主表数据导入完成---"+materiel.getMaterielCode());
                log.info("物料主表数据导入完成---{}", materiel.getMaterielCode());
                return Result.OK();
            } catch (SQLException se) {
                log.error(se.getMessage(),se);
//						 se.printStackTrace();
                return Result.error("关务系统连接失败！请联系管理员检查！");
            }
        } catch (ClassNotFoundException e) {
            return Result.error("找不到驱动程序类 ，加载驱动失败！");
        }
    }
}
