package org.jeecg.modules.mes.produce.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description: mes_file_collection_aoi
 * @Author: jeecg-boot
 * @Date:   2021-04-19
 * @Version: V1.0
 */
@Data
@TableName("mes_file_collection_aoi")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_file_collection_aoi对象", description="mes_file_collection_aoi")
public class MesFileCollectionAoi implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private String sysOrgCode;
    /**产线*/
    @Excel(name = "产线", width = 15)
    @ApiModelProperty(value = "产线")
    private String line;
	/**PCB编号*/
	@Excel(name = "PCB编号", width = 15)
    @ApiModelProperty(value = "PCB编号")
    private String pcbNo;
	/**产品名称*/
	@Excel(name = "产品名称", width = 15)
    @ApiModelProperty(value = "产品名称")
    private String produceName;
	/**测试用时*/
	@Excel(name = "测试用时", width = 15)
    @ApiModelProperty(value = "测试用时")
    private String testTime;
	/**面*/
	@Excel(name = "面", width = 15)
    @ApiModelProperty(value = "面")
    private String tbFace;
	/**测试结果*/
	@Excel(name = "测试结果", width = 15)
    @ApiModelProperty(value = "测试结果")
    private String testResult;
	/**机器编号*/
	@Excel(name = "机器编号", width = 15)
    @ApiModelProperty(value = "机器编号")
    private String machineNo;
	/**条码/二维码*/
	@Excel(name = "条码/二维码", width = 15)
    @ApiModelProperty(value = "条码/二维码")
    private String machineCode;
	/**PCB Ng总数*/
	@Excel(name = "PCB Ng总数", width = 15)
    @ApiModelProperty(value = "PCB Ng总数")
    private String pcbNgNumber;
	/**误判NG数量*/
	@Excel(name = "误判NG数量", width = 15)
    @ApiModelProperty(value = "误判NG数量")
    private String errNgNumber;
	/**操作员*/
	@Excel(name = "操作员", width = 15)
    @ApiModelProperty(value = "操作员")
    private String optPerson;
	/**班次*/
	@Excel(name = "班次", width = 15)
    @ApiModelProperty(value = "班次")
    private String orderClass;
	/**线别*/
	@Excel(name = "线别", width = 15)
    @ApiModelProperty(value = "线别")
    private String lineType;
	/**元件数量*/
	@Excel(name = "元件数量", width = 15)
    @ApiModelProperty(value = "元件数量")
    private String compoNumber;
	/**检测点数*/
	@Excel(name = "检测点数", width = 15)
    @ApiModelProperty(value = "检测点数")
    private String testNumber;
	/**制令单号*/
	@Excel(name = "制令单号", width = 15)
    @ApiModelProperty(value = "制令单号")
    private String query1;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
    @ApiModelProperty(value = "备用2")
    private String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
    @ApiModelProperty(value = "备用3")
    private String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
    @ApiModelProperty(value = "备用4")
    private String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
    @ApiModelProperty(value = "备用5")
    private String query5;
}
