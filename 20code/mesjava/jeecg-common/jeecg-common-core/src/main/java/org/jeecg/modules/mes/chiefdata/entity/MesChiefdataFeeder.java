package org.jeecg.modules.mes.chiefdata.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 主数据—FEEDER建档
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Data
@TableName("mes_chiefdata_feeder")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_chiefdata_feeder对象", description="主数据—FEEDER建档")
public class MesChiefdataFeeder implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**新增方式*/
	@Excel(name = "新增方式", width = 15, dicCode = "gain_manner")
	@Dict(dicCode = "gain_manner")
    @ApiModelProperty(value = "新增方式")
    private java.lang.String gainManner;
	/**FEEDER SN*/
	@Excel(name = "FEEDER SN", width = 15)
    @ApiModelProperty(value = "FEEDER SN")
    private java.lang.String feederSn;
	/**批次数量*/
	@Excel(name = "批次数量", width = 15)
    @ApiModelProperty(value = "批次数量")
    private java.lang.String batchNum;
	/**FEEDER规格*/
	@Excel(name = "FEEDER规格", width = 15)
    @ApiModelProperty(value = "FEEDER规格")
    private java.lang.String feederGague;
	/**FEEDER类型*/
	@Excel(name = "FEEDER类型", width = 15, dicCode = "feeder_type")
	@Dict(dicCode = "feeder_type")
    @ApiModelProperty(value = "FEEDER类型")
    private java.lang.String feederType;
	/**FEEDER ID*/
	@Excel(name = "FEEDER ID", width = 15)
    @ApiModelProperty(value = "FEEDER 名称")
    private java.lang.String feederId;
	/**左(单)通道SN*/
	@Excel(name = "左(单)通道SN", width = 15)
    @ApiModelProperty(value = "左(单)通道SN")
    private java.lang.String leftSnpassage;
	/**右通道SN*/
	@Excel(name = "右通道SN", width = 15)
    @ApiModelProperty(value = "右通道SN")
    private java.lang.String rightSnpassage;
	/**中通道SN*/
	@Excel(name = "中通道SN", width = 15)
    @ApiModelProperty(value = "中通道SN")
    private java.lang.String middleSnpassage;
	/**使用点数上限*/
	@Excel(name = "使用点数上限", width = 15)
    @ApiModelProperty(value = "使用点数上限")
    private java.lang.String pointnumLimit;
	/**保养点数*/
	@Excel(name = "保养点数", width = 15)
    @ApiModelProperty(value = "保养点数")
    private java.lang.String maintainPointnum;
	/**提醒点数*/
	@Excel(name = "提醒点数", width = 15)
    @ApiModelProperty(value = "提醒点数")
    private java.lang.String remindPointnum;
	/**使用周期上限*/
	@Excel(name = "使用周期上限", width = 15)
    @ApiModelProperty(value = "使用周期上限")
    private java.lang.String cycleLimit;
	/**保养周期*/
	@Excel(name = "保养周期", width = 15)
    @ApiModelProperty(value = "保养周期")
    private java.lang.String maintainCycle;
	/**提醒天数*/
	@Excel(name = "提醒天数", width = 15)
    @ApiModelProperty(value = "提醒天数")
    private java.lang.String remindDays;
	/**当前使用点数*/
	@Excel(name = "当前使用点数", width = 15)
    @ApiModelProperty(value = "当前使用点数")
    private java.lang.String query1;
	/**最近保养日期*/
	@Excel(name = "最近保养日期", width = 15)
    @ApiModelProperty(value = "最近保养日期")
    private java.lang.String query2;
	/**保养人*/
	@Excel(name = "保养人", width = 15)
    @ApiModelProperty(value = "保养人")
    private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
    @ApiModelProperty(value = "备用4")
    private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
    @ApiModelProperty(value = "备用5")
    private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
    @ApiModelProperty(value = "备用6")
    private java.lang.String query6;
}
