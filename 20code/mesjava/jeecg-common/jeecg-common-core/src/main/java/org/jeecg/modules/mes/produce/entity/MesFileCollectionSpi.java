package org.jeecg.modules.mes.produce.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: mes_file_collection_spi
 * @Author: jeecg-boot
 * @Date:   2021-04-16
 * @Version: V1.0
 */
@Data
@TableName("mes_file_collection_spi")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_file_collection_spi对象", description="mes_file_collection_spi")
public class MesFileCollectionSpi implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private String sysOrgCode;
    /**产线*/
    @Excel(name = "产线", width = 15)
    @ApiModelProperty(value = "产线")
    private String line;
	/**机种名*/
	@Excel(name = "机种名", width = 15)
    @ApiModelProperty(value = "机种名")
    private String machineName;
	/**条码*/
	@Excel(name = "条码", width = 15)
    @ApiModelProperty(value = "条码")
    private String code;
	/**组别，线别*/
	@Excel(name = "组别，线别", width = 15)
    @ApiModelProperty(value = "组别，线别")
    private String groupLine;
	/**noMean*/
	@Excel(name = "noMean", width = 15)
    @ApiModelProperty(value = "noMean")
    private String noMean;
    /**型号*/
    @Excel(name = "型号", width = 15)
    @ApiModelProperty(value = "型号")
    private String machineType;
	/**批次工单*/
	@Excel(name = "批次工单", width = 15)
    @ApiModelProperty(value = "批次工单")
    private String workOrder;
	/**测试时间(年、月、日)*/
	@Excel(name = "测试时间(年、月、日)", width = 15)
    @ApiModelProperty(value = "测试时间(年、月、日)")
    private String testTimeDay;
	/**测试时间(时、分、秒)*/
	@Excel(name = "测试时间(时、分、秒)", width = 15)
    @ApiModelProperty(value = "测试时间(时、分、秒)")
    private String testTimeHour;
	/**测试结果*/
	@Excel(name = "测试结果", width = 15)
    @ApiModelProperty(value = "测试结果")
    private String testResult;
	/**T面(分T/B面)*/
	@Excel(name = "T面(分T/B面)", width = 15)
    @ApiModelProperty(value = "T面(分T/B面)")
    private String tbFace;
	/**测试点数*/
	@Excel(name = "测试点数", width = 15)
    @ApiModelProperty(value = "测试点数")
    private String testNum;
	/**实际不良数*/
	@Excel(name = "实际不良数", width = 15)
    @ApiModelProperty(value = "实际不良数")
    private String badNum;
	/**备用1*/
	@Excel(name = "备用1", width = 15)
    @ApiModelProperty(value = "备用1")
    private String attr1;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
    @ApiModelProperty(value = "备用2")
    private String attr2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
    @ApiModelProperty(value = "备用3")
    private String attr3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
    @ApiModelProperty(value = "备用4")
    private String attr4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
    @ApiModelProperty(value = "备用5")
    private String attr5;
}
