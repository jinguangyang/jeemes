package org.jeecg.modules.mes.order.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

/**
 * @Description: 进口申请单子表
 * @Author: jeecg-boot
 * @Date:   2021-04-07
 * @Version: V1.0
 */
@Data
@TableName("imp_apply_bill")
@ApiModel(value="imp_apply_main对象", description="进口申请单主表")
public class ImpApplyBill implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ID_WORKER_STR)
	@ApiModelProperty(value = "主键")
	private java.lang.String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "创建日期")
	private java.util.Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "更新日期")
	private java.util.Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private java.lang.String sysOrgCode;
	/**进货单号*/
	@Excel(name = "进货单号", width = 15)
	@ApiModelProperty(value = "进货单号")
	private java.lang.String invoiceNo;
	/**物料标记*/
	@Excel(name = "物料标记", width = 15)
	@ApiModelProperty(value = "物料标记")
	private java.lang.String imgExgFlag;
	/**项号*/
	@Excel(name = "项号", width = 15)
	@ApiModelProperty(value = "项号")
	private java.lang.Integer itemNo;
	/**品名*/
	@Excel(name = "品名", width = 15)
	@ApiModelProperty(value = "品名")
	private java.lang.String materialName;
	/**料号*/
	@Excel(name = "料号", width = 15)
	@ApiModelProperty(value = "料号")
	private java.lang.String material;
	/**规格*/
	@Excel(name = "规格", width = 15)
	@ApiModelProperty(value = "规格")
	private java.lang.String materialGuige;
	/**批次号*/
	@Excel(name = "批次号", width = 15)
	@ApiModelProperty(value = "批次号")
	private java.lang.String sectionNo;
	/**公司编码*/
	@Excel(name = "公司编码", width = 15)
	@ApiModelProperty(value = "公司编码")
	private java.lang.String companyCode;
	/**上传时间*/
	@Excel(name = "上传时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "上传时间")
	private java.util.Date uploadDate;
	/**企业单价*/
	@Excel(name = "企业单价", width = 15)
	@ApiModelProperty(value = "企业单价")
	private java.lang.Double price;
	/**企业总价*/
	@Excel(name = "企业总价", width = 15)
	@ApiModelProperty(value = "企业总价")
	private java.lang.Double amount;
	/**币制*/
	@Excel(name = "币制", width = 15)
	@ApiModelProperty(value = "币制")
	private java.lang.String curr;
	/**企业关键合并key*/
	@Excel(name = "企业关键合并key", width = 15)
	@ApiModelProperty(value = "企业关键合并key")
	private java.lang.String companyMergerKey;
	/**票据日期*/
	@Excel(name = "票据日期", width = 15)
	@ApiModelProperty(value = "票据日期")
	private java.lang.String invoiceDate;
	/**板数*/
	@Excel(name = "板数", width = 15)
	@ApiModelProperty(value = "板数")
	private java.lang.Integer palletNum;
	/**件数*/
	@Excel(name = "件数", width = 15)
	@ApiModelProperty(value = "件数")
	private java.lang.Integer pcs;
	/**包装种类*/
	@Excel(name = "包装种类", width = 15)
	@ApiModelProperty(value = "包装种类")
	private java.lang.String wrap;
	/**箱号*/
	@Excel(name = "箱号", width = 15)
	@ApiModelProperty(value = "箱号")
	private java.lang.String containerNo;
	/**企业数量*/
	@Excel(name = "企业数量", width = 15)
	@ApiModelProperty(value = "企业数量")
	private java.lang.Double qty;
	/**净重*/
	@Excel(name = "净重", width = 15)
	@ApiModelProperty(value = "净重")
	private java.lang.Double netWeight;
	/**毛重*/
	@Excel(name = "毛重", width = 15)
	@ApiModelProperty(value = "毛重")
	private java.lang.Double grossWeight;
	/**客户/供应商*/
	@Excel(name = "客户/供应商", width = 15)
	@ApiModelProperty(value = "客户/供应商")
	private java.lang.String scmCoc;
	/**上传状态*/
	@Excel(name = "上传状态", width = 15)
	@ApiModelProperty(value = "上传状态")
	private java.lang.String impApplyState;
	/**关联id*/
	@ApiModelProperty(value = "关联id")
	private java.lang.String impMainId;
	/**关务id*/
	@Excel(name = "关务id", width = 15)
	@ApiModelProperty(value = "关务id")
	private java.lang.String gwId;
	/**乐观锁*/
	@Excel(name = "乐观锁", width = 15)
	@ApiModelProperty(value = "乐观锁")
	private java.lang.Integer optLock;
	/**客户料号*/
	@Excel(name = "客户料号", width = 15)
	@ApiModelProperty(value = "客户料号")
	private java.lang.String clientCode;
	/**商品编码*/
	@Excel(name = "商品编码", width = 15)
	@ApiModelProperty(value = "商品编码")
	private java.lang.String productCode;
}
