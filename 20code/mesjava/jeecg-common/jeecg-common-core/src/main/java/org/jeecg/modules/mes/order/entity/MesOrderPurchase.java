package org.jeecg.modules.mes.order.entity;

import java.io.Serializable;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import org.jeecgframework.poi.excel.annotation.Excel;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 订单管理—采购订单
 * @Author: jeecg-boot
 * @Date:   2020-11-11
 * @Version: V1.0
 */
@Data
@TableName("mes_order_purchase")
@ApiModel(value="mes_order_purchase对象", description="订单管理—采购订单")
public class MesOrderPurchase implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**订单编号*/
    @Excel(name = "订单编号", width = 15)
    @ApiModelProperty(value = "订单编号")
    private java.lang.String orderCode;
	/**订单名称*/
    @Excel(name = "订单名称", width = 15)
    @ApiModelProperty(value = "订单名称")
    private java.lang.String orderName;
	/**订单类型*/
    @Excel(name = "订单类型", width = 15)
    @ApiModelProperty(value = "订单类型")
    private java.lang.String orderType;
	/**供应商id*/
    @Excel(name = "供应商id", width = 15)
    @ApiModelProperty(value = "供应商id")
    private java.lang.String supplierId;
	/**供应商编号*/
    @Excel(name = "供应商编号", width = 15)
    @ApiModelProperty(value = "供应商编号")
    private java.lang.String supplierCode;
	/**供应商名称*/
    @Excel(name = "供应商名称", width = 15)
    @ApiModelProperty(value = "供应商名称")
    private java.lang.String supplierName;
	/**凭证日期*/
    @Excel(name = "凭证日期", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "凭证日期")
    private java.util.Date verifyDate;
	/**公司id*/
    @Excel(name = "公司id", width = 15)
    @ApiModelProperty(value = "公司id")
    private java.lang.String companyId;
	/**公司编号*/
    @Excel(name = "公司编号", width = 15)
    @ApiModelProperty(value = "公司编号")
    private java.lang.String companyCode;
	/**公司名称*/
    @Excel(name = "公司名称", width = 15)
    @ApiModelProperty(value = "公司名称")
    private java.lang.String companyName;
	/**付款条款*/
    @Excel(name = "付款条款", width = 15)
    @ApiModelProperty(value = "付款条款")
    private java.lang.String payItem;
	/**货币*/
    @Excel(name = "货币", width = 15)
    @ApiModelProperty(value = "货币")
    private java.lang.String currency;
	/**汇率*/
    @Excel(name = "汇率", width = 15)
    @ApiModelProperty(value = "汇率")
    private java.lang.String exchangeRate;
	/**国际贸易条件*/
    @Excel(name = "国际贸易条件", width = 15)
    @ApiModelProperty(value = "国际贸易条件")
    private java.lang.String tradeCondition;
	/**街道/门牌号*/
    @Excel(name = "街道/门牌号", width = 15)
    @ApiModelProperty(value = "街道/门牌号")
    private java.lang.String receiveAddress;
	/**电话*/
    @Excel(name = "电话", width = 15)
    @ApiModelProperty(value = "电话")
    private java.lang.String phone;
	/**备注*/
    @Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private java.lang.String notes;
	/**是否收货完成*/
    @Excel(name = "是否收货完成", width = 15)
    @ApiModelProperty(value = "是否收货完成")
    private java.lang.String ifFinish;
	/**备用3*/
    @Excel(name = "备用3", width = 15)
    @ApiModelProperty(value = "备用3")
    private java.lang.String query3;
	/**备用4*/
    @Excel(name = "备用4", width = 15)
    @ApiModelProperty(value = "备用4")
    private java.lang.String query4;
    /**是否报关（默认为0）*/
    @Excel(name = "是否报关（默认为0）", width = 15)
    @ApiModelProperty(value = "是否报关（默认为0）")
    private java.lang.String importState;
	/**总价*/
    @Excel(name = "总价", width = 15)
    @ApiModelProperty(value = "总价")
    private java.lang.String totalPrice;

    /**物料id*/
//    @Excel(name = "物料id", width = 15)
    @ApiModelProperty(value = "物料id")
    @TableField(exist = false)
    private java.lang.String materielId;
    /**物料编号*/
//    @Excel(name = "物料编号", width = 15)
    @ApiModelProperty(value = "物料编号")
    @TableField(exist = false)
    private java.lang.String materielCode;
    /**物料名称*/
//    @Excel(name = "物料名称", width = 15)
    @ApiModelProperty(value = "物料名称")
    @TableField(exist = false)
    private java.lang.String materielName;
    /**单位*/
//    @Excel(name = "单位", width = 15)
    @ApiModelProperty(value = "单位")
    @TableField(exist = false)
    private java.lang.String unit;
    /**规格*/
//    @Excel(name = "规格", width = 15)
    @ApiModelProperty(value = "规格")
    @TableField(exist = false)
    private java.lang.String gauge;
    /**总数量*/
//    @Excel(name = "总数量", width = 15)
    @ApiModelProperty(value = "总数量")
    @TableField(exist = false)
    private java.lang.String grossAccount;
}
