package org.jeecg.modules.mes.produce.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: mes_file_collection_tpj_head
 * @Author: jeecg-boot
 * @Date:   2021-04-19
 * @Version: V1.0
 */
@Data
@TableName("mes_file_collection_tpj_head")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_file_collection_tpj_head对象", description="mes_file_collection_tpj_head")
public class MesFileCollectionTpjHead implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private String sysOrgCode;
    /**产线*/
    @Excel(name = "产线", width = 15)
    @ApiModelProperty(value = "产线")
    private String line;
	/**主表id*/
	@Excel(name = "主表id", width = 15)
    @ApiModelProperty(value = "主表id")
    private String mainId;
	/**站点id*/
	@Excel(name = "站点id", width = 15)
    @ApiModelProperty(value = "站点id")
    private String stationId;
	/**传送通道id*/
	@Excel(name = "传送通道id", width = 15)
    @ApiModelProperty(value = "传送通道id")
    private String cnvrLaneId;
	/**headNo*/
	@Excel(name = "headNo", width = 15)
    @ApiModelProperty(value = "headNo")
    private String headNo;
	/**headId*/
	@Excel(name = "headId", width = 15)
    @ApiModelProperty(value = "headId")
    private String headId;
	/**吸取成功数*/
	@Excel(name = "吸取成功数", width = 15)
    @ApiModelProperty(value = "吸取成功数")
    private String pickedNum;
	/**贴片成功数*/
	@Excel(name = "贴片成功数", width = 15)
    @ApiModelProperty(value = "贴片成功数")
    private String placedNum;
	/**吸取错误次数*/
	@Excel(name = "吸取错误次数", width = 15)
    @ApiModelProperty(value = "吸取错误次数")
    private String pickErrorNum;
	/**元件落下次数*/
	@Excel(name = "元件落下次数", width = 15)
    @ApiModelProperty(value = "元件落下次数")
    private String fallCompoNum;
	/**元件带回次数*/
	@Excel(name = "元件带回次数", width = 15)
    @ApiModelProperty(value = "元件带回次数")
    private String takeoutCompoNum;
	/**激光错误次数*/
	@Excel(name = "激光错误次数", width = 15)
    @ApiModelProperty(value = "激光错误次数")
    private String laserErrorNum;
	/**异元件错误次数*/
	@Excel(name = "异元件错误次数", width = 15)
    @ApiModelProperty(value = "异元件错误次数")
    private String diffCompoErrorNum;
	/**芯片立起错误次数*/
	@Excel(name = "芯片立起错误次数", width = 15)
    @ApiModelProperty(value = "芯片立起错误次数")
    private String tombErrorNum;
	/**元件姿态错误次数*/
	@Excel(name = "元件姿态错误次数", width = 15)
    @ApiModelProperty(value = "元件姿态错误次数")
    private String posCompoNum;
	/**元件角度异常次数*/
	@Excel(name = "元件角度异常次数", width = 15)
    @ApiModelProperty(value = "元件角度异常次数")
    private String angleCompoNum;
	/**元件用尽次数*/
	@Excel(name = "元件用尽次数", width = 15)
    @ApiModelProperty(value = "元件用尽次数")
    private String noCompoNum;
	/**重试超过次数*/
	@Excel(name = "重试超过次数", width = 15)
    @ApiModelProperty(value = "重试超过次数")
    private String pickRetryOverNum;
	/**图像识别错误次数*/
	@Excel(name = "图像识别错误次数", width = 15)
    @ApiModelProperty(value = "图像识别错误次数")
    private String visionErrorNum;
	/**共面性错误次数*/
	@Excel(name = "共面性错误次数", width = 15)
    @ApiModelProperty(value = "共面性错误次数")
    private String coplaErrorNum;
	/**校验错误次数*/
	@Excel(name = "校验错误次数", width = 15)
    @ApiModelProperty(value = "校验错误次数")
    private String verifyErrorNum;
	/**引脚玩弯曲检测次数*/
	@Excel(name = "引脚玩弯曲检测次数", width = 15)
    @ApiModelProperty(value = "引脚玩弯曲检测次数")
    private String leadBendErrorNum;
	/**吸取位置偏移次数*/
	@Excel(name = "吸取位置偏移次数", width = 15)
    @ApiModelProperty(value = "吸取位置偏移次数")
    private String pickPosErrorNum;
	/**元件废弃次数*/
	@Excel(name = "元件废弃次数", width = 15)
    @ApiModelProperty(value = "元件废弃次数")
    private String scrapNum;
	/**其他错误次数*/
	@Excel(name = "其他错误次数", width = 15)
    @ApiModelProperty(value = "其他错误次数")
    private String otherErrorNum;
	/**headDeviceType*/
	@Excel(name = "headDeviceType", width = 15)
    @ApiModelProperty(value = "headDeviceType")
    private String headDeviceType;
}
