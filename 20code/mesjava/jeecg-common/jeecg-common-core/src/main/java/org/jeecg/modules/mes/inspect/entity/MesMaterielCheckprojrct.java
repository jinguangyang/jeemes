package org.jeecg.modules.mes.inspect.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.jeecg.common.aspect.annotation.Dict;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

/**
 * @Description: 质检中心-物料检测项目
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
@Data
@TableName("mes_materiel_checkprojrct")
@ApiModel(value="mes_materiel_checkprojrct对象", description="质检中心-物料检测项目")
public class MesMaterielCheckprojrct implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**新增方式*/
    @Excel(name = "新增方式", width = 15, dicCode = "gain_manner")
    @Dict(dicCode = "gain_manner")
    @ApiModelProperty(value = "新增方式")
    private java.lang.String newgainManner;
	/**检测阶别*/
    @Excel(name = "检测阶别", width = 15, dicCode = "inspect_grade")
    @Dict(dicCode = "inspect_grade")
    @ApiModelProperty(value = "检测阶别")
    private java.lang.String checkGrade;
	/**机种/物料*/
    @Excel(name = "机种/物料", width = 15)
    @ApiModelProperty(value = "机种/物料")
    private java.lang.String matOrMac;
	/**物料类别*/
    @Excel(name = "物料类别", width = 15)
    @ApiModelProperty(value = "物料类别")
    private java.lang.String materielGrade;
	/**检测类型*/
    @Excel(name = "检测类型", width = 15)
    @ApiModelProperty(value = "检测类型")
    private java.lang.String checkType;
	/**机种名称*/
    @Excel(name = "机种名称", width = 15)
    @ApiModelProperty(value = "机种名称")
    private java.lang.String machineName;
	/**机种规格*/
    @Excel(name = "机种规格", width = 15)
    @ApiModelProperty(value = "机种规格")
    private java.lang.String machineGague;
	/**备用1*/
    @Excel(name = "备用1", width = 15)
    @ApiModelProperty(value = "备用1")
    private java.lang.String query1;
	/**备用2*/
    @Excel(name = "备用2", width = 15)
    @ApiModelProperty(value = "备用2")
    private java.lang.String query2;
	/**备用3*/
    @Excel(name = "备用3", width = 15)
    @ApiModelProperty(value = "备用3")
    private java.lang.String query3;
	/**备用4*/
    @Excel(name = "备用4", width = 15)
    @ApiModelProperty(value = "备用4")
    private java.lang.String query4;
	/**备用5*/
    @Excel(name = "备用5", width = 15)
    @ApiModelProperty(value = "备用5")
    private java.lang.String query5;
	/**备用6*/
    @Excel(name = "备用6", width = 15)
    @ApiModelProperty(value = "备用6")
    private java.lang.String query6;
}
