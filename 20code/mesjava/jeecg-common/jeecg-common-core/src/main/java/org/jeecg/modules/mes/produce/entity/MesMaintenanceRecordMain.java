package org.jeecg.modules.mes.produce.entity;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 维护保养点检记录主表
 * @Author: jeecg-boot
 * @Date:   2021-06-10
 * @Version: V1.0
 */
@Data
@TableName("mes_maintenance_record_main")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_maintenance_record_main对象", description="维护保养点检记录主表")
public class MesMaintenanceRecordMain implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**表类型*/
	@Excel(name = "表类型", width = 15)
    @ApiModelProperty(value = "表类型")
    private java.lang.String tableType;
	/**表名*/
	@Excel(name = "表名", width = 15)
    @ApiModelProperty(value = "表名")
    private java.lang.String tableName;
	/**检查日期*/
	@Excel(name = "检查日期", width = 15, format = "yyyy-MM")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM")
    @DateTimeFormat(pattern="yyyy-MM")
    @ApiModelProperty(value = "检查日期")
    private java.util.Date inspectionDate;
	/**线别*/
	@Excel(name = "线别", width = 15, dicCode = "mgroup_code")
	@Dict(dicCode = "mgroup_code")
    @ApiModelProperty(value = "线别")
    private java.lang.String lineType;
	/**设备名称/检查项目*/
	@Excel(name = "设备名称/检查项目", width = 15)
    @ApiModelProperty(value = "设备名称/检查项目")
    private java.lang.String equipmentName;
	/**设备型号/检查位置*/
	@Excel(name = "设备型号/检查位置", width = 15)
    @ApiModelProperty(value = "设备型号/检查位置")
    private java.lang.String equipmentModel;
	/**购置日期/启动检查时间*/
	@Excel(name = "购置日期/启动检查时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "购置日期/启动检查时间")
    private java.util.Date datePurchase;
	/**文件编号/资产编码*/
	@Excel(name = "文件编号/资产编码", width = 15)
    @ApiModelProperty(value = "文件编号/资产编码")
    private java.lang.String documentNumber;
	/**检查方法*/
	@Excel(name = "检查方法", width = 15)
    @ApiModelProperty(value = "检查方法")
    private java.lang.String inspectionMethod;
	/**检查周期*/
	@Excel(name = "检查周期", width = 15)
    @ApiModelProperty(value = "检查周期")
    private java.lang.String inspectionCycle;
	/**计入方式*/
	@Excel(name = "计入方式", width = 15)
    @ApiModelProperty(value = "计入方式")
    private java.lang.String accountingMethod;
	/**注意事项*/
	@Excel(name = "注意事项", width = 15)
    @ApiModelProperty(value = "注意事项")
    private java.lang.String beCareful;
	/**设备SN*/
	@Excel(name = "设备SN", width = 15)
    @ApiModelProperty(value = "设备SN")
    private java.lang.String equipmentSn;
	/**表类型编码*/
	@Excel(name = "表类型编码", width = 15, dicCode = "facility_temp_type")
	@Dict(dicCode = "facility_temp_type")
    @ApiModelProperty(value = "表类型编码")
    private java.lang.String tableTypeCode;
	/**表名称编码*/
	@Excel(name = "表名称编码", width = 15, dicCode = "facility_name_temp")
	@Dict(dicCode = "facility_name_temp")
    @ApiModelProperty(value = "表名称编码")
    private java.lang.String tableNameCode;
	/**保养人*/
	@Excel(name = "保养人", width = 15)
    @ApiModelProperty(value = "保养人")
    private java.lang.String maintainer;
	/**稽查人*/
	@Excel(name = "稽查人", width = 15)
    @ApiModelProperty(value = "稽查人")
    private java.lang.String reviewer;
	/**备用1*/
	@Excel(name = "备用1", width = 15)
    @ApiModelProperty(value = "备用1")
    private java.lang.String query1;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
    @ApiModelProperty(value = "备用2")
    private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
    @ApiModelProperty(value = "备用3")
    private java.lang.String query3;

    /**日保养记录*/
    @TableField(exist = false)
    private Map<String, List<MesMaintenanceRecordItem>> dayList;

    /**周保养记录*/
    @TableField(exist = false)
    private Map<String, List<MesMaintenanceRecordItem>> weekList;

    /**月保养记录*/
    @TableField(exist = false)
    private Map<String, List<MesMaintenanceRecordItem>> monthList;
}
