package org.jeecg.modules.mes.chiefdata.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 主数据—时序备料
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Data
@TableName("mes_chiefdata_sparematerial")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_chiefdata_sparematerial对象", description="主数据—时序备料")
public class MesChiefdataSparematerial implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**制令单号*/
	@Excel(name = "制令单号", width = 15)
    @ApiModelProperty(value = "制令单号")
    private java.lang.String commandCode;
	/**机种*/
	@Excel(name = "机种", width = 15)
    @ApiModelProperty(value = "机种")
    private java.lang.String machineSort;
	/**机种名称*/
	@Excel(name = "机种名称", width = 15)
    @ApiModelProperty(value = "机种名称")
    private java.lang.String machineName;
	/**机种规格*/
	@Excel(name = "机种规格", width = 15)
    @ApiModelProperty(value = "机种规格")
    private java.lang.String machineGague;
	/**计划数量*/
	@Excel(name = "计划数量", width = 15)
    @ApiModelProperty(value = "计划数量")
    private java.lang.String planNum;
	/**累积备料数*/
	@Excel(name = "累积备料数", width = 15)
    @ApiModelProperty(value = "累积备料数")
    private java.lang.String rackupSparenum;
	/**物料间隔(H)*/
	@Excel(name = "物料间隔(H)", width = 15)
    @ApiModelProperty(value = "物料间隔(H)")
    private java.lang.String materielSpan;
	/**当前备料时间*/
	@Excel(name = "当前备料时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "当前备料时间")
    private java.util.Date currentSparetime;
	/**标准工时(S)*/
	@Excel(name = "标准工时(S)", width = 15)
    @ApiModelProperty(value = "标准工时(S)")
    private java.lang.String standardTime;
	/**当前备料数*/
	@Excel(name = "当前备料数", width = 15)
    @ApiModelProperty(value = "当前备料数")
    private java.lang.String currentSparenum;
	/**状态*/
	@Excel(name = "状态", width = 15)
    @ApiModelProperty(value = "状态")
    private java.lang.String state;
	/**生产面别*/
	@Excel(name = "生产面别", width = 15)
    @ApiModelProperty(value = "生产面别")
    private java.lang.String produceFace;
	/**下次备料数*/
	@Excel(name = "下次备料数", width = 15)
    @ApiModelProperty(value = "下次备料数")
    private java.lang.String nextSparenum;
	/**下次备料时间*/
	@Excel(name = "下次备料时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "下次备料时间")
    private java.util.Date nextSparetime;
	/**备注*/
	@Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private java.lang.String notes;
	/**备用1*/
	@Excel(name = "备用1", width = 15)
    @ApiModelProperty(value = "备用1")
    private java.lang.String query1;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
    @ApiModelProperty(value = "备用2")
    private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
    @ApiModelProperty(value = "备用3")
    private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
    @ApiModelProperty(value = "备用4")
    private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
    @ApiModelProperty(value = "备用5")
    private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
    @ApiModelProperty(value = "备用6")
    private java.lang.String query6;
}
