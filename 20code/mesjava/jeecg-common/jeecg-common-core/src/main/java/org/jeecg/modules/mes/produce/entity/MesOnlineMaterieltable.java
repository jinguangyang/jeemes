package org.jeecg.modules.mes.produce.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description:  制造中心-在线料表-子表
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Data
@TableName("mes_online_materieltable")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_online_materieltable对象", description="制造中心-在线料表-子表")
public class MesOnlineMaterieltable implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**线别*/
	@Excel(name = "线别", width = 15)
    @ApiModelProperty(value = "线别")
    private java.lang.String lineType;
    /**成品料号*/
    @Excel(name = "成品料号", width = 15)
    @ApiModelProperty(value = "成品料号")
    private java.lang.String productCode;
    /**成品名称*/
    @Excel(name = "成品名称", width = 15)
    @ApiModelProperty(value = "成品名称")
    private java.lang.String productName;
	/**物料料号*/
	@Excel(name = "物料料号", width = 15)
    @ApiModelProperty(value = "物料料号")
    private java.lang.String materielCode;
	/**物料名称*/
	@Excel(name = "物料名称", width = 15)
    @ApiModelProperty(value = "物料名称")
    private java.lang.String materielName;
	/**物料规格*/
	@Excel(name = "物料规格", width = 15)
    @ApiModelProperty(value = "物料规格")
    private java.lang.String materielGague;
    /**通道*/
    @Excel(name = "通道", width = 15)
    @ApiModelProperty(value = "通道")
    private java.lang.String passage;
    /**点数*/
    @Excel(name = "点数", width = 15)
    @ApiModelProperty(value = "点数")
    private java.lang.String pointNum;
    /**正面/反面*/
    @Excel(name = "正面/反面", width = 15)
    @ApiModelProperty(value = "正面/反面")
    private java.lang.String query5;
	/**设备序号*/
	@Excel(name = "设备序号", width = 15)
    @ApiModelProperty(value = "设备序号")
    private java.lang.String deviceSerial;
	/**TABLE*/
	@Excel(name = "TABLE", width = 15)
    @ApiModelProperty(value = "TABLE")
    private java.lang.String materielTable;
	/**料站*/
	@Excel(name = "料站", width = 15)
    @ApiModelProperty(value = "料站")
    private java.lang.String materielStation;
	/**点位*/
	@Excel(name = "点位", width = 15)
    @ApiModelProperty(value = "点位")
    private java.lang.String pointLocation;
	/**TRAY盘物料*/
	@Excel(name = "TRAY盘物料", width = 15, dicCode = "yn")
	@Dict(dicCode = "yn")
    @ApiModelProperty(value = "TRAY盘物料")
    private java.lang.String trayPlate;
	/**跳过标志*/
	@Excel(name = "跳过标志", width = 15, dicCode = "yn")
	@Dict(dicCode = "yn")
    @ApiModelProperty(value = "跳过标志")
    private java.lang.String skipToken;
	/**轨道*/
	@Excel(name = "轨道", width = 15)
    @ApiModelProperty(value = "轨道")
    private java.lang.String track;
	/**自制条码*/
	@Excel(name = "自制条码", width = 15)
    @ApiModelProperty(value = "自制条码")
    private java.lang.String selfBarcode;
	/**货架编码*/
	@Excel(name = "货架编码", width = 15)
    @ApiModelProperty(value = "货架编码")
    private java.lang.String shelfCode;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
    @ApiModelProperty(value = "备用6")
    private java.lang.String query6;
    /**主表ID*/
	@ApiModelProperty(value = "主表ID")
	private java.lang.String mainId;
}
