package org.jeecg.modules.mes.produce.vo;

import java.util.List;
import org.jeecg.modules.mes.produce.entity.MesBackupReceiveinfo;
import org.jeecg.modules.mes.produce.entity.MesBackupReceiveitem;
import lombok.Data;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelEntity;
import org.jeecgframework.poi.excel.annotation.ExcelCollection;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 备品领用-基本信息
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Data
@ApiModel(value="mes_backup_receiveinfoPage对象", description="备品领用-基本信息")
public class MesBackupReceiveinfoPage {

	/**id*/
	@ApiModelProperty(value = "id")
	private java.lang.String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "创建日期")
	private java.util.Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "更新日期")
	private java.util.Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private java.lang.String sysOrgCode;
	/**领用单号*/
	@Excel(name = "领用单号", width = 15)
	@ApiModelProperty(value = "领用单号")
	private java.lang.String receiveCode;
	/**领用类型*/
	@Excel(name = "领用类型", width = 15)
	@ApiModelProperty(value = "领用类型")
	private java.lang.String receiveType;
	/**领用部门*/
	@Excel(name = "领用部门", width = 15)
	@ApiModelProperty(value = "领用部门")
	private java.lang.String receiveDepart;
	/**领用人*/
	@Excel(name = "领用人", width = 15)
	@ApiModelProperty(value = "领用人")
	private java.lang.String receivePerson;
	/**领用备注*/
	@Excel(name = "领用备注", width = 15)
	@ApiModelProperty(value = "领用备注")
	private java.lang.String receiveNote;
	/**备用1*/
	@Excel(name = "备用1", width = 15)
	@ApiModelProperty(value = "备用1")
	private java.lang.String query1;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
	@ApiModelProperty(value = "备用2")
	private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
	@ApiModelProperty(value = "备用3")
	private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
	@ApiModelProperty(value = "备用4")
	private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
	@ApiModelProperty(value = "备用5")
	private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
	@ApiModelProperty(value = "备用6")
	private java.lang.String query6;
	
	@ExcelCollection(name="备品领用-明细信息")
	@ApiModelProperty(value = "备品领用-明细信息")
	private List<MesBackupReceiveitem> mesBackupReceiveitemList;
	
}
