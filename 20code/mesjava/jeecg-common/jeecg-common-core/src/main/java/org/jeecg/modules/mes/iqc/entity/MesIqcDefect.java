package org.jeecg.modules.mes.iqc.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 来料检验缺陷描述
 * @Author: jeecg-boot
 * @Date:   2021-03-19
 * @Version: V1.0
 */
@Data
@TableName("mes_iqc_defect")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_iqc_defect对象", description="来料检验缺陷描述")
public class MesIqcDefect implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**iqc物料id*/
	@Excel(name = "iqc物料id", width = 15)
    @ApiModelProperty(value = "iqc物料id")
    private java.lang.String iqcMaterialId;
	/**类型名称*/
	@Excel(name = "类型名称", width = 15)
    @ApiModelProperty(value = "类型名称")
    private java.lang.String defectTypeName;
	/**缺陷描述*/
	@Excel(name = "缺陷描述", width = 15)
    @ApiModelProperty(value = "缺陷描述")
    private java.lang.String defectDesc;
	/**不良品数量*/
	@Excel(name = "不良品数量", width = 15)
    @ApiModelProperty(value = "不良品数量")
    private java.lang.String defectNum;
	/**不良品-致命*/
	@Excel(name = "不良品-致命", width = 15)
    @ApiModelProperty(value = "不良品-致命")
    private java.lang.String rejectsNum1;
	/**不良品-严重*/
	@Excel(name = "不良品-严重", width = 15)
    @ApiModelProperty(value = "不良品-严重")
    private java.lang.String rejectsNum2;
	/**不良品-轻微*/
	@Excel(name = "不良品-轻微", width = 15)
    @ApiModelProperty(value = "不良品-轻微")
    private java.lang.String rejectsNum3;

    /**所属部门*/
    @ApiModelProperty(value = "所属部门")
    @Excel(name = "所属部门", width = 15)
    private java.lang.String sysOrgCode;
}
