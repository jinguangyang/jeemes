package org.jeecg.modules.mes.produce.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: DIP首件确认表-检查内容
 * @Author: jeecg-boot
 * @Date:   2021-03-23
 * @Version: V1.0
 */
@Data
@TableName("first_piece_dip_log")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="first_piece_dip_log对象", description="DIP首件确认表-检查内容")
public class FirstPieceDipLog implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**DIP首件确认表id*/
	@Excel(name = "DIP首件确认表id", width = 15)
    @ApiModelProperty(value = "DIP首件确认表id")
    private java.lang.String firstId;
	/**检查内容*/
	@Excel(name = "检查内容", width = 15)
    @ApiModelProperty(value = "检查内容")
    private java.lang.String firstContent;
	/**内容类型内容类型(1：前3条, 2：插件位,3:插件后两条4:波峰状况 5:夹具 6:补焊 7:洗板、压接 8:装配 9、其他 10、新增项)"*/
	@Excel(name = "内容类型(1：前3条, 2：插件位,3:插件后两条4:波峰状况5:夹具6:补焊7:其他8:新增项)", width = 15)
    @ApiModelProperty(value = "内容类型(1：前3条, 2：插件位,3:插件后两条4:波峰状况 5:夹具 6:补焊 7:洗板、压接 8:装配 9、其他 10、新增项、11 确认项目)")
    private java.lang.String contentType;
	/**备用1*/
	@Excel(name = "备用1", width = 15)
    @ApiModelProperty(value = "类型1~10使用 1、3、5、7、9为是否确定 2、4、6、8、10 为是否合格 11为具体的输入内容")
    private java.lang.String query1;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
    @ApiModelProperty(value = "类型6、8使用 6为温度要求 8为力矩要求 ")
    private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
    @ApiModelProperty(value = "类型6、8使用 6为温度实测 8为力矩实测")
    private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
    @ApiModelProperty(value = "类型6使用 6为漏电值")
    private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
    @ApiModelProperty(value = "备用5")
    private java.lang.String query5;
}
