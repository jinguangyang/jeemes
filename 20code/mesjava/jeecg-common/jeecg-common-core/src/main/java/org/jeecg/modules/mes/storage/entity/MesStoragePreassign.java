package org.jeecg.modules.mes.storage.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 仓库管理—预配明细
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Data
@TableName("mes_storage_preassign")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_storage_preassign对象", description="仓库管理—预配明细")
public class MesStoragePreassign implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**预配ID*/
	@Excel(name = "预配ID", width = 15)
    @ApiModelProperty(value = "预配ID")
    private java.lang.String preassignId;
	/**波次单号*/
	@Excel(name = "波次单号", width = 15)
    @ApiModelProperty(value = "波次单号")
    private java.lang.String waveCode;
	/**出库单号*/
	@Excel(name = "出库单号", width = 15)
    @ApiModelProperty(value = "出库单号")
    private java.lang.String outwareCode;
	/**出库行号*/
	@Excel(name = "出库行号", width = 15)
    @ApiModelProperty(value = "出库行号")
    private java.lang.String outwareRownum;
	/**产品编号*/
	@Excel(name = "产品编号", width = 15)
    @ApiModelProperty(value = "产品编号")
    private java.lang.String productCode;
	/**产品名称*/
	@Excel(name = "产品名称", width = 15)
    @ApiModelProperty(value = "产品名称")
    private java.lang.String productName;
	/**货主编号*/
	@Excel(name = "货主编号", width = 15)
    @ApiModelProperty(value = "货主编号")
    private java.lang.String shipperCode;
	/**批号*/
	@Excel(name = "批号", width = 15)
    @ApiModelProperty(value = "批号")
    private java.lang.String wholeNum;
	/**包装编码*/
	@Excel(name = "包装编码", width = 15)
    @ApiModelProperty(value = "包装编码")
    private java.lang.String parcelCode;
	/**包装单位*/
	@Excel(name = "包装单位", width = 15)
    @ApiModelProperty(value = "包装单位")
    private java.lang.String parcelUnit;
	/**预配数量*/
	@Excel(name = "预配数量", width = 15)
    @ApiModelProperty(value = "预配数量")
    private java.lang.String preassignNum;
	/**单位*/
	@Excel(name = "单位", width = 15)
    @ApiModelProperty(value = "单位")
    private java.lang.String unit;
	/**区域号*/
	@Excel(name = "区域号", width = 15)
    @ApiModelProperty(value = "区域号")
    private java.lang.String regionNum;
	/**库位号*/
	@Excel(name = "库位号", width = 15)
    @ApiModelProperty(value = "库位号")
    private java.lang.String wareSitenum;
	/**仓库编号*/
	@Excel(name = "仓库编号", width = 15)
    @ApiModelProperty(value = "仓库编号")
    private java.lang.String wareCode;
	/**备用1*/
	@Excel(name = "备用1", width = 15)
    @ApiModelProperty(value = "备用1")
    private java.lang.String query1;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
    @ApiModelProperty(value = "备用2")
    private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
    @ApiModelProperty(value = "备用3")
    private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
    @ApiModelProperty(value = "备用4")
    private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
    @ApiModelProperty(value = "备用5")
    private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
    @ApiModelProperty(value = "备用6")
    private java.lang.String query6;
}
