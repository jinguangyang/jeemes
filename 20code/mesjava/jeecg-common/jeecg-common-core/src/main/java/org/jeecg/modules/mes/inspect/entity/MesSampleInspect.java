package org.jeecg.modules.mes.inspect.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 品质报表—抽检
 * @Author: jeecg-boot
 * @Date:   2021-01-20
 * @Version: V1.0
 */
@Data
@TableName("mes_sample_inspect")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_sample_inspect对象", description="品质报表—抽检")
public class MesSampleInspect implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**序号*/
	@Excel(name = "序号", width = 15)
    @ApiModelProperty(value = "序号")
    private java.lang.String sequence;
	/**客户*/
	@Excel(name = "客户", width = 15)
    @ApiModelProperty(value = "客户")
    private java.lang.String client;
	/**日期*/
	@Excel(name = "日期", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "日期")
    private java.util.Date checkDate;
	/**班别*/
	@Excel(name = "班别", width = 15)
    @ApiModelProperty(value = "班别")
    private java.lang.String classSort;
	/**工序*/
	@Excel(name = "工序", width = 15)
    @ApiModelProperty(value = "工序")
    private java.lang.String produceProcedure;
	/**机型*/
	@Excel(name = "机型", width = 15)
    @ApiModelProperty(value = "机型")
    private java.lang.String machineSize;
	/**线别*/
	@Excel(name = "线别", width = 15)
    @ApiModelProperty(value = "线别")
    private java.lang.String lineSort;
	/**站别*/
	@Excel(name = "站别", width = 15)
    @ApiModelProperty(value = "站别")
    private java.lang.String stationSort;
	/**抽检数量*/
	@Excel(name = "抽检数量", width = 15)
    @ApiModelProperty(value = "抽检数量")
    private java.lang.String sampleNum;
	/**抽检结果*/
	@Excel(name = "抽检结果", width = 15)
    @ApiModelProperty(value = "抽检结果")
    private java.lang.String sampleResult;
	/**FQC抽检人员*/
	@Excel(name = "FQC抽检人员", width = 15)
    @ApiModelProperty(value = "FQC抽检人员")
    private java.lang.String fqcPerson;
	/**抽检不良数量*/
	@Excel(name = "抽检不良数量", width = 15)
    @ApiModelProperty(value = "抽检不良数量")
    private java.lang.String badNum;
	/**不良条码*/
	@Excel(name = "不良条码", width = 15)
    @ApiModelProperty(value = "不良条码")
    private java.lang.String badBarcode;
	/**处理结果*/
	@Excel(name = "处理结果", width = 15)
    @ApiModelProperty(value = "处理结果")
    private java.lang.String dealResult;
	/**备注*/
	@Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private java.lang.String notes;
	/**备用1*/
	@Excel(name = "备用1", width = 15)
    @ApiModelProperty(value = "备用1")
    private java.lang.String query1;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
    @ApiModelProperty(value = "备用2")
    private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
    @ApiModelProperty(value = "备用3")
    private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
    @ApiModelProperty(value = "备用4")
    private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
    @ApiModelProperty(value = "备用5")
    private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
    @ApiModelProperty(value = "备用6")
    private java.lang.String query6;
}
