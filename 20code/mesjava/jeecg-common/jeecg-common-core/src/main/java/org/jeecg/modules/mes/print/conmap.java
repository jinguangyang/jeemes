package org.jeecg.modules.mes.print;

public class conmap {
    private String conType;
    private String conKey;
    private String conValue;

    public String getConType() {
        return conType;
    }

    public void setConType(String conType) {
        this.conType = conType;
    }

    public String getConKey() {
        return conKey;
    }

    public void setConKey(String conKey) {
        this.conKey = conKey;
    }

    public String getConValue() {
        return conValue;
    }

    public void setConValue(String conValue) {
        this.conValue = conValue;
    }
}
