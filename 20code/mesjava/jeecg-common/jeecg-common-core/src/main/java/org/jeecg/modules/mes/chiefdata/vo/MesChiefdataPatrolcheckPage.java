package org.jeecg.modules.mes.chiefdata.vo;

import java.util.List;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataPatrolcheck;
import org.jeecg.modules.mes.chiefdata.entity.MesPatrolcheckTimespan;
import lombok.Data;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelEntity;
import org.jeecgframework.poi.excel.annotation.ExcelCollection;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 主数据—巡检方案
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Data
@ApiModel(value="mes_chiefdata_patrolcheckPage对象", description="主数据—巡检方案")
public class MesChiefdataPatrolcheckPage {

	/**id*/
	@ApiModelProperty(value = "id")
	private java.lang.String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "创建日期")
	private java.util.Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "更新日期")
	private java.util.Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private java.lang.String sysOrgCode;
	/**方案名称*/
	@Excel(name = "方案名称", width = 15)
	@ApiModelProperty(value = "方案名称")
	private java.lang.String mannerName;
	/**默认标志*/
	@Excel(name = "默认标志", width = 15)
	@ApiModelProperty(value = "默认标志")
	private java.lang.String defaultToken;
	/**换班巡检*/
	@Excel(name = "换班巡检", width = 15)
	@ApiModelProperty(value = "换班巡检")
	private java.lang.String changeClass;
	/**巡检模式*/
	@Excel(name = "巡检模式", width = 15)
	@ApiModelProperty(value = "巡检模式")
	private java.lang.String patrolModel;
	/**提前预警时间(分钟)*/
	@Excel(name = "提前预警时间(分钟)", width = 15)
	@ApiModelProperty(value = "提前预警时间(分钟)")
	private java.lang.String preWarntime;
	/**巡检方式*/
	@Excel(name = "巡检方式", width = 15)
	@ApiModelProperty(value = "巡检方式")
	private java.lang.String patrolManner;
	/**投产巡检时间(分钟)*/
	@Excel(name = "投产巡检时间(分钟)", width = 15)
	@ApiModelProperty(value = "投产巡检时间(分钟)")
	private java.lang.String pitchChecktime;
	/**备用1*/
	@Excel(name = "备用1", width = 15)
	@ApiModelProperty(value = "备用1")
	private java.lang.String query1;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
	@ApiModelProperty(value = "备用2")
	private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
	@ApiModelProperty(value = "备用3")
	private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
	@ApiModelProperty(value = "备用4")
	private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
	@ApiModelProperty(value = "备用5")
	private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
	@ApiModelProperty(value = "备用6")
	private java.lang.String query6;
	
	@ExcelCollection(name="主数据—巡检方案时段")
	@ApiModelProperty(value = "主数据—巡检方案时段")
	private List<MesPatrolcheckTimespan> mesPatrolcheckTimespanList;
	
}
