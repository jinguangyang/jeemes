package org.jeecg.modules.mes.chiefdata.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 主数据—商品
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Data
@TableName("mes_chiefdata_product")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_chiefdata_product对象", description="主数据—商品")
public class MesChiefdataProduct implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**商品编码*/
	@Excel(name = "商品编码", width = 15)
    @ApiModelProperty(value = "商品编码")
    private java.lang.String productCode;
	/**商品名称*/
	@Excel(name = "商品名称", width = 15)
    @ApiModelProperty(value = "商品名称")
    private java.lang.String productName;
	/**配送点*/
	@Excel(name = "配送点", width = 15)
    @ApiModelProperty(value = "配送点")
    private java.lang.String deliverSite;
	/**英文名称*/
	@Excel(name = "英文名称", width = 15)
    @ApiModelProperty(value = "英文名称")
    private java.lang.String englishName;
	/**日文名称*/
	@Excel(name = "日文名称", width = 15)
    @ApiModelProperty(value = "日文名称")
    private java.lang.String japaneseName;
	/**客户商品编码*/
	@Excel(name = "客户商品编码", width = 15)
    @ApiModelProperty(value = "客户商品编码")
    private java.lang.String clientProductcode;
	/**商品规格*/
	@Excel(name = "商品规格", width = 15)
    @ApiModelProperty(value = "商品规格")
    private java.lang.String productSize;
	/**产品属性*/
	@Excel(name = "产品属性", width = 15)
    @ApiModelProperty(value = "产品属性")
    private java.lang.String productAttribute;
	/**存放温层*/
	@Excel(name = "存放温层", width = 15)
    @ApiModelProperty(value = "存放温层")
    private java.lang.String lodgeLayer;
	/**拆零控制*/
	@Excel(name = "拆零控制", width = 15)
    @ApiModelProperty(value = "拆零控制")
    private java.lang.String defuseControl;
	/**码盘单层数量*/
	@Excel(name = "码盘单层数量", width = 15)
    @ApiModelProperty(value = "码盘单层数量")
    private java.lang.String plateSinglenum;
	/**码盘层高*/
	@Excel(name = "码盘层高", width = 15)
    @ApiModelProperty(value = "码盘层高")
    private java.lang.String plateHeight;
	/**计费商品类*/
	@Excel(name = "计费商品类", width = 15)
    @ApiModelProperty(value = "计费商品类")
    private java.lang.String payType;
	/**商品条码*/
	@Excel(name = "商品条码", width = 15)
    @ApiModelProperty(value = "商品条码")
    private java.lang.String productBarcode;
	/**商品品牌*/
	@Excel(name = "商品品牌", width = 15)
    @ApiModelProperty(value = "商品品牌")
    private java.lang.String productBrand;
	/**保质期*/
	@Excel(name = "保质期", width = 15)
    @ApiModelProperty(value = "保质期")
    private java.lang.String shelfLife;
	/**单位*/
	@Excel(name = "单位", width = 15)
    @ApiModelProperty(value = "单位")
    private java.lang.String unit;
	/**拆零单位*/
	@Excel(name = "拆零单位", width = 15)
    @ApiModelProperty(value = "拆零单位")
    private java.lang.String defuseUnit;
	/**体积*/
	@Excel(name = "体积", width = 15)
    @ApiModelProperty(value = "体积")
    private java.lang.String bulk;
	/**重量*/
	@Excel(name = "重量", width = 15)
    @ApiModelProperty(value = "重量")
    private java.lang.String weight;
	/**允收天数*/
	@Excel(name = "允收天数", width = 15)
    @ApiModelProperty(value = "允收天数")
    private java.lang.String admitReceivedays;
	/**拆零数量*/
	@Excel(name = "拆零数量", width = 15)
    @ApiModelProperty(value = "拆零数量")
    private java.lang.String defuseNum;
	/**价格*/
	@Excel(name = "价格", width = 15)
    @ApiModelProperty(value = "价格")
    private java.lang.String price;
	/**长*/
	@Excel(name = "长", width = 15)
    @ApiModelProperty(value = "长")
    private java.lang.String length;
	/**宽*/
	@Excel(name = "宽", width = 15)
    @ApiModelProperty(value = "宽")
    private java.lang.String wide;
	/**高*/
	@Excel(name = "高", width = 15)
    @ApiModelProperty(value = "高")
    private java.lang.String height;
	/**单位组*/
	@Excel(name = "单位组", width = 15)
    @ApiModelProperty(value = "单位组")
    private java.lang.String unitGroup;
	/**备用1*/
	@Excel(name = "备用1", width = 15)
    @ApiModelProperty(value = "备用1")
    private java.lang.String query1;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
    @ApiModelProperty(value = "备用2")
    private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
    @ApiModelProperty(value = "备用3")
    private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
    @ApiModelProperty(value = "备用4")
    private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
    @ApiModelProperty(value = "备用5")
    private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
    @ApiModelProperty(value = "备用6")
    private java.lang.String query6;
}
