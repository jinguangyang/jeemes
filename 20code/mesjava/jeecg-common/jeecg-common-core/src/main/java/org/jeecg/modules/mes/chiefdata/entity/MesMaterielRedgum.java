package org.jeecg.modules.mes.chiefdata.entity;

import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 主数据—辅料-红胶/锡膏
 * @Author: jeecg-boot
 * @Date:   2020-11-17
 * @Version: V1.0
 */
@ApiModel(value="mes_chiefdata_materiel对象", description="主数据—物料")
@Data
@TableName("mes_materiel_redgum")
public class MesMaterielRedgum implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
	@ApiModelProperty(value = "id")
	private java.lang.String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "创建日期")
	private java.util.Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "更新日期")
	private java.util.Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private java.lang.String sysOrgCode;
	/**安全库存量*/
	@Excel(name = "安全库存量", width = 15)
	@ApiModelProperty(value = "安全库存量")
	private java.lang.String storageNum;
	/**生成规则*/
	@Excel(name = "生成规则", width = 15)
	@ApiModelProperty(value = "生成规则")
	private java.lang.String createRule;
	/**回温时间(分钟)*/
	@Excel(name = "回温时间(分钟)", width = 15)
	@ApiModelProperty(value = "回温时间(分钟)")
	private java.lang.String heatTime;
	/**回温上限时间(分钟)*/
	@Excel(name = "回温上限时间(分钟)", width = 15)
	@ApiModelProperty(value = "回温上限时间(分钟)")
	private java.lang.String heatLimit;
	/**搅拌时间(分钟)*/
	@Excel(name = "搅拌时间(分钟)", width = 15)
	@ApiModelProperty(value = "搅拌时间(分钟)")
	private java.lang.String stirTime;
	/**搅拌上限时间(分钟)*/
	@Excel(name = "搅拌上限时间(分钟)", width = 15)
	@ApiModelProperty(value = "搅拌上限时间(分钟)")
	private java.lang.String stirLimit;
	/**回温次数*/
	@Excel(name = "回温次数", width = 15)
	@ApiModelProperty(value = "回温次数")
	private java.lang.String heatNum;
	/**过炉时间(分钟)*/
	@Excel(name = "过炉时间(分钟)", width = 15)
	@ApiModelProperty(value = "过炉时间(分钟)")
	private java.lang.String crossStove;
	/**保质期(天)*/
	@Excel(name = "保质期(天)", width = 15)
	@ApiModelProperty(value = "保质期(天)")
	private java.lang.String shelfDay;
	/**开罐保质期(分钟)*/
	@Excel(name = "开罐保质期(分钟)", width = 15)
	@ApiModelProperty(value = "开罐保质期(分钟)")
	private java.lang.String openShelfday;
	/**回温衰减(%)*/
	@Excel(name = "回温衰减(%)", width = 15)
	@ApiModelProperty(value = "回温衰减(%)")
	private java.lang.String heatReduce;
	/**RoHS*/
	@Excel(name = "RoHS", width = 15, dicCode = "yn")
	@ApiModelProperty(value = "RoHS")
	private java.lang.String rohs;
	/**辅料制具id*/
	@ApiModelProperty(value = "辅料制具id")
	private java.lang.String materielId;
	/**备用1*/
	@Excel(name = "辅料名称", width = 15)
	@ApiModelProperty(value = "辅料名称")
	private java.lang.String ancillaryName;
	/**备用2*/
	@Excel(name = "料号", width = 15)
	@ApiModelProperty(value = "料号")
	private java.lang.String ancillaryCode;
	/**备用3*/
	@Excel(name = "类型", width = 15)
	@ApiModelProperty(value = "类型")
	private java.lang.String ancillaryType;
	/**备用4*/
	@Excel(name = "规格", width = 15)
	@ApiModelProperty(value = "规格")
	private java.lang.String ancillaryGague;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
	@ApiModelProperty(value = "备用5")
	private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
	@ApiModelProperty(value = "备用6")
	private java.lang.String query6;
}
