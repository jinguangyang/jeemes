package org.jeecg.modules.mes.chiefdata.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 主数据—费用设置
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Data
@TableName("mes_chiefdata_fundset")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_chiefdata_fundset对象", description="主数据—费用设置")
public class MesChiefdataFundset implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**费用编号*/
	@Excel(name = "费用编号", width = 15)
    @ApiModelProperty(value = "费用编号")
    private java.lang.String fundCode;
	/**费用名称*/
	@Excel(name = "费用名称", width = 15)
    @ApiModelProperty(value = "费用名称")
    private java.lang.String fundName;
	/**费用计算公式*/
	@Excel(name = "费用计算公式", width = 15)
    @ApiModelProperty(value = "费用计算公式")
    private java.lang.String fundFormula;
	/**费用科目编码*/
	@Excel(name = "费用科目编码", width = 15)
    @ApiModelProperty(value = "费用科目编码")
    private java.lang.String fundSubjectcode;
	/**费用类型*/
	@Excel(name = "费用类型", width = 15, dicCode = "fund_type")
	@Dict(dicCode = "fund_type")
    @ApiModelProperty(value = "费用类型")
    private java.lang.String fundType;
	/**计费标准*/
	@Excel(name = "计费标准", width = 15, dicCode = "pay_standard")
	@Dict(dicCode = "pay_standard")
    @ApiModelProperty(value = "计费标准")
    private java.lang.String payStandard;
	/**计费方式*/
	@Excel(name = "计费方式", width = 15, dicCode = "pay_manner")
	@Dict(dicCode = "pay_manner")
    @ApiModelProperty(value = "计费方式")
    private java.lang.String payManner;
	/**最低费用*/
	@Excel(name = "最低费用", width = 15)
    @ApiModelProperty(value = "最低费用")
    private java.lang.String minFund;
	/**最高费用*/
	@Excel(name = "最高费用", width = 15)
    @ApiModelProperty(value = "最高费用")
    private java.lang.String maxFund;
	/**周期单位*/
	@Excel(name = "周期单位", width = 15)
    @ApiModelProperty(value = "周期单位")
    private java.lang.String cycleUnit;
	/**计费周期*/
	@Excel(name = "计费周期", width = 15)
    @ApiModelProperty(value = "计费周期")
    private java.lang.String payCycle;
	/**固定计费日*/
	@Excel(name = "固定计费日", width = 15)
    @ApiModelProperty(value = "固定计费日")
    private java.lang.String settingPayday;
	/**计费阶梯方式*/
	@Excel(name = "计费阶梯方式", width = 15)
    @ApiModelProperty(value = "计费阶梯方式")
    private java.lang.String payLadder;
	/**级差类型*/
	@Excel(name = "级差类型", width = 15)
    @ApiModelProperty(value = "级差类型")
    private java.lang.String gradeType;
	/**计费条件的SQL语句*/
	@Excel(name = "计费条件的SQL语句", width = 15)
    @ApiModelProperty(value = "计费条件的SQL语句")
    private java.lang.String paySql;
	/**备用1*/
	@Excel(name = "备用1", width = 15)
    @ApiModelProperty(value = "备用1")
    private java.lang.String query1;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
    @ApiModelProperty(value = "备用2")
    private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
    @ApiModelProperty(value = "备用3")
    private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
    @ApiModelProperty(value = "备用4")
    private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
    @ApiModelProperty(value = "备用5")
    private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
    @ApiModelProperty(value = "备用6")
    private java.lang.String query6;
}
