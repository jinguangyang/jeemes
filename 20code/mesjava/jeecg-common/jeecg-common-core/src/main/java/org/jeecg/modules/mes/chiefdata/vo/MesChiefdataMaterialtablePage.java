package org.jeecg.modules.mes.chiefdata.vo;

import java.util.List;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataMaterialtable;
import org.jeecg.modules.mes.chiefdata.entity.MesMaterialtableItem;
import lombok.Data;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelEntity;
import org.jeecgframework.poi.excel.annotation.ExcelCollection;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 主数据—料表管理
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Data
@ApiModel(value="mes_chiefdata_materialtablePage对象", description="主数据—料表管理")
public class MesChiefdataMaterialtablePage {

	/**id*/
	@ApiModelProperty(value = "id")
	private java.lang.String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "创建日期")
	private java.util.Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "更新日期")
	private java.util.Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private java.lang.String sysOrgCode;
	/**料站表编码*/
	@Excel(name = "料站表编码", width = 15)
	@ApiModelProperty(value = "料站表编码")
	private java.lang.String mtableCode;
	/**料站表名称*/
	@Excel(name = "料站表名称", width = 15)
	@ApiModelProperty(value = "料站表名称")
	private java.lang.String mtableName;
	/**加工面别*/
	@Excel(name = "加工面别", width = 15)
	@ApiModelProperty(value = "加工面别")
	private java.lang.String processFace;
	/**机种料号*/
	@Excel(name = "机种料号", width = 15)
	@ApiModelProperty(value = "机种料号")
	private java.lang.String machineCode;
	/**机种名称*/
	@Excel(name = "机种名称", width = 15)
	@ApiModelProperty(value = "机种名称")
	private java.lang.String machineName;
	/**机种规格*/
	@Excel(name = "机种规格", width = 15)
	@ApiModelProperty(value = "机种规格")
	private java.lang.String machineGague;
	/**贴片速率*/
	@Excel(name = "贴片速率", width = 15)
	@ApiModelProperty(value = "贴片速率")
	private java.lang.String pieceRate;
	/**贴片点数*/
	@Excel(name = "贴片点数", width = 15)
	@ApiModelProperty(value = "贴片点数")
	private java.lang.String piecePointnum;
	/**有效标志*/
	@Excel(name = "有效标志", width = 15)
	@ApiModelProperty(value = "有效标志")
	private java.lang.String validToken;
	/**轨道*/
	@Excel(name = "轨道", width = 15)
	@ApiModelProperty(value = "轨道")
	private java.lang.String track;
	/**备注*/
	@Excel(name = "备注", width = 15)
	@ApiModelProperty(value = "备注")
	private java.lang.String notes;
	/**备用1*/
	@Excel(name = "备用1", width = 15)
	@ApiModelProperty(value = "备用1")
	private java.lang.String query1;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
	@ApiModelProperty(value = "备用2")
	private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
	@ApiModelProperty(value = "备用3")
	private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
	@ApiModelProperty(value = "备用4")
	private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
	@ApiModelProperty(value = "备用5")
	private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
	@ApiModelProperty(value = "备用6")
	private java.lang.String query6;
	
	@ExcelCollection(name="主数据—料站表明细")
	@ApiModelProperty(value = "主数据—料站表明细")
	private List<MesMaterialtableItem> mesMaterialtableItemList;
	
}
