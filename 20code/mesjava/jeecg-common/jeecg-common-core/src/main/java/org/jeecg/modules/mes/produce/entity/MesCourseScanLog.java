package org.jeecg.modules.mes.produce.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 物料追踪记录
 * @Author: jeecg-boot
 * @Date:   2021-03-16
 * @Version: V1.0
 */
@Data
@TableName("mes_course_scan_log")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_course_scan_log对象", description="物料追踪记录")
public class MesCourseScanLog implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**FEEDER SN*/
	@Excel(name = "FEEDER SN", width = 15)
    @ApiModelProperty(value = "FEEDER SN")
    private java.lang.String feederSn;
	/**使用通道*/
	@Excel(name = "使用通道", width = 15)
    @ApiModelProperty(value = "使用通道")
    private java.lang.String passage;
	/**物料id*/
	@Excel(name = "物料id", width = 15)
    @ApiModelProperty(value = "物料id")
    private java.lang.String materielId;
	/**料号*/
	@Excel(name = "料号", width = 15)
    @ApiModelProperty(value = "料号")
    private java.lang.String materielCode;
	/**物料名称*/
	@Excel(name = "物料名称", width = 15)
    @ApiModelProperty(value = "物料名称")
    private java.lang.String materielName;
	/**工厂编号*/
	@Excel(name = "工厂编号", width = 15)
    @ApiModelProperty(value = "工厂编号")
    private java.lang.String wareCode;
	/**工厂名称*/
	@Excel(name = "工厂名称", width = 15)
    @ApiModelProperty(value = "工厂名称")
    private java.lang.String wareName;
	/**存储位置*/
	@Excel(name = "存储位置", width = 15)
    @ApiModelProperty(value = "存储位置")
    private java.lang.String wareSite;
	/**用量*/
	@Excel(name = "用量", width = 15)
    @ApiModelProperty(value = "用量")
    private java.lang.String quantity;
	/**单位*/
	@Excel(name = "单位", width = 15)
    @ApiModelProperty(value = "单位")
    private java.lang.String unit;
	/**制令单id*/
	@Excel(name = "制令单id", width = 15)
    @ApiModelProperty(value = "制令单id")
    private java.lang.String commandbillId;
	/**制令单号*/
	@Excel(name = "制令单号", width = 15)
    @ApiModelProperty(value = "制令单号")
    private java.lang.String commandbillCode;
	/**线别*/
	@Excel(name = "线别", width = 15)
    @ApiModelProperty(value = "线别")
    private java.lang.String lineType;
	/**二维码编号*/
	@Excel(name = "二维码编号", width = 15)
    @ApiModelProperty(value = "二维码编号")
    private java.lang.String storageId;
    /**pcb编号*/
    @Excel(name = "pcb编号", width = 15)
    @ApiModelProperty(value = "pcb编号")
    private java.lang.String pcbId;

}
