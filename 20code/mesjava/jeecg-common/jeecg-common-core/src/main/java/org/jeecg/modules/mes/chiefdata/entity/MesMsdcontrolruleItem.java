package org.jeecg.modules.mes.chiefdata.entity;

import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 主数据—MSD管控规则明细
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@ApiModel(value="mes_chiefdata_msdcontrolrule对象", description="主数据—MSD管控规则")
@Data
@TableName("mes_msdcontrolrule_item")
public class MesMsdcontrolruleItem implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
	@ApiModelProperty(value = "id")
	private java.lang.String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "创建日期")
	private java.util.Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "更新日期")
	private java.util.Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private java.lang.String sysOrgCode;
	/**烘烤温度(°C)*/
	@Excel(name = "烘烤温度(°C)", width = 15)
	@ApiModelProperty(value = "烘烤温度(°C)")
	private java.lang.String toastTemp;
	/**烘烤下限(H)*/
	@Excel(name = "烘烤下限(H)", width = 15)
	@ApiModelProperty(value = "烘烤下限(H)")
	private java.lang.String toastDownlimit;
	/**烘烤上限(H)*/
	@Excel(name = "烘烤上限(H)", width = 15)
	@ApiModelProperty(value = "烘烤上限(H)")
	private java.lang.String toastUplimit;
	/**偏差温度(°C)*/
	@Excel(name = "偏差温度(°C)", width = 15)
	@ApiModelProperty(value = "偏差温度(°C)")
	private java.lang.String tempGap;
	/**MSD规则ID*/
	@ApiModelProperty(value = "MSD规则ID")
	private java.lang.String msdRuleid;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
	@ApiModelProperty(value = "备用2")
	private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
	@ApiModelProperty(value = "备用3")
	private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
	@ApiModelProperty(value = "备用4")
	private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
	@ApiModelProperty(value = "备用5")
	private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
	@ApiModelProperty(value = "备用6")
	private java.lang.String query6;
}
