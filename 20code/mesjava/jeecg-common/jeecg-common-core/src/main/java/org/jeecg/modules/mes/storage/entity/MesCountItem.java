package org.jeecg.modules.mes.storage.entity;

import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 仓库管理—盘点子表
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@ApiModel(value="mes_storage_count对象", description="仓库管理—盘点")
@Data
@TableName("mes_count_item")
public class MesCountItem implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
	@ApiModelProperty(value = "id")
	private java.lang.String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "创建日期")
	private java.util.Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "更新日期")
	private java.util.Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private java.lang.String sysOrgCode;
	/**行号*/
	@Excel(name = "行号", width = 15)
	@ApiModelProperty(value = "行号")
	private java.lang.String rowNum;
	/**产品编号*/
	@Excel(name = "产品编号", width = 15)
	@ApiModelProperty(value = "产品编号")
	private java.lang.String productCode;
	/**产品名称*/
	@Excel(name = "产品名称", width = 15)
	@ApiModelProperty(value = "产品名称")
	private java.lang.String productName;
	/**货主编号*/
	@Excel(name = "货主编号", width = 15)
	@ApiModelProperty(value = "货主编号")
	private java.lang.String shipperCode;
	/**批号*/
	@Excel(name = "批号", width = 15)
	@ApiModelProperty(value = "批号")
	private java.lang.String batchCode;
	/**包装编码*/
	@Excel(name = "包装编码", width = 15)
	@ApiModelProperty(value = "包装编码")
	private java.lang.String packageCode;
	/**区域号*/
	@Excel(name = "区域号", width = 15)
	@ApiModelProperty(value = "区域号")
	private java.lang.String regionCode;
	/**库位号*/
	@Excel(name = "库位号", width = 15)
	@ApiModelProperty(value = "库位号")
	private java.lang.String waresiteNum;
	/**仓库编号*/
	@Excel(name = "仓库编号", width = 15)
	@ApiModelProperty(value = "仓库编号")
	private java.lang.String wareCode;
	/**库存数量*/
	@Excel(name = "库存数量", width = 15)
	@ApiModelProperty(value = "库存数量")
	private java.lang.String storageNum;
	/**盘点数量*/
	@Excel(name = "盘点数量", width = 15)
	@ApiModelProperty(value = "盘点数量")
	private java.lang.String countNum;
	/**差异数量*/
	@Excel(name = "差异数量", width = 15)
	@ApiModelProperty(value = "差异数量")
	private java.lang.String marginNum;
	/**单位*/
	@Excel(name = "单位", width = 15)
	@ApiModelProperty(value = "单位")
	private java.lang.String unit;
	/**抽盘数量*/
	@Excel(name = "抽盘数量", width = 15)
	@ApiModelProperty(value = "抽盘数量")
	private java.lang.String sampleNum;
	/**抽盘比例*/
	@Excel(name = "抽盘比例", width = 15)
	@ApiModelProperty(value = "抽盘比例")
	private java.lang.String sampleRate;
	/**监盘人*/
	@Excel(name = "监盘人", width = 15)
	@ApiModelProperty(value = "监盘人")
	private java.lang.String overseePerson;
	/**盘点id*/
	@ApiModelProperty(value = "盘点id")
	private java.lang.String countId;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
	@ApiModelProperty(value = "备用2")
	private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
	@ApiModelProperty(value = "备用3")
	private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
	@ApiModelProperty(value = "备用4")
	private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
	@ApiModelProperty(value = "备用5")
	private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
	@ApiModelProperty(value = "备用6")
	private java.lang.String query6;
}
