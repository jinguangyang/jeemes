package org.jeecg.modules.mes.chiefdata.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.jeecg.common.aspect.annotation.Dict;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

/**
 * @Description: BOM—数据项
 * @Author: jeecg-boot
 * @Date:   2020-11-17
 * @Version: V1.0
 */
@Data
@TableName("mes_chiefdata_bomitem")
@ApiModel(value="mes_chiefdata_bom对象", description="主数据—BOM")
public class MesChiefdataBomitem implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
	@ApiModelProperty(value = "id")
	private java.lang.String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "创建日期")
	private java.util.Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "更新日期")
	private java.util.Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private java.lang.String sysOrgCode;
	/**物料id*/
//	@Excel(name = "物料id", width = 15)
	@ApiModelProperty(value = "物料id")
	private java.lang.String materielId;
	/**料件类型*/
	@Excel(name = "料件类型", width = 15)
	@Dict(dicCode = "material_type")
	@ApiModelProperty(value = "料件类型")
	private java.lang.String materielType;
	/**商品编码*/
	@Excel(name = "商品编码", width = 15)
	@ApiModelProperty(value = "商品编码")
	private java.lang.String productCode;
	/**客户料号*/
	@Excel(name = "客户料号", width = 15)
	@ApiModelProperty(value = "客户料号")
	private java.lang.String clientCode;
	/**内部料号*/
	@Excel(name = "内部料号", width = 15)
	@ApiModelProperty(value = "内部料号")
	private java.lang.String materielCode;
	/**料件名称*/
	@Excel(name = "料件名称", width = 15)
	@ApiModelProperty(value = "品名")
	private java.lang.String materielName;
	/**料件规格*/
	@Excel(name = "料件规格", width = 15)
	@ApiModelProperty(value = "料件规格")
	private java.lang.String materielGauge;
	/**误差*/
	@Excel(name = "误差", width = 15)
	@ApiModelProperty(value = "误差")
	private java.lang.String differences;
	/**伏数*/
	@Excel(name = "伏数", width = 15)
	@ApiModelProperty(value = "伏数")
	private java.lang.String voltage;
	/**封装*/
	@Excel(name = "封装", width = 15)
	@ApiModelProperty(value = "封装")
	private java.lang.String packup;
	/**物料阶别*/
	@Excel(name = "物料阶别", width = 15)
	@ApiModelProperty(value = "物料阶别")
	private java.lang.String materielGrade;
	/**损耗率*/
	@Excel(name = "损耗率", width = 15)
	@ApiModelProperty(value = "损耗率")
	private java.lang.String intangibleRate;
	/**单位*/
	@Excel(name = "单位", width = 15)
	@ApiModelProperty(value = "单位")
	private java.lang.String unit;
	/**正面用量*/
	@Excel(name = "正面用量", width = 15)
	@ApiModelProperty(value = "正面用量")
	private java.lang.String quantity;
	/**反面用量*/
	@Excel(name = "反面用量", width = 15)
	@ApiModelProperty(value = "反面用量")
	private java.lang.String inverseQuantity;
	/**位号*/
	@Excel(name = "位号", width = 15)
	@ApiModelProperty(value = "位号")
	private java.lang.String positionNum;
	/**半成品类型*/
	@Excel(name = "半成品类型", width = 15)
	@ApiModelProperty(value = "半成品类型")
	private java.lang.String materialType;
	/**最小包装数*/
	@Excel(name = "最小包装数", width = 15)
	@ApiModelProperty(value = "最小包装数")
	private java.lang.String packType;
	/**净耗*/
//	@Excel(name = "净耗", width = 15)
	@ApiModelProperty(value = "净耗")
	private java.lang.String unitConsume;
	/**有形损耗率*/
//	@Excel(name = "有形损耗率", width = 15)
	@ApiModelProperty(value = "有形损耗率")
	private java.lang.String wastageRate;
	/**点位*/
//	@Excel(name = "点位", width = 15)
	@ApiModelProperty(value = "点位")
	private java.lang.String spot;
	/**反面点位*/
//	@Excel(name = "反面点位", width = 15)
	@ApiModelProperty(value = "反面点位")
	private java.lang.String inverseSpot;
	/**正面点位*/
//	@Excel(name = "正面点位", width = 15)
	@ApiModelProperty(value = "正面点位")
	private java.lang.String frontSpot;
	/**正面用量*/
//	@Excel(name = "正面用量", width = 15)
	@ApiModelProperty(value = "正面用量")
	private java.lang.String frontQuantity;
	/**BOMid*/
	@ApiModelProperty(value = "BOMid")
	private java.lang.String bomId;
	/**生产阶别*/
//	@Excel(name = "生产阶别", width = 15)
	@ApiModelProperty(value = "生产阶别")
	private java.lang.String produceGrade;
}
