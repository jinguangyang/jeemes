package org.jeecg.modules.mes.inspect.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.List;

/**
 * @Description: 巡检模块记录
 * @Author: jeecg-boot
 * @Date:   2021-03-21
 * @Version: V1.0
 */
@Data
@TableName("patrol_inspect_log")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="patrol_inspect_log对象", description="巡检模块记录")
public class PatrolInspectLog implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
    /**所属部门*/
    @ApiModelProperty(value = "主表id")
    private java.lang.String mainId;
    /**巡检日期*/
    @Excel(name = "巡检日期", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "巡检日期")
    private java.util.Date  inspectDate;
	/**班别*/
	@Excel(name = "班别", width = 15)
    @ApiModelProperty(value = "班别")
    private java.lang.String inspectFrequen;
	/**线别*/
	@Excel(name = "线别", width = 15)
    @ApiModelProperty(value = "线别")
    private java.lang.String lineType;
	/**巡检类型*/
	@Excel(name = "巡检类型", width = 15)
    @ApiModelProperty(value = "巡检类型")
    private java.lang.String patrolInspectType;
    /**产品型号*/
    @Excel(name = "产品型号", width = 15)
    @ApiModelProperty(value = "产品型号")
    private java.lang.String produceTypeNum;
    /**照片*/
    @Excel(name = "照片", width = 15)
    @ApiModelProperty(value = "照片")
    private java.lang.String inspectImg;
	/**巡检项目*/
	@Excel(name = "巡检项目", width = 15)
    @ApiModelProperty(value = "巡检项目")
    private java.lang.String patrolInspectProject;
	/**巡检内容*/
	@Excel(name = "巡检内容", width = 15)
    @ApiModelProperty(value = "巡检内容")
    private java.lang.String patrolInspectContent;
	/**是否合格*/
	@Excel(name = "是否合格", width = 15)
    @ApiModelProperty(value = "是否合格")
    private java.lang.String patrolUp;
    /**是否合格数组*/
//    @Excel(name = "是否合格", width = 15)
    @ApiModelProperty(value = "是否合格数组")
    @TableField(exist = false)
    private List<String> patrolUps;
	/**改善情况*/
	@Excel(name = "改善情况", width = 15)
    @ApiModelProperty(value = "改善情况")
    private java.lang.String patrolState;
}
