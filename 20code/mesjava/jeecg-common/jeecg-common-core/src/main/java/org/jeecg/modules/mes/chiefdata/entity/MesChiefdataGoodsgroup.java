package org.jeecg.modules.mes.chiefdata.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 主数据—产品组
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Data
@TableName("mes_chiefdata_goodsgroup")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="mes_chiefdata_goodsgroup对象", description="主数据—产品组")
public class MesChiefdataGoodsgroup implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "id")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**物料组编码*/
	@Excel(name = "物料组编码", width = 15)
    @ApiModelProperty(value = "物料组编码")
    private java.lang.String groupCode;
	/**物料组名称*/
	@Excel(name = "物料组名称", width = 15)
    @ApiModelProperty(value = "物料组名称")
    private java.lang.String groupName;
	/**物料组类型*/
	@Excel(name = "物料组类型", width = 15, dicCode = "group_type")
	@Dict(dicCode = "group_type")
    @ApiModelProperty(value = "物料组类型")
    private java.lang.String groupType;
	/**工艺名称*/
	@Excel(name = "工艺名称", width = 15)
    @ApiModelProperty(value = "工艺名称")
    private java.lang.String craftName;
	/**加工面*/
	@Excel(name = "加工面", width = 15)
    @ApiModelProperty(value = "加工面")
    private java.lang.String processSide;
	/**不良报废标志*/
	@Excel(name = "不良报废标志", width = 15, dicCode = "yn")
	@Dict(dicCode = "yn")
    @ApiModelProperty(value = "不良报废标志")
    private java.lang.String discardedToken;
	/**FQC自动送检*/
	@Excel(name = "FQC自动送检", width = 15, dicCode = "yn")
	@Dict(dicCode = "yn")
    @ApiModelProperty(value = "FQC自动送检")
    private java.lang.String fqcAutocheck;
	/**送批数量*/
	@Excel(name = "送批数量", width = 15)
    @ApiModelProperty(value = "送批数量")
    private java.lang.String sendChecknum;
	/**维修上限次数*/
	@Excel(name = "维修上限次数", width = 15)
    @ApiModelProperty(value = "维修上限次数")
    private java.lang.String fixLimit;
	/**管控类型*/
	@Excel(name = "管控类型", width = 15)
    @ApiModelProperty(value = "管控类型")
    private java.lang.String controlType;
	/**产品条码规则*/
	@Excel(name = "产品条码规则", width = 15)
    @ApiModelProperty(value = "产品条码规则")
    private java.lang.String productBarcode;
	/**小板条码规则*/
	@Excel(name = "小板条码规则", width = 15)
    @ApiModelProperty(value = "小板条码规则")
    private java.lang.String smallBarcode;
	/**联板数*/
	@Excel(name = "联板数", width = 15)
    @ApiModelProperty(value = "联板数")
    private java.lang.String linkBoards;
	/**条码拼板数*/
	@Excel(name = "条码拼板数", width = 15)
    @ApiModelProperty(value = "条码拼板数")
    private java.lang.String barcodeBoards;
	/**库存复检周期*/
	@Excel(name = "库存复检周期", width = 15)
    @ApiModelProperty(value = "库存复检周期")
    private java.lang.String stockCheckcycle;
	/**复检提醒天数*/
	@Excel(name = "复检提醒天数", width = 15)
    @ApiModelProperty(value = "复检提醒天数")
    private java.lang.String checkRemindays;
	/**老化时长*/
	@Excel(name = "老化时长", width = 15)
    @ApiModelProperty(value = "老化时长")
    private java.lang.String agingHours;
	/**呆滞周期*/
	@Excel(name = "呆滞周期", width = 15)
    @ApiModelProperty(value = "呆滞周期")
    private java.lang.String tranceCycle;
	/**体积*/
	@Excel(name = "体积", width = 15)
    @ApiModelProperty(value = "体积")
    private java.lang.String bulk;
	/**备用1*/
	@Excel(name = "备用1", width = 15)
    @ApiModelProperty(value = "备用1")
    private java.lang.String query1;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
    @ApiModelProperty(value = "备用2")
    private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
    @ApiModelProperty(value = "备用3")
    private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
    @ApiModelProperty(value = "备用4")
    private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
    @ApiModelProperty(value = "备用5")
    private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
    @ApiModelProperty(value = "备用6")
    private java.lang.String query6;
}
