package org.jeecg.modules.mes.chiefdata.entity;

import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * @Description: 物料—其他
 * @Author: jeecg-boot
 * @Date:   2020-11-17
 * @Version: V1.0
 */
@ApiModel(value="mes_chiefdata_materiel对象", description="主数据—物料")
@Data
@TableName("mes_materiel_other")
public class MesMaterielOther implements Serializable {
    private static final long serialVersionUID = 1L;

	/**id*/
	@TableId(type = IdType.ID_WORKER_STR)
	@ApiModelProperty(value = "id")
	private java.lang.String id;
	/**创建人*/
	@ApiModelProperty(value = "创建人")
	private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "创建日期")
	private java.util.Date createTime;
	/**更新人*/
	@ApiModelProperty(value = "更新人")
	private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
	@ApiModelProperty(value = "更新日期")
	private java.util.Date updateTime;
	/**所属部门*/
	@ApiModelProperty(value = "所属部门")
	private java.lang.String sysOrgCode;
	/**MSD管控标志*/
	@Excel(name = "MSD管控标志", width = 15, dicCode = "yn")
	@ApiModelProperty(value = "MSD管控标志")
	private java.lang.String msdSymbol;
	/**ROHS*/
	@Excel(name = "ROHS", width = 15, dicCode = "yn")
	@ApiModelProperty(value = "ROHS")
	private java.lang.String rohs;
	/**MSD规则*/
	@Excel(name = "MSD规则", width = 15)
	@ApiModelProperty(value = "MSD规则")
	private java.lang.String msdRule;
	/**客户料号*/
	@Excel(name = "客户料号", width = 15)
	@ApiModelProperty(value = "客户料号")
	private java.lang.String clientCode;
	/**料盘尺寸*/
	@Excel(name = "料盘尺寸", width = 15)
	@ApiModelProperty(value = "料盘尺寸")
	private java.lang.String plateSize;
	/**物料id*/
	@ApiModelProperty(value = "物料id")
	private java.lang.String materielId;
	/**备用1*/
	@Excel(name = "备用1", width = 15)
	@ApiModelProperty(value = "备用1")
	private java.lang.String query1;
	/**备用2*/
	@Excel(name = "备用2", width = 15)
	@ApiModelProperty(value = "备用2")
	private java.lang.String query2;
	/**备用3*/
	@Excel(name = "备用3", width = 15)
	@ApiModelProperty(value = "备用3")
	private java.lang.String query3;
	/**备用4*/
	@Excel(name = "备用4", width = 15)
	@ApiModelProperty(value = "备用4")
	private java.lang.String query4;
	/**备用5*/
	@Excel(name = "备用5", width = 15)
	@ApiModelProperty(value = "备用5")
	private java.lang.String query5;
	/**备用6*/
	@Excel(name = "备用6", width = 15)
	@ApiModelProperty(value = "备用6")
	private java.lang.String query6;
}
