package org.jeecg.modules.mes.chiefdata.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Select;
import org.jeecg.modules.mes.chiefdata.entity.MesMaterielRedgum;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 主数据—辅料-红胶/锡膏
 * @Author: jeecg-boot
 * @Date:   2020-10-16
 * @Version: V1.0
 */
public interface MesMaterielRedgumMapper extends BaseMapper<MesMaterielRedgum> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<MesMaterielRedgum> selectByMainId(@Param("mainId") String mainId);

	@Select("select * from mes_materiel_redgum where materiel_id=#{pCode}")
	MesMaterielRedgum findBymId(@Param("pCode") String pCode);
}
