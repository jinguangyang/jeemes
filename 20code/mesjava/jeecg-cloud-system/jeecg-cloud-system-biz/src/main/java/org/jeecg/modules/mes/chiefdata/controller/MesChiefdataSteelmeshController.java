package org.jeecg.modules.mes.chiefdata.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataScraper;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataSteelmesh;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataSteelmeshService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 钢网建档
 * @Author: jeecg-boot
 * @Date:   2021-04-19
 * @Version: V1.0
 */
@Api(tags="钢网建档")
@RestController
@RequestMapping("/chiefdata/mesChiefdataSteelmesh")
@Slf4j
public class MesChiefdataSteelmeshController extends JeecgController<MesChiefdataSteelmesh, IMesChiefdataSteelmeshService> {
	@Autowired
	private IMesChiefdataSteelmeshService mesChiefdataSteelmeshService;
	
	/**
	 * 分页列表查询
	 *
	 * @param mesChiefdataSteelmesh
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "钢网建档-分页列表查询")
	@ApiOperation(value="钢网建档-分页列表查询", notes="钢网建档-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesChiefdataSteelmesh mesChiefdataSteelmesh,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesChiefdataSteelmesh> queryWrapper = QueryGenerator.initQueryWrapper(mesChiefdataSteelmesh, req.getParameterMap());
		Page<MesChiefdataSteelmesh> page = new Page<MesChiefdataSteelmesh>(pageNo, pageSize);
		IPage<MesChiefdataSteelmesh> pageList = mesChiefdataSteelmeshService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param mesChiefdataSteelmesh
	 * @return
	 */
	@AutoLog(value = "钢网建档-添加")
	@ApiOperation(value="钢网建档-添加", notes="钢网建档-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MesChiefdataSteelmesh mesChiefdataSteelmesh) {
		mesChiefdataSteelmeshService.save(mesChiefdataSteelmesh);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param mesChiefdataSteelmesh
	 * @return
	 */
	@AutoLog(value = "钢网建档-编辑")
	@ApiOperation(value="钢网建档-编辑", notes="钢网建档-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody MesChiefdataSteelmesh mesChiefdataSteelmesh) {
		mesChiefdataSteelmeshService.updateById(mesChiefdataSteelmesh);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "钢网建档-通过id删除")
	@ApiOperation(value="钢网建档-通过id删除", notes="钢网建档-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		mesChiefdataSteelmeshService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "钢网建档-批量删除")
	@ApiOperation(value="钢网建档-批量删除", notes="钢网建档-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.mesChiefdataSteelmeshService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "钢网建档-通过id查询")
	@ApiOperation(value="钢网建档-通过id查询", notes="钢网建档-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		MesChiefdataSteelmesh mesChiefdataSteelmesh = mesChiefdataSteelmeshService.getById(id);
		if(mesChiefdataSteelmesh==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(mesChiefdataSteelmesh);
	}
	 /**
	  * 远程调用，编辑钢网
	  *
	  * @param mesChiefdataSteelmesh
	  * @return
	  */
	 @PutMapping(value = "/editSteelmesh")
	 public String editSteelmeshComitem(@RequestBody MesChiefdataSteelmesh mesChiefdataSteelmesh) {
		 mesChiefdataSteelmeshService.updateById(mesChiefdataSteelmesh);
		 return "编辑成功!";
	 }
	 /**
	  * 远程调用，根据sn编码，增加使用次数
	  *
	  * @param steelmeshSn
	  * @return
	  */
	 @GetMapping(value = "/increaseSteelmeshNum")
	 public boolean increaseSteelmeshNum(@RequestParam(name="steelmeshSn",required=true) String steelmeshSn){
		 return mesChiefdataSteelmeshService.increasePointUsageNum(steelmeshSn);
	 }

	 /**
	  * 远程调用，get Steelmesh 根据Steelmesh sn获取
	  *
	  * @param sn
	  * @return
	  */
	 @GetMapping(value = "/getSteelmesh")
	 public MesChiefdataSteelmesh getSteelmeshComitem(@RequestParam(name="sn",required=true) String sn) {
		 QueryWrapper<MesChiefdataSteelmesh> queryWrapper = new QueryWrapper<>();
		 queryWrapper.eq("steelmesh_sn",sn);
		 MesChiefdataSteelmesh mesChiefdataSteelmesh = mesChiefdataSteelmeshService.getOne(queryWrapper);
		 return mesChiefdataSteelmesh;
	 }
    /**
    * 导出excel
    *
    * @param request
    * @param mesChiefdataSteelmesh
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesChiefdataSteelmesh mesChiefdataSteelmesh) {
        return super.exportXls(request, mesChiefdataSteelmesh, MesChiefdataSteelmesh.class, "钢网建档");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesChiefdataSteelmesh.class);
    }

}
