package org.jeecg.modules.mes.chiefdata.service;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataScraper;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 刮刀建档
 * @Author: jeecg-boot
 * @Date:   2021-04-19
 * @Version: V1.0
 */
public interface IMesChiefdataScraperService extends IService<MesChiefdataScraper> {

    boolean increasePointUsageNum(String scraperSn);
}
