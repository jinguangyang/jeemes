package org.jeecg.modules.mes.chiefdata.service;

import org.jeecg.modules.mes.chiefdata.entity.MesMsdcontrolruleItem;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataMsdcontrolrule;
import com.baomidou.mybatisplus.extension.service.IService;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 主数据—MSD管控规则
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
public interface IMesChiefdataMsdcontrolruleService extends IService<MesChiefdataMsdcontrolrule> {

	/**
	 * 添加一对多
	 * 
	 */
	public void saveMain(MesChiefdataMsdcontrolrule mesChiefdataMsdcontrolrule,List<MesMsdcontrolruleItem> mesMsdcontrolruleItemList) ;
	
	/**
	 * 修改一对多
	 * 
	 */
	public void updateMain(MesChiefdataMsdcontrolrule mesChiefdataMsdcontrolrule,List<MesMsdcontrolruleItem> mesMsdcontrolruleItemList);
	
	/**
	 * 删除一对多
	 */
	public void delMain (String id);
	
	/**
	 * 批量删除一对多
	 */
	public void delBatchMain (Collection<? extends Serializable> idList);
	
}
