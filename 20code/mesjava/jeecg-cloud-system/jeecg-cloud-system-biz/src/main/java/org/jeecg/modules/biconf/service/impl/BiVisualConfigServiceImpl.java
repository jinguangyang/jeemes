package org.jeecg.modules.biconf.service.impl;

import org.jeecg.modules.biconf.entity.BiVisualConfig;
import org.jeecg.modules.biconf.mapper.BiVisualConfigMapper;
import org.jeecg.modules.biconf.service.IBiVisualConfigService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 大屏页面配置数据
 * @Author: jeecg-boot
 * @Date:   2020-09-09
 * @Version: V1.0
 */
@Service
public class BiVisualConfigServiceImpl extends ServiceImpl<BiVisualConfigMapper, BiVisualConfig> implements IBiVisualConfigService {

}
