package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataSupplieraddress;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataSupplieraddressMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataSupplieraddressService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 主数据—供应商地址
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Service
public class MesChiefdataSupplieraddressServiceImpl extends ServiceImpl<MesChiefdataSupplieraddressMapper, MesChiefdataSupplieraddress> implements IMesChiefdataSupplieraddressService {

}
