package org.jeecg.modules.mes.chiefdata.mapper;

import java.util.List;
import org.jeecg.modules.mes.chiefdata.entity.MesClassbatchTimetable;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 班组—时间
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
public interface MesClassbatchTimetableMapper extends BaseMapper<MesClassbatchTimetable> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<MesClassbatchTimetable> selectByMainId(@Param("mainId") String mainId);
}
