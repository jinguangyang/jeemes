package org.jeecg.modules.mes.mesapp.service;

import org.jeecg.modules.mes.mesapp.entity.MesAppFunction;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 主数据—APP功能模块
 * @Author: jeecg-boot
 * @Date:   2020-10-14
 * @Version: V1.0
 */
public interface IMesAppFunctionService extends IService<MesAppFunction> {

}
