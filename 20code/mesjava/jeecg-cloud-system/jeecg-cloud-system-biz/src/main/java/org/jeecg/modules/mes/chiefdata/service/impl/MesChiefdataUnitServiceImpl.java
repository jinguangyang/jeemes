package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataUnit;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataUnitMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataUnitService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 主数据—单位
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Service
public class MesChiefdataUnitServiceImpl extends ServiceImpl<MesChiefdataUnitMapper, MesChiefdataUnit> implements IMesChiefdataUnitService {

}
