package org.jeecg.modules.mes.chiefdata.service;

import org.jeecg.modules.mes.chiefdata.entity.MesElectronicbusBaseinfo;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataElectronicbus;
import com.baomidou.mybatisplus.extension.service.IService;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 主数据—电子料车
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
public interface IMesChiefdataElectronicbusService extends IService<MesChiefdataElectronicbus> {

	/**
	 * 添加一对多
	 * 
	 */
	public void saveMain(MesChiefdataElectronicbus mesChiefdataElectronicbus,List<MesElectronicbusBaseinfo> mesElectronicbusBaseinfoList) ;
	
	/**
	 * 修改一对多
	 * 
	 */
	public void updateMain(MesChiefdataElectronicbus mesChiefdataElectronicbus,List<MesElectronicbusBaseinfo> mesElectronicbusBaseinfoList);
	
	/**
	 * 删除一对多
	 */
	public void delMain (String id);
	
	/**
	 * 批量删除一对多
	 */
	public void delBatchMain (Collection<? extends Serializable> idList);
	
}
