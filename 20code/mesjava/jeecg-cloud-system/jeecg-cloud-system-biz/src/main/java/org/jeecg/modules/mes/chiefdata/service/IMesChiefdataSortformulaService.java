package org.jeecg.modules.mes.chiefdata.service;

import org.jeecg.modules.mes.chiefdata.entity.MesSortformulaStrategyinfo;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataSortformula;
import com.baomidou.mybatisplus.extension.service.IService;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 主数据—拣货方案
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
public interface IMesChiefdataSortformulaService extends IService<MesChiefdataSortformula> {

	/**
	 * 添加一对多
	 * 
	 */
	public void saveMain(MesChiefdataSortformula mesChiefdataSortformula,List<MesSortformulaStrategyinfo> mesSortformulaStrategyinfoList) ;
	
	/**
	 * 修改一对多
	 * 
	 */
	public void updateMain(MesChiefdataSortformula mesChiefdataSortformula,List<MesSortformulaStrategyinfo> mesSortformulaStrategyinfoList);
	
	/**
	 * 删除一对多
	 */
	public void delMain (String id);
	
	/**
	 * 批量删除一对多
	 */
	public void delBatchMain (Collection<? extends Serializable> idList);
	
}
