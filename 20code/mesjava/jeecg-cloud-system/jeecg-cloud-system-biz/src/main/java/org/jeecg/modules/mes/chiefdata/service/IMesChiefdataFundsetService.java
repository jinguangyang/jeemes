package org.jeecg.modules.mes.chiefdata.service;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataFundset;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 主数据—费用设置
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
public interface IMesChiefdataFundsetService extends IService<MesChiefdataFundset> {

}
