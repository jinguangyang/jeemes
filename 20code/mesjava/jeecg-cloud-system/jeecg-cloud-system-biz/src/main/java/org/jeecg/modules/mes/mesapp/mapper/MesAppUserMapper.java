package org.jeecg.modules.mes.mesapp.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.mesapp.entity.MesAppUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 主数据—用户功能
 * @Author: jeecg-boot
 * @Date:   2020-10-14
 * @Version: V1.0
 */
public interface MesAppUserMapper extends BaseMapper<MesAppUser> {

}
