package org.jeecg.modules.mes.chiefdata.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataClientaddress;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataClientaddressService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 主数据—客户地址
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Api(tags="主数据—客户地址")
@RestController
@RequestMapping("/chiefdata/mesChiefdataClientaddress")
@Slf4j
public class MesChiefdataClientaddressController extends JeecgController<MesChiefdataClientaddress, IMesChiefdataClientaddressService> {
	@Autowired
	private IMesChiefdataClientaddressService mesChiefdataClientaddressService;
	
	/**
	 * 分页列表查询
	 *
	 * @param mesChiefdataClientaddress
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "主数据—客户地址-分页列表查询")
	@ApiOperation(value="主数据—客户地址-分页列表查询", notes="主数据—客户地址-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesChiefdataClientaddress mesChiefdataClientaddress,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesChiefdataClientaddress> queryWrapper = QueryGenerator.initQueryWrapper(mesChiefdataClientaddress, req.getParameterMap());
		Page<MesChiefdataClientaddress> page = new Page<MesChiefdataClientaddress>(pageNo, pageSize);
		IPage<MesChiefdataClientaddress> pageList = mesChiefdataClientaddressService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param mesChiefdataClientaddress
	 * @return
	 */
	@AutoLog(value = "主数据—客户地址-添加")
	@ApiOperation(value="主数据—客户地址-添加", notes="主数据—客户地址-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MesChiefdataClientaddress mesChiefdataClientaddress) {
		mesChiefdataClientaddressService.save(mesChiefdataClientaddress);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param mesChiefdataClientaddress
	 * @return
	 */
	@AutoLog(value = "主数据—客户地址-编辑")
	@ApiOperation(value="主数据—客户地址-编辑", notes="主数据—客户地址-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody MesChiefdataClientaddress mesChiefdataClientaddress) {
		mesChiefdataClientaddressService.updateById(mesChiefdataClientaddress);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "主数据—客户地址-通过id删除")
	@ApiOperation(value="主数据—客户地址-通过id删除", notes="主数据—客户地址-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		mesChiefdataClientaddressService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "主数据—客户地址-批量删除")
	@ApiOperation(value="主数据—客户地址-批量删除", notes="主数据—客户地址-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.mesChiefdataClientaddressService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "主数据—客户地址-通过id查询")
	@ApiOperation(value="主数据—客户地址-通过id查询", notes="主数据—客户地址-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		MesChiefdataClientaddress mesChiefdataClientaddress = mesChiefdataClientaddressService.getById(id);
		if(mesChiefdataClientaddress==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(mesChiefdataClientaddress);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param mesChiefdataClientaddress
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesChiefdataClientaddress mesChiefdataClientaddress) {
        return super.exportXls(request, mesChiefdataClientaddress, MesChiefdataClientaddress.class, "主数据—客户地址");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesChiefdataClientaddress.class);
    }

}
