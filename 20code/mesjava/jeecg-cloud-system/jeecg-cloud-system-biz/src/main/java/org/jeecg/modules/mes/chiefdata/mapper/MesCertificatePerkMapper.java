package org.jeecg.modules.mes.chiefdata.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.chiefdata.entity.MesCertificatePerk;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 物料凭证抬头
 * @Author: jeecg-boot
 * @Date:   2020-10-12
 * @Version: V1.0
 */
public interface MesCertificatePerkMapper extends BaseMapper<MesCertificatePerk> {

}
