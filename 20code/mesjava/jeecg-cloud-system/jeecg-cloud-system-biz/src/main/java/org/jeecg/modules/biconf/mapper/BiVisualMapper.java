package org.jeecg.modules.biconf.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.biconf.entity.BiVisual;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 大屏列表
 * @Author: jeecg-boot
 * @Date:   2020-09-09
 * @Version: V1.0
 */
public interface BiVisualMapper extends BaseMapper<BiVisual> {

}
