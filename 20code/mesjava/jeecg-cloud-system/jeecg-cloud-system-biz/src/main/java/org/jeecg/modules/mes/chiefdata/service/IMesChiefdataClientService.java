package org.jeecg.modules.mes.chiefdata.service;

import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataClient;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 主数据—客户
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
public interface IMesChiefdataClientService extends IService<MesChiefdataClient> {

    Result<?> add(MesChiefdataClient mesChiefdataClient);

    Result<?> edit(MesChiefdataClient mesChiefdataClient);
}
