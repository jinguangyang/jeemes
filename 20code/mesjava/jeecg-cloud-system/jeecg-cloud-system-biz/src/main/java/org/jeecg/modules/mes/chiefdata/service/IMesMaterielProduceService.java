package org.jeecg.modules.mes.chiefdata.service;

import org.jeecg.modules.mes.chiefdata.entity.MesMaterielProduce;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 物料—生产
 * @Author: jeecg-boot
 * @Date:   2020-10-16
 * @Version: V1.0
 */
public interface IMesMaterielProduceService extends IService<MesMaterielProduce> {

	public List<MesMaterielProduce> selectByMainId(String mainId);
}
