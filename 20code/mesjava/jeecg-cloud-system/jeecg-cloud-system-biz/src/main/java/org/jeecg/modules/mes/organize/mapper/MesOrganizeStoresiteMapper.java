package org.jeecg.modules.mes.organize.mapper;

import java.util.List;
import org.jeecg.modules.mes.organize.entity.MesOrganizeStoresite;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 组织—工厂库存点
 * @Author: jeecg-boot
 * @Date:   2020-10-19
 * @Version: V1.0
 */
public interface MesOrganizeStoresiteMapper extends BaseMapper<MesOrganizeStoresite> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<MesOrganizeStoresite> selectByMainId(@Param("mainId") String mainId);

}
