package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataClassbatch;
import org.jeecg.modules.mes.chiefdata.entity.MesClassbatchTimetable;
import org.jeecg.modules.mes.chiefdata.mapper.MesClassbatchTimetableMapper;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataClassbatchMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataClassbatchService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Collection;

/**
 * @Description: 主数据—班组
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Service
public class MesChiefdataClassbatchServiceImpl extends ServiceImpl<MesChiefdataClassbatchMapper, MesChiefdataClassbatch> implements IMesChiefdataClassbatchService {

	@Autowired
	private MesChiefdataClassbatchMapper mesChiefdataClassbatchMapper;
	@Autowired
	private MesClassbatchTimetableMapper mesClassbatchTimetableMapper;
	
	@Override
	@Transactional
	public void saveMain(MesChiefdataClassbatch mesChiefdataClassbatch, List<MesClassbatchTimetable> mesClassbatchTimetableList) {
		mesChiefdataClassbatchMapper.insert(mesChiefdataClassbatch);
		if(mesClassbatchTimetableList!=null && mesClassbatchTimetableList.size()>0) {
			for(MesClassbatchTimetable entity:mesClassbatchTimetableList) {
				//外键设置
				entity.setClassId(mesChiefdataClassbatch.getId());
				mesClassbatchTimetableMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void updateMain(MesChiefdataClassbatch mesChiefdataClassbatch,List<MesClassbatchTimetable> mesClassbatchTimetableList) {
		mesChiefdataClassbatchMapper.updateById(mesChiefdataClassbatch);
		
		//1.先删除子表数据
		mesClassbatchTimetableMapper.deleteByMainId(mesChiefdataClassbatch.getId());
		
		//2.子表数据重新插入
		if(mesClassbatchTimetableList!=null && mesClassbatchTimetableList.size()>0) {
			for(MesClassbatchTimetable entity:mesClassbatchTimetableList) {
				//外键设置
				entity.setClassId(mesChiefdataClassbatch.getId());
				mesClassbatchTimetableMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void delMain(String id) {
		mesClassbatchTimetableMapper.deleteByMainId(id);
		mesChiefdataClassbatchMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			mesClassbatchTimetableMapper.deleteByMainId(id.toString());
			mesChiefdataClassbatchMapper.deleteById(id);
		}
	}
	
}
