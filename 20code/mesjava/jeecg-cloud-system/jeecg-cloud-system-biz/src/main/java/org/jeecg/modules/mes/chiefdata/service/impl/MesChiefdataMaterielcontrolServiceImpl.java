package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataMaterielcontrol;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataMaterielcontrolMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataMaterielcontrolService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 主数据—物料管控
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Service
public class MesChiefdataMaterielcontrolServiceImpl extends ServiceImpl<MesChiefdataMaterielcontrolMapper, MesChiefdataMaterielcontrol> implements IMesChiefdataMaterielcontrolService {

}
