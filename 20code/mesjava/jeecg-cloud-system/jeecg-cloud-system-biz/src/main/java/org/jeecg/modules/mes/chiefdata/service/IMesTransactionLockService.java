package org.jeecg.modules.mes.chiefdata.service;

import org.jeecg.modules.mes.chiefdata.entity.MesTransactionLock;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 业务锁定
 * @Author: jeecg-boot
 * @Date:   2020-10-24
 * @Version: V1.0
 */
public interface IMesTransactionLockService extends IService<MesTransactionLock> {

}
