package org.jeecg.modules.mes.chiefdata.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.jeecg.modules.mes.chiefdata.entity.MesMaterielRedgum;
import org.jeecg.modules.mes.chiefdata.mapper.MesMaterielRedgumMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesMaterielRedgumService;
import org.springframework.stereotype.Service;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 主数据—辅料-红胶/锡膏
 * @Author: jeecg-boot
 * @Date: 2020-10-16
 * @Version: V1.0
 */
@Service
public class MesMaterielRedgumServiceImpl extends ServiceImpl<MesMaterielRedgumMapper, MesMaterielRedgum> implements IMesMaterielRedgumService {

    @Autowired
    private MesMaterielRedgumMapper mesMaterielRedgumMapper;

    @Override
    public List<MesMaterielRedgum> selectByMainId(String mainId) {
        return mesMaterielRedgumMapper.selectByMainId(mainId);
    }

    @Override
    public MesMaterielRedgum findBypCode(String pCode) {
        return baseMapper.findBymId(pCode);

    }
}
