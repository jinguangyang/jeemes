package org.jeecg.modules.biconf.service.impl;

import org.jeecg.modules.biconf.entity.BiVisualCategory;
import org.jeecg.modules.biconf.mapper.BiVisualCategoryMapper;
import org.jeecg.modules.biconf.service.IBiVisualCategoryService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 大屏分类
 * @Author: jeecg-boot
 * @Date:   2020-09-09
 * @Version: V1.0
 */
@Service
public class BiVisualCategoryServiceImpl extends ServiceImpl<BiVisualCategoryMapper, BiVisualCategory> implements IBiVisualCategoryService {

}
