package org.jeecg.modules.mes.chiefdata.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.ApplyBillDBUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.mes.chiefdata.entity.*;
import org.jeecg.modules.mes.chiefdata.service.*;
import org.jeecg.modules.mes.chiefdata.vo.MesChiefdataMaterielPage;
import org.jeecg.modules.mes.client.ProduceClient;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

 /**
 * @Description: 主数据—物料
 * @Author: jeecg-boot
 * @Date:   2020-10-16
 * @Version: V1.0
 */
@Api(tags="主数据—物料")
@RestController
@RequestMapping("/chiefdata/mesChiefdataMateriel")
@Slf4j
public class MesChiefdataMaterielController {
	@Autowired
	private IMesChiefdataMaterielService mesChiefdataMaterielService;
	@Autowired
	private IMesMaterielPcbService mesMaterielPcbService;
	@Autowired
	private IMesMaterielNetknifeService mesMaterielNetknifeService;
	@Autowired
	private IMesMaterielProduceService mesMaterielProduceService;
	@Autowired
	private IMesMaterielRedgumService mesMaterielRedgumService;
	@Autowired
	private IMesMaterielQualityService mesMaterielQualityService;
	@Autowired
	private IMesMaterielStorageService mesMaterielStorageService;
	@Autowired
	private IMesMaterielOtherService mesMaterielOtherService;
	@Autowired
	ProduceClient produceClient;

	/**
	 * 分页列表查询
	 *
	 * @param mesChiefdataMateriel
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "主数据—物料-分页列表查询")
	@ApiOperation(value="主数据—物料-分页列表查询", notes="主数据—物料-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesChiefdataMateriel mesChiefdataMateriel,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesChiefdataMateriel> queryWrapper = QueryGenerator.initQueryWrapper(mesChiefdataMateriel, req.getParameterMap());
		Page<MesChiefdataMateriel> page = new Page<MesChiefdataMateriel>(pageNo, pageSize);
		IPage<MesChiefdataMateriel> pageList = mesChiefdataMaterielService.page(page, queryWrapper);
		List<MesChiefdataMateriel> materielList = pageList.getRecords();
		if(materielList.size() != 0){
			for (MesChiefdataMateriel materiel : materielList) {
				if(StringUtils.isBlank(materiel.getImportState())){
					materiel.setImportState("0");
					mesChiefdataMaterielService.updateById(materiel);
				}
			}
		}
		IPage<MesChiefdataMateriel> pageList1 = mesChiefdataMaterielService.page(page, queryWrapper);
		return Result.ok(pageList1);
	}


	 @AutoLog(value = "主数据—物料-获取半成品成品列表")
	 @ApiOperation(value="主数据—物料-获取半成品成品列表", notes="主数据—物料-获取半成品成品列表")
	 @GetMapping(value = "/getCpList")
	 public Result<?> getCpList() {
		 QueryWrapper<MesChiefdataMateriel> w1 =new QueryWrapper<>();
		 w1.select("materiel_code","concat(materiel_code,'-',product_name) as product_name");
		 w1.in("materiel_type","成品","半成品");
		 List<MesChiefdataMateriel> list = mesChiefdataMaterielService.list(w1);
		 return Result.ok(list);
	 }

	 @AutoLog(value = "主数据—物料-首页获取半成品成品列表")
	 @ApiOperation(value="主数据—物料-首页获取半成品成品列表", notes="主数据—物料-首页获取半成品成品列表")
	 @GetMapping(value = "/getDashCpList")
	 public Result<?> getDashCpList() {
		 QueryWrapper<MesChiefdataMateriel> w1 =new QueryWrapper<>();
		 w1.select("materiel_code","product_name");
		 w1.in("materiel_type","成品","半成品");
		 List<MesChiefdataMateriel> list = mesChiefdataMaterielService.list(w1);
		 return Result.ok(list);
	 }

	/**
	 *   添加
	 *
	 * @param mesChiefdataMaterielPage
	 * @return
	 */
	@AutoLog(value = "主数据—物料-添加")
	@ApiOperation(value="主数据—物料-添加", notes="主数据—物料-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MesChiefdataMaterielPage mesChiefdataMaterielPage) {
		MesChiefdataMateriel mesChiefdataMateriel = new MesChiefdataMateriel();
		BeanUtils.copyProperties(mesChiefdataMaterielPage, mesChiefdataMateriel);
		mesChiefdataMateriel.setImportState("0");
		mesChiefdataMaterielService.saveMain(mesChiefdataMateriel, mesChiefdataMaterielPage.getMesMaterielPcbList(),mesChiefdataMaterielPage.getMesMaterielNetknifeList(),mesChiefdataMaterielPage.getMesMaterielProduceList(),mesChiefdataMaterielPage.getMesMaterielRedgumList(),mesChiefdataMaterielPage.getMesMaterielQualityList(),mesChiefdataMaterielPage.getMesMaterielStorageList(),mesChiefdataMaterielPage.getMesMaterielOtherList());
		return Result.ok("添加成功！");
	}

	 @AutoLog(value = "主数据—物料-导入关务系统")
	 @ApiOperation(value="主数据—物料-导入关务系统", notes="主数据—物料-导入关务系统")
	 @GetMapping(value = "/importMaterielSqlServer")
	 public Result<?> importMaterielSqlServer(@RequestParam(name = "id", required = true) String id) {
		QueryWrapper<MesChiefdataMateriel> queryWrapper = new QueryWrapper<>();
		queryWrapper.eq("import_state","0");//是否导入关务系统，若已导入，则为1,反之为0
		queryWrapper.eq("id",id);
		 List<MesChiefdataMateriel> materielList = mesChiefdataMaterielService.list(queryWrapper);
		 System.err.println("物料导入关务系统:"+materielList);
		 System.err.println("物料导入关务系统数量:"+materielList.size());
		 if(materielList.size() != 0){
			 for (MesChiefdataMateriel materiel : materielList) {
				 ApplyBillDBUtil.importMaterielSqlServer(materiel);
				 materiel.setImportState("1");
				 mesChiefdataMaterielService.updateById(materiel);
			 }
		 }else {
			 return Result.ok("import_state导入状态为0 的物料数据未查到！请检查！");
		 }
		 return Result.ok("导入成功！");
	 }

	/**
	 *  编辑
	 *
	 * @param mesChiefdataMaterielPage
	 * @return
	 */
	@AutoLog(value = "主数据—物料-编辑")
	@ApiOperation(value="主数据—物料-编辑", notes="主数据—物料-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody MesChiefdataMaterielPage mesChiefdataMaterielPage) {
		MesChiefdataMateriel mesChiefdataMateriel = new MesChiefdataMateriel();
		BeanUtils.copyProperties(mesChiefdataMaterielPage, mesChiefdataMateriel);
		MesChiefdataMateriel mesChiefdataMaterielEntity = mesChiefdataMaterielService.getById(mesChiefdataMateriel.getId());
		if(mesChiefdataMaterielEntity==null) {
			return Result.error("未找到对应数据");
		}
		mesChiefdataMaterielService.updateMain(mesChiefdataMateriel, mesChiefdataMaterielPage.getMesMaterielPcbList(),mesChiefdataMaterielPage.getMesMaterielNetknifeList(),mesChiefdataMaterielPage.getMesMaterielProduceList(),mesChiefdataMaterielPage.getMesMaterielRedgumList(),mesChiefdataMaterielPage.getMesMaterielQualityList(),mesChiefdataMaterielPage.getMesMaterielStorageList(),mesChiefdataMaterielPage.getMesMaterielOtherList());
		return Result.ok("编辑成功!");
	}

	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "主数据—物料-通过id删除")
	@ApiOperation(value="主数据—物料-通过id删除", notes="主数据—物料-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		mesChiefdataMaterielService.delMain(id);
		return Result.ok("删除成功!");
	}

	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "主数据—物料-批量删除")
	@ApiOperation(value="主数据—物料-批量删除", notes="主数据—物料-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.mesChiefdataMaterielService.delBatchMain(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功！");
	}

	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "主数据—物料-通过id查询")
	@ApiOperation(value = "主数据—物料-通过id查询", notes = "主数据—物料-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
		MesChiefdataMateriel mesChiefdataMateriel = mesChiefdataMaterielService.getById(id);
		if (mesChiefdataMateriel == null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(mesChiefdataMateriel);
	}

	 /**
	  * 远程调用 用物料code查出物料pcb信息
	  * @param materielCode
	  * @return
	  */
	 @GetMapping(value = "/selectByMaterielPcb")
	 public List<MesMaterielPcb> selectByMaterielPcb(@RequestParam(name = "materielCode", required = true) String materielCode) {
		 List<MesMaterielPcb> mesMaterielPcbs = new ArrayList<>();
	 	 QueryWrapper<MesChiefdataMateriel> queryWrapper = new QueryWrapper<>();
	 	 queryWrapper.eq("materiel_code",materielCode);
		 MesChiefdataMateriel mesChiefdataMateriel = mesChiefdataMaterielService.getOne(queryWrapper);
		 mesMaterielPcbs = mesMaterielPcbService.selectByMainId(mesChiefdataMateriel.getId());
		 return mesMaterielPcbs;
	 }

	 /**
	  * 通过商品编码查询物料
	  *
	  * @param pCode 商品编码
	  * @return
	  */
	 @AutoLog(value = "主数据—物料-通过商品编码查询")
	 @ApiOperation(value = "主数据—物料-通过商品编码查询", notes = "主数据—物料-通过商品编码查询")
	 @GetMapping(value = "/queryByProductCode")
	 public MesChiefdataMateriel queryByProductCode(@RequestParam(name = "pCode") String pCode) {
		 LambdaQueryWrapper<MesChiefdataMateriel> wrapper = new LambdaQueryWrapper();
		 wrapper.eq(MesChiefdataMateriel::getMaterielCode, pCode);
		 MesChiefdataMateriel mesChiefdataMateriel = mesChiefdataMaterielService.getOne(wrapper);
		 if (mesChiefdataMateriel == null) {
			 return null;
		 }
		 return mesChiefdataMateriel;
	 }

	 /**
	  * 通过料号查询
	  *
	  * @param mCode
	  * @return
	  */
	 @AutoLog(value = "主数据—物料-通过料号查询")
	 @ApiOperation(value = "主数据—物料-通过料号查询", notes = "主数据—物料-通过料号查询")
	 @GetMapping(value = "/queryByMcode")
	 public MesChiefdataMateriel queryByMcode(@RequestParam(name="mCode",required=true) String mCode) {
		 QueryWrapper<MesChiefdataMateriel> queryWrapper = new QueryWrapper<>();
		 queryWrapper.eq("materiel_code", mCode);
		 List<MesChiefdataMateriel> mesChiefDataMateriel = mesChiefdataMaterielService.list(queryWrapper);
		 if (com.epms.util.ObjectHelper.isEmpty(mesChiefDataMateriel)) {
			 return null;
		 }
		 return mesChiefDataMateriel.get(0);
	 }

	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "物料—PCB通过主表ID查询")
	@ApiOperation(value="物料—PCB主表ID查询", notes="物料—PCB-通主表ID查询")
	@GetMapping(value = "/queryMesMaterielPcbByMainId")
	public Result<?> queryMesMaterielPcbListByMainId(@RequestParam(name="id",required=true) String id) {
		List<MesMaterielPcb> mesMaterielPcbList = mesMaterielPcbService.selectByMainId(id);
		return Result.ok(mesMaterielPcbList);
	}
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "主数据—辅料-钢网/刮刀通过主表ID查询")
	@ApiOperation(value="主数据—辅料-钢网/刮刀主表ID查询", notes="主数据—辅料-钢网/刮刀-通主表ID查询")
	@GetMapping(value = "/queryMesMaterielNetknifeByMainId")
	public Result<?> queryMesMaterielNetknifeListByMainId(@RequestParam(name="id",required=true) String id) {
		List<MesMaterielNetknife> mesMaterielNetknifeList = mesMaterielNetknifeService.selectByMainId(id);
		return Result.ok(mesMaterielNetknifeList);
	}
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "物料—生产通过主表ID查询")
	@ApiOperation(value="物料—生产主表ID查询", notes="物料—生产-通主表ID查询")
	@GetMapping(value = "/queryMesMaterielProduceByMainId")
	public Result<?> queryMesMaterielProduceListByMainId(@RequestParam(name="id",required=true) String id) {
		List<MesMaterielProduce> mesMaterielProduceList = mesMaterielProduceService.selectByMainId(id);
		return Result.ok(mesMaterielProduceList);
	}
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "主数据—辅料-红胶/锡膏通过主表ID查询")
	@ApiOperation(value="主数据—辅料-红胶/锡膏主表ID查询", notes="主数据—辅料-红胶/锡膏-通主表ID查询")
	@GetMapping(value = "/queryMesMaterielRedgumByMainId")
	public Result<?> queryMesMaterielRedgumListByMainId(@RequestParam(name="id",required=true) String id) {
		List<MesMaterielRedgum> mesMaterielRedgumList = mesMaterielRedgumService.selectByMainId(id);
		return Result.ok(mesMaterielRedgumList);
	}
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "物料—品质通过主表ID查询")
	@ApiOperation(value="物料—品质主表ID查询", notes="物料—品质-通主表ID查询")
	@GetMapping(value = "/queryMesMaterielQualityByMainId")
	public Result<?> queryMesMaterielQualityListByMainId(@RequestParam(name="id",required=true) String id) {
		List<MesMaterielQuality> mesMaterielQualityList = mesMaterielQualityService.selectByMainId(id);
		return Result.ok(mesMaterielQualityList);
	}
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "物料—仓储通过主表ID查询")
	@ApiOperation(value="物料—仓储主表ID查询", notes="物料—仓储-通主表ID查询")
	@GetMapping(value = "/queryMesMaterielStorageByMainId")
	public Result<?> queryMesMaterielStorageListByMainId(@RequestParam(name="id",required=true) String id) {
		List<MesMaterielStorage> mesMaterielStorageList = mesMaterielStorageService.selectByMainId(id);
		return Result.ok(mesMaterielStorageList);
	}
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "物料—其他通过主表ID查询")
	@ApiOperation(value="物料—其他主表ID查询", notes="物料—其他-通主表ID查询")
	@GetMapping(value = "/queryMesMaterielOtherByMainId")
	public Result<?> queryMesMaterielOtherListByMainId(@RequestParam(name="id",required=true) String id) {
		List<MesMaterielOther> mesMaterielOtherList = mesMaterielOtherService.selectByMainId(id);
		return Result.ok(mesMaterielOtherList);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param mesChiefdataMateriel
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesChiefdataMateriel mesChiefdataMateriel) {
      // Step.1 组装查询条件查询数据
      QueryWrapper<MesChiefdataMateriel> queryWrapper = QueryGenerator.initQueryWrapper(mesChiefdataMateriel, request.getParameterMap());
      LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

      //Step.2 获取导出数据
      List<MesChiefdataMateriel> queryList = mesChiefdataMaterielService.list(queryWrapper);
      // 过滤选中数据
      String selections = request.getParameter("selections");
      List<MesChiefdataMateriel> mesChiefdataMaterielList = new ArrayList<MesChiefdataMateriel>();
      if(oConvertUtils.isEmpty(selections)) {
          mesChiefdataMaterielList = queryList;
      }else {
          List<String> selectionList = Arrays.asList(selections.split(","));
          mesChiefdataMaterielList = queryList.stream().filter(item -> selectionList.contains(item.getId())).collect(Collectors.toList());
      }

      // Step.3 组装pageList
      List<MesChiefdataMaterielPage> pageList = new ArrayList<MesChiefdataMaterielPage>();
      for (MesChiefdataMateriel main : mesChiefdataMaterielList) {
          MesChiefdataMaterielPage vo = new MesChiefdataMaterielPage();
          BeanUtils.copyProperties(main, vo);
          List<MesMaterielPcb> mesMaterielPcbList = mesMaterielPcbService.selectByMainId(main.getId());
          vo.setMesMaterielPcbList(mesMaterielPcbList);
          List<MesMaterielNetknife> mesMaterielNetknifeList = mesMaterielNetknifeService.selectByMainId(main.getId());
          vo.setMesMaterielNetknifeList(mesMaterielNetknifeList);
          List<MesMaterielProduce> mesMaterielProduceList = mesMaterielProduceService.selectByMainId(main.getId());
          vo.setMesMaterielProduceList(mesMaterielProduceList);
          List<MesMaterielRedgum> mesMaterielRedgumList = mesMaterielRedgumService.selectByMainId(main.getId());
          vo.setMesMaterielRedgumList(mesMaterielRedgumList);
          List<MesMaterielQuality> mesMaterielQualityList = mesMaterielQualityService.selectByMainId(main.getId());
          vo.setMesMaterielQualityList(mesMaterielQualityList);
          List<MesMaterielStorage> mesMaterielStorageList = mesMaterielStorageService.selectByMainId(main.getId());
          vo.setMesMaterielStorageList(mesMaterielStorageList);
          List<MesMaterielOther> mesMaterielOtherList = mesMaterielOtherService.selectByMainId(main.getId());
          vo.setMesMaterielOtherList(mesMaterielOtherList);
          pageList.add(vo);
      }

      // Step.4 AutoPoi 导出Excel
      ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
      mv.addObject(NormalExcelConstants.FILE_NAME, "主数据—物料列表");
      mv.addObject(NormalExcelConstants.CLASS, MesChiefdataMaterielPage.class);
      mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("主数据—物料数据", "导出人:"+sysUser.getRealname(), "主数据—物料"));
      mv.addObject(NormalExcelConstants.DATA_LIST, pageList);
      return mv;
    }

    /**
    * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
      MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
      Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
      for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
          MultipartFile file = entity.getValue();// 获取上传文件对象
          ImportParams params = new ImportParams();
          params.setTitleRows(2);
          params.setHeadRows(1);
          params.setNeedSave(true);
          try {
			  int i = 0;
			  List<MesChiefdataMaterielPage> list = ExcelImportUtil.importExcel(file.getInputStream(), MesChiefdataMaterielPage.class, params);

			  int successNum = 0;
			  int failureNum = 0;
			  StringBuilder successMsg = new StringBuilder();
			  StringBuilder failureMsg = new StringBuilder();

			  for (MesChiefdataMaterielPage page : list) {
				  if (StringUtils.isNotBlank(page.getMaterielCode())) {

					  String mCode = page.getMaterielCode();
					  if (StringUtils.isBlank(page.getUnit()) || StringUtils.isBlank(page.getMaterielType())) {
						  failureNum++;
						  failureMsg.append(failureNum + "料号为：" + mCode + " 的物料类型和单位都不能为空！导入失败");
						  continue;
					  }
					  //去掉料号首尾空格
					  String tmCode = mCode.trim();
					  QueryWrapper<MesChiefdataMateriel> queryWrapper = new QueryWrapper<>();
					  queryWrapper.eq("materiel_code", tmCode);
					  MesChiefdataMateriel materiel = mesChiefdataMaterielService.getOne(queryWrapper);
					if (materiel == null) {
						MesChiefdataMateriel po = new MesChiefdataMateriel();
						BeanUtils.copyProperties(page, po);
						po.setImportState("0");
						mesChiefdataMaterielService.saveMain(po, page.getMesMaterielPcbList(), page.getMesMaterielNetknifeList(), page.getMesMaterielProduceList(), page.getMesMaterielRedgumList(), page.getMesMaterielQualityList(), page.getMesMaterielStorageList(), page.getMesMaterielOtherList());
						i += 1;

						successNum++;
						successMsg.append(successNum + "料号为：" + mCode + " 的物料 导入成功");
					} else {
						failureNum++;
						failureMsg.append(failureNum + "料号为：" + mCode + " 的物料已重复！导入失败");
					}
				  }
			  }

			  if (failureNum > 0) {
				  failureMsg.insert(0, "成功" + successNum + "条；失败！共 " + failureNum + " 条物料，料号如下：");
				  return Result.error(failureMsg.toString());
			  } else {
				  successMsg.insert(0, "恭喜您，数据已全部导入成功！共 " + successNum + " 条，数据如下：");
				  return Result.ok(successMsg.toString());
			  }

		  } catch (Exception e) {
              log.error(e.getMessage(),e);
              return Result.error("文件导入失败:"+e.getMessage());
          } finally {
              try {
                  file.getInputStream().close();
              } catch (IOException e) {
                  e.printStackTrace();
              }
          }
      }
      return Result.ok("文件导入失败！");
    }

}
