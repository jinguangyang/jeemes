package org.jeecg.modules.mes.organize.service;

import org.jeecg.modules.mes.organize.entity.MesOrganizeStoresite;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 组织—工厂库存点
 * @Author: jeecg-boot
 * @Date:   2020-10-19
 * @Version: V1.0
 */
public interface IMesOrganizeStoresiteService extends IService<MesOrganizeStoresite> {

	public List<MesOrganizeStoresite> selectByMainId(String mainId);
}
