package org.jeecg.modules.mes.chiefdata.controller;

import java.io.UnsupportedEncodingException;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.vo.LoginUser;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.mes.chiefdata.entity.MesMaterialtableItem;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataMaterialtable;
import org.jeecg.modules.mes.chiefdata.vo.MesChiefdataMaterialtablePage;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataMaterialtableService;
import org.jeecg.modules.mes.chiefdata.service.IMesMaterialtableItemService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 主数据—料表管理
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Api(tags="主数据—料表管理")
@RestController
@RequestMapping("/chiefdata/mesChiefdataMaterialtable")
@Slf4j
public class MesChiefdataMaterialtableController {
	@Autowired
	private IMesChiefdataMaterialtableService mesChiefdataMaterialtableService;
	@Autowired
	private IMesMaterialtableItemService mesMaterialtableItemService;
	
	/**
	 * 分页列表查询
	 *
	 * @param mesChiefdataMaterialtable
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "主数据—料表管理-分页列表查询")
	@ApiOperation(value="主数据—料表管理-分页列表查询", notes="主数据—料表管理-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesChiefdataMaterialtable mesChiefdataMaterialtable,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesChiefdataMaterialtable> queryWrapper = QueryGenerator.initQueryWrapper(mesChiefdataMaterialtable, req.getParameterMap());
		Page<MesChiefdataMaterialtable> page = new Page<MesChiefdataMaterialtable>(pageNo, pageSize);
		IPage<MesChiefdataMaterialtable> pageList = mesChiefdataMaterialtableService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param mesChiefdataMaterialtablePage
	 * @return
	 */
	@AutoLog(value = "主数据—料表管理-添加")
	@ApiOperation(value="主数据—料表管理-添加", notes="主数据—料表管理-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MesChiefdataMaterialtablePage mesChiefdataMaterialtablePage) {
		MesChiefdataMaterialtable mesChiefdataMaterialtable = new MesChiefdataMaterialtable();
		BeanUtils.copyProperties(mesChiefdataMaterialtablePage, mesChiefdataMaterialtable);
		mesChiefdataMaterialtableService.saveMain(mesChiefdataMaterialtable, mesChiefdataMaterialtablePage.getMesMaterialtableItemList());
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param mesChiefdataMaterialtablePage
	 * @return
	 */
	@AutoLog(value = "主数据—料表管理-编辑")
	@ApiOperation(value="主数据—料表管理-编辑", notes="主数据—料表管理-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody MesChiefdataMaterialtablePage mesChiefdataMaterialtablePage) {
		MesChiefdataMaterialtable mesChiefdataMaterialtable = new MesChiefdataMaterialtable();
		BeanUtils.copyProperties(mesChiefdataMaterialtablePage, mesChiefdataMaterialtable);
		MesChiefdataMaterialtable mesChiefdataMaterialtableEntity = mesChiefdataMaterialtableService.getById(mesChiefdataMaterialtable.getId());
		if(mesChiefdataMaterialtableEntity==null) {
			return Result.error("未找到对应数据");
		}
		mesChiefdataMaterialtableService.updateMain(mesChiefdataMaterialtable, mesChiefdataMaterialtablePage.getMesMaterialtableItemList());
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "主数据—料表管理-通过id删除")
	@ApiOperation(value="主数据—料表管理-通过id删除", notes="主数据—料表管理-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		mesChiefdataMaterialtableService.delMain(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "主数据—料表管理-批量删除")
	@ApiOperation(value="主数据—料表管理-批量删除", notes="主数据—料表管理-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.mesChiefdataMaterialtableService.delBatchMain(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功！");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "主数据—料表管理-通过id查询")
	@ApiOperation(value="主数据—料表管理-通过id查询", notes="主数据—料表管理-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		MesChiefdataMaterialtable mesChiefdataMaterialtable = mesChiefdataMaterialtableService.getById(id);
		if(mesChiefdataMaterialtable==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(mesChiefdataMaterialtable);

	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "主数据—料站表明细通过主表ID查询")
	@ApiOperation(value="主数据—料站表明细主表ID查询", notes="主数据—料站表明细-通主表ID查询")
	@GetMapping(value = "/queryMesMaterialtableItemByMainId")
	public Result<?> queryMesMaterialtableItemListByMainId(@RequestParam(name="id",required=true) String id) {
		List<MesMaterialtableItem> mesMaterialtableItemList = mesMaterialtableItemService.selectByMainId(id);
		return Result.ok(mesMaterialtableItemList);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param mesChiefdataMaterialtable
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesChiefdataMaterialtable mesChiefdataMaterialtable) {
      // Step.1 组装查询条件查询数据
      QueryWrapper<MesChiefdataMaterialtable> queryWrapper = QueryGenerator.initQueryWrapper(mesChiefdataMaterialtable, request.getParameterMap());
      LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

      //Step.2 获取导出数据
      List<MesChiefdataMaterialtable> queryList = mesChiefdataMaterialtableService.list(queryWrapper);
      // 过滤选中数据
      String selections = request.getParameter("selections");
      List<MesChiefdataMaterialtable> mesChiefdataMaterialtableList = new ArrayList<MesChiefdataMaterialtable>();
      if(oConvertUtils.isEmpty(selections)) {
          mesChiefdataMaterialtableList = queryList;
      }else {
          List<String> selectionList = Arrays.asList(selections.split(","));
          mesChiefdataMaterialtableList = queryList.stream().filter(item -> selectionList.contains(item.getId())).collect(Collectors.toList());
      }

      // Step.3 组装pageList
      List<MesChiefdataMaterialtablePage> pageList = new ArrayList<MesChiefdataMaterialtablePage>();
      for (MesChiefdataMaterialtable main : mesChiefdataMaterialtableList) {
          MesChiefdataMaterialtablePage vo = new MesChiefdataMaterialtablePage();
          BeanUtils.copyProperties(main, vo);
          List<MesMaterialtableItem> mesMaterialtableItemList = mesMaterialtableItemService.selectByMainId(main.getId());
          vo.setMesMaterialtableItemList(mesMaterialtableItemList);
          pageList.add(vo);
      }

      // Step.4 AutoPoi 导出Excel
      ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
      mv.addObject(NormalExcelConstants.FILE_NAME, "主数据—料表管理列表");
      mv.addObject(NormalExcelConstants.CLASS, MesChiefdataMaterialtablePage.class);
      mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("主数据—料表管理数据", "导出人:"+sysUser.getRealname(), "主数据—料表管理"));
      mv.addObject(NormalExcelConstants.DATA_LIST, pageList);
      return mv;
    }

    /**
    * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
      MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
      Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
      for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
          MultipartFile file = entity.getValue();// 获取上传文件对象
          ImportParams params = new ImportParams();
          params.setTitleRows(2);
          params.setHeadRows(1);
          params.setNeedSave(true);
          try {
              List<MesChiefdataMaterialtablePage> list = ExcelImportUtil.importExcel(file.getInputStream(), MesChiefdataMaterialtablePage.class, params);
              for (MesChiefdataMaterialtablePage page : list) {
                  MesChiefdataMaterialtable po = new MesChiefdataMaterialtable();
                  BeanUtils.copyProperties(page, po);
                  mesChiefdataMaterialtableService.saveMain(po, page.getMesMaterialtableItemList());
              }
              return Result.ok("文件导入成功！数据行数:" + list.size());
          } catch (Exception e) {
              log.error(e.getMessage(),e);
              return Result.error("文件导入失败:"+e.getMessage());
          } finally {
              try {
                  file.getInputStream().close();
              } catch (IOException e) {
                  e.printStackTrace();
              }
          }
      }
      return Result.ok("文件导入失败！");
    }

}
