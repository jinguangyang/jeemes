package org.jeecg.modules.mes.chiefdata.service;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataClientgroup;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 主数据—客户组
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
public interface IMesChiefdataClientgroupService extends IService<MesChiefdataClientgroup> {

}
