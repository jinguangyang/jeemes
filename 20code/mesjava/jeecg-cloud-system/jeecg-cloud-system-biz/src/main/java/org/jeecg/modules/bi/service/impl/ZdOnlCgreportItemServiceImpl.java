package org.jeecg.modules.bi.service.impl;

import org.jeecg.modules.bi.entity.zdOnlCgreportItem;
import org.jeecg.modules.bi.mapper.zdOnlCgreportItemMapper;
import org.jeecg.modules.bi.service.IzdOnlCgreportItemService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: onl_cgreport_item
 * @Author: wms-cloud
 * @Date:   2020-12-09
 * @Version: V1.0
 */
@Service
public class ZdOnlCgreportItemServiceImpl extends ServiceImpl<zdOnlCgreportItemMapper, zdOnlCgreportItem> implements IzdOnlCgreportItemService {
	
	@Autowired
	private zdOnlCgreportItemMapper zdOnlCgreportItemMapper;
	
	@Override
	public List<zdOnlCgreportItem> selectByMainId(String mainId) {
		return zdOnlCgreportItemMapper.selectByMainId(mainId);
	}
}
