package org.jeecg.modules.quartz.job;

import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.system.api.ISysBaseAPI;
import org.jeecg.modules.mes.client.TransactionClient;
import org.jeecg.modules.mes.order.entity.MesOrderProduce;
import org.jeecg.modules.message.service.ISysMessageService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

/**
 * @Package org.jeecg.modules.quartz.job
 * @date 2021/4/22 22:36
 * @description
 */
@Slf4j
public class ProduceRemindJob implements Job {

    @Autowired
    private TransactionClient transactionClient;
    @Autowired
    private ISysBaseAPI sysBaseAPI;

    /**
     * 定时提醒用户有没有入库完成
     * @param context
     * @throws JobExecutionException
     */
    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {

        List<MesOrderProduce> mesOrderProduceList = transactionClient.getPlanedOrderProduceList();
        Calendar calendar = Calendar.getInstance();
        int flag = Integer.parseInt(new SimpleDateFormat("MM").format(calendar.getTime())) % 30;
        if (mesOrderProduceList.size() > 0 && flag == 0) {
            for (MesOrderProduce mesOrderProduce : mesOrderProduceList) {
                sysBaseAPI.sendSysAnnouncement("admin",mesOrderProduce.getCreateBy(),"订单超时提醒"
                        ,"订单已超时,订单号:"+mesOrderProduce.getOrderCode());
            }
        }
    }
}

