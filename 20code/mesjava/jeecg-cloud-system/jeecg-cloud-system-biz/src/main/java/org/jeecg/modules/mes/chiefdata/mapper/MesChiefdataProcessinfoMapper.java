package org.jeecg.modules.mes.chiefdata.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataProcessinfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 主数据—工序信息
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
public interface MesChiefdataProcessinfoMapper extends BaseMapper<MesChiefdataProcessinfo> {

}
