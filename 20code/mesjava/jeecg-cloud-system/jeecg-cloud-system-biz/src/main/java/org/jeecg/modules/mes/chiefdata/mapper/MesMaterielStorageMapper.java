package org.jeecg.modules.mes.chiefdata.mapper;

import java.util.List;
import org.jeecg.modules.mes.chiefdata.entity.MesMaterielStorage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 物料—仓储
 * @Author: jeecg-boot
 * @Date:   2020-10-16
 * @Version: V1.0
 */
public interface MesMaterielStorageMapper extends BaseMapper<MesMaterielStorage> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<MesMaterielStorage> selectByMainId(@Param("mainId") String mainId);
}
