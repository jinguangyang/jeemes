package org.jeecg.modules.mes.chiefdata.service;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataSupplier;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 主数据—供应商
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
public interface IMesChiefdataSupplierService extends IService<MesChiefdataSupplier> {

}
