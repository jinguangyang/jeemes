package org.jeecg.modules.mes.chiefdata.controller;

import java.io.UnsupportedEncodingException;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.vo.LoginUser;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.mes.chiefdata.entity.MesSortformulaStrategyinfo;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataSortformula;
import org.jeecg.modules.mes.chiefdata.vo.MesChiefdataSortformulaPage;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataSortformulaService;
import org.jeecg.modules.mes.chiefdata.service.IMesSortformulaStrategyinfoService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 主数据—拣货方案
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Api(tags="主数据—拣货方案")
@RestController
@RequestMapping("/chiefdata/mesChiefdataSortformula")
@Slf4j
public class MesChiefdataSortformulaController {
	@Autowired
	private IMesChiefdataSortformulaService mesChiefdataSortformulaService;
	@Autowired
	private IMesSortformulaStrategyinfoService mesSortformulaStrategyinfoService;
	
	/**
	 * 分页列表查询
	 *
	 * @param mesChiefdataSortformula
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "主数据—拣货方案-分页列表查询")
	@ApiOperation(value="主数据—拣货方案-分页列表查询", notes="主数据—拣货方案-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesChiefdataSortformula mesChiefdataSortformula,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesChiefdataSortformula> queryWrapper = QueryGenerator.initQueryWrapper(mesChiefdataSortformula, req.getParameterMap());
		Page<MesChiefdataSortformula> page = new Page<MesChiefdataSortformula>(pageNo, pageSize);
		IPage<MesChiefdataSortformula> pageList = mesChiefdataSortformulaService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param mesChiefdataSortformulaPage
	 * @return
	 */
	@AutoLog(value = "主数据—拣货方案-添加")
	@ApiOperation(value="主数据—拣货方案-添加", notes="主数据—拣货方案-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MesChiefdataSortformulaPage mesChiefdataSortformulaPage) {
		MesChiefdataSortformula mesChiefdataSortformula = new MesChiefdataSortformula();
		BeanUtils.copyProperties(mesChiefdataSortformulaPage, mesChiefdataSortformula);
		mesChiefdataSortformulaService.saveMain(mesChiefdataSortformula, mesChiefdataSortformulaPage.getMesSortformulaStrategyinfoList());
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param mesChiefdataSortformulaPage
	 * @return
	 */
	@AutoLog(value = "主数据—拣货方案-编辑")
	@ApiOperation(value="主数据—拣货方案-编辑", notes="主数据—拣货方案-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody MesChiefdataSortformulaPage mesChiefdataSortformulaPage) {
		MesChiefdataSortformula mesChiefdataSortformula = new MesChiefdataSortformula();
		BeanUtils.copyProperties(mesChiefdataSortformulaPage, mesChiefdataSortformula);
		MesChiefdataSortformula mesChiefdataSortformulaEntity = mesChiefdataSortformulaService.getById(mesChiefdataSortformula.getId());
		if(mesChiefdataSortformulaEntity==null) {
			return Result.error("未找到对应数据");
		}
		mesChiefdataSortformulaService.updateMain(mesChiefdataSortformula, mesChiefdataSortformulaPage.getMesSortformulaStrategyinfoList());
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "主数据—拣货方案-通过id删除")
	@ApiOperation(value="主数据—拣货方案-通过id删除", notes="主数据—拣货方案-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		mesChiefdataSortformulaService.delMain(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "主数据—拣货方案-批量删除")
	@ApiOperation(value="主数据—拣货方案-批量删除", notes="主数据—拣货方案-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.mesChiefdataSortformulaService.delBatchMain(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功！");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "主数据—拣货方案-通过id查询")
	@ApiOperation(value="主数据—拣货方案-通过id查询", notes="主数据—拣货方案-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		MesChiefdataSortformula mesChiefdataSortformula = mesChiefdataSortformulaService.getById(id);
		if(mesChiefdataSortformula==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(mesChiefdataSortformula);

	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "主数据—拣货方案策略通过主表ID查询")
	@ApiOperation(value="主数据—拣货方案策略主表ID查询", notes="主数据—拣货方案策略-通主表ID查询")
	@GetMapping(value = "/queryMesSortformulaStrategyinfoByMainId")
	public Result<?> queryMesSortformulaStrategyinfoListByMainId(@RequestParam(name="id",required=true) String id) {
		List<MesSortformulaStrategyinfo> mesSortformulaStrategyinfoList = mesSortformulaStrategyinfoService.selectByMainId(id);
		return Result.ok(mesSortformulaStrategyinfoList);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param mesChiefdataSortformula
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesChiefdataSortformula mesChiefdataSortformula) {
      // Step.1 组装查询条件查询数据
      QueryWrapper<MesChiefdataSortformula> queryWrapper = QueryGenerator.initQueryWrapper(mesChiefdataSortformula, request.getParameterMap());
      LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

      //Step.2 获取导出数据
      List<MesChiefdataSortformula> queryList = mesChiefdataSortformulaService.list(queryWrapper);
      // 过滤选中数据
      String selections = request.getParameter("selections");
      List<MesChiefdataSortformula> mesChiefdataSortformulaList = new ArrayList<MesChiefdataSortformula>();
      if(oConvertUtils.isEmpty(selections)) {
          mesChiefdataSortformulaList = queryList;
      }else {
          List<String> selectionList = Arrays.asList(selections.split(","));
          mesChiefdataSortformulaList = queryList.stream().filter(item -> selectionList.contains(item.getId())).collect(Collectors.toList());
      }

      // Step.3 组装pageList
      List<MesChiefdataSortformulaPage> pageList = new ArrayList<MesChiefdataSortformulaPage>();
      for (MesChiefdataSortformula main : mesChiefdataSortformulaList) {
          MesChiefdataSortformulaPage vo = new MesChiefdataSortformulaPage();
          BeanUtils.copyProperties(main, vo);
          List<MesSortformulaStrategyinfo> mesSortformulaStrategyinfoList = mesSortformulaStrategyinfoService.selectByMainId(main.getId());
          vo.setMesSortformulaStrategyinfoList(mesSortformulaStrategyinfoList);
          pageList.add(vo);
      }

      // Step.4 AutoPoi 导出Excel
      ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
      mv.addObject(NormalExcelConstants.FILE_NAME, "主数据—拣货方案列表");
      mv.addObject(NormalExcelConstants.CLASS, MesChiefdataSortformulaPage.class);
      mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("主数据—拣货方案数据", "导出人:"+sysUser.getRealname(), "主数据—拣货方案"));
      mv.addObject(NormalExcelConstants.DATA_LIST, pageList);
      return mv;
    }

    /**
    * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
      MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
      Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
      for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
          MultipartFile file = entity.getValue();// 获取上传文件对象
          ImportParams params = new ImportParams();
          params.setTitleRows(2);
          params.setHeadRows(1);
          params.setNeedSave(true);
          try {
              List<MesChiefdataSortformulaPage> list = ExcelImportUtil.importExcel(file.getInputStream(), MesChiefdataSortformulaPage.class, params);
              for (MesChiefdataSortformulaPage page : list) {
                  MesChiefdataSortformula po = new MesChiefdataSortformula();
                  BeanUtils.copyProperties(page, po);
                  mesChiefdataSortformulaService.saveMain(po, page.getMesSortformulaStrategyinfoList());
              }
              return Result.ok("文件导入成功！数据行数:" + list.size());
          } catch (Exception e) {
              log.error(e.getMessage(),e);
              return Result.error("文件导入失败:"+e.getMessage());
          } finally {
              try {
                  file.getInputStream().close();
              } catch (IOException e) {
                  e.printStackTrace();
              }
          }
      }
      return Result.ok("文件导入失败！");
    }

}
