package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesAncillarytoolHeat;
import org.jeecg.modules.mes.chiefdata.mapper.MesAncillarytoolHeatMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesAncillarytoolHeatService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 制造中心-辅料回温
 * @Author: jeecg-boot
 * @Date:   2020-11-04
 * @Version: V1.0
 */
@Service
public class MesAncillarytoolHeatServiceImpl extends ServiceImpl<MesAncillarytoolHeatMapper, MesAncillarytoolHeat> implements IMesAncillarytoolHeatService {

}
