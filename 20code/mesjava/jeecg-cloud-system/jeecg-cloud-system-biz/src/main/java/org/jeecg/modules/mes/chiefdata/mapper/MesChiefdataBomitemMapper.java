package org.jeecg.modules.mes.chiefdata.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataBomitem;

import java.util.List;

/**
 * @Description: BOM—数据项
 * @Author: jeecg-boot
 * @Date:   2020-10-20
 * @Version: V1.0
 */
public interface MesChiefdataBomitemMapper extends BaseMapper<MesChiefdataBomitem> {

	public boolean deleteByMainId(@Param("mainId") String mainId);

	public List<MesChiefdataBomitem> selectByMainId(@Param("mainId") String mainId);

	@Select("select * from mes_chiefdata_bomitem where bom_id=(SELECT id from mes_chiefdata_bom where machinesort_code=(SELECT materiel_code from mes_chiefdata_materiel where id=#{materialId}))")
	public List<MesChiefdataBomitem> selectByMaterialId(@Param("materialId") String materialId);

	@Select("select * from `jeecg-cloud`.mes_chiefdata_bomitem where bom_id=(select id from `jeecg-cloud`.mes_chiefdata_bom where machinesort_code=#{machinesortCode}) and materiel_code=#{materielCode}")
	public MesChiefdataBomitem getsfg(@Param("machinesortCode") String machinesortCode,@Param("materielCode") String materielCode);
}
