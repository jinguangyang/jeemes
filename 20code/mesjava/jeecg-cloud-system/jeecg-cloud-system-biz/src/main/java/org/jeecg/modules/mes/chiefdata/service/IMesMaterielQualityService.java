package org.jeecg.modules.mes.chiefdata.service;

import org.jeecg.modules.mes.chiefdata.entity.MesMaterielQuality;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 物料—品质
 * @Author: jeecg-boot
 * @Date:   2020-10-16
 * @Version: V1.0
 */
public interface IMesMaterielQualityService extends IService<MesMaterielQuality> {

	public List<MesMaterielQuality> selectByMainId(String mainId);
}
