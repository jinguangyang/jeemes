package org.jeecg.modules.mes.client;

import org.jeecg.common.constant.ServiceNameConstants;
import org.jeecg.modules.mes.produce.entity.MesAncillarytoolHeat;
import org.jeecg.modules.mes.produce.entity.MesCommandbillInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Component
@FeignClient(contextId = "ProduceServiceClient",value = ServiceNameConstants.PRODUCE_SERVICE,qualifier = "produceService")
public interface ProduceClient {

    @GetMapping("produce/mesAncillarytoolHeat/listByFinishTime")
    public List<MesAncillarytoolHeat> listByFinishTime();

    @GetMapping("produce/mesCommandbillInfo/getInfoByLine")
    public List<MesCommandbillInfo> getInfoByLine();

    @GetMapping("produce/mesCommandbillInfo/getInfoTotalByLine")
    public MesCommandbillInfo getInfoTotalByLine();
}
