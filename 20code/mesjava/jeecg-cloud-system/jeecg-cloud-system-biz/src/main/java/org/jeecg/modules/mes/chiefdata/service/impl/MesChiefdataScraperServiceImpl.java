package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataScraper;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataScraperMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataScraperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 刮刀建档
 * @Author: jeecg-boot
 * @Date:   2021-04-19
 * @Version: V1.0
 */
@Service
public class MesChiefdataScraperServiceImpl extends ServiceImpl<MesChiefdataScraperMapper, MesChiefdataScraper> implements IMesChiefdataScraperService {

    @Autowired
    private MesChiefdataScraperMapper scraperMapper;
    @Override
    public boolean increasePointUsageNum(String scraperSn) {
        return scraperMapper.increasePointUsageNum(scraperSn);
    }
}
