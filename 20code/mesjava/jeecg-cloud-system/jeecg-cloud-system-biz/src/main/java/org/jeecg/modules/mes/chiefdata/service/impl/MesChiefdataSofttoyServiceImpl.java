package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataSofttoy;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataSofttoyMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataSofttoyService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 主数据—软体管理
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Service
public class MesChiefdataSofttoyServiceImpl extends ServiceImpl<MesChiefdataSofttoyMapper, MesChiefdataSofttoy> implements IMesChiefdataSofttoyService {

}
