package org.jeecg.modules.demo.client;

import com.alibaba.fastjson.JSONObject;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.ServiceNameConstants;
import org.jeecg.common.system.vo.LoginUser;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Component
@FeignClient(contextId = "MytestServiceClient", value = ServiceNameConstants.TRANSACTION_SERVICE)
public interface MytestServiceClient {

    @GetMapping("ems/industryTest/showname/{name}")
    public String getName(@PathVariable("name") String name);


}
