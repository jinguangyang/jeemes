package org.jeecg.modules.mes.chiefdata.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataDockettype;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataDockettypeService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 主数据—单据类型配置
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Api(tags="主数据—单据类型配置")
@RestController
@RequestMapping("/chiefdata/mesChiefdataDockettype")
@Slf4j
public class MesChiefdataDockettypeController extends JeecgController<MesChiefdataDockettype, IMesChiefdataDockettypeService> {
	@Autowired
	private IMesChiefdataDockettypeService mesChiefdataDockettypeService;
	
	/**
	 * 分页列表查询
	 *
	 * @param mesChiefdataDockettype
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "主数据—单据类型配置-分页列表查询")
	@ApiOperation(value="主数据—单据类型配置-分页列表查询", notes="主数据—单据类型配置-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesChiefdataDockettype mesChiefdataDockettype,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesChiefdataDockettype> queryWrapper = QueryGenerator.initQueryWrapper(mesChiefdataDockettype, req.getParameterMap());
		Page<MesChiefdataDockettype> page = new Page<MesChiefdataDockettype>(pageNo, pageSize);
		IPage<MesChiefdataDockettype> pageList = mesChiefdataDockettypeService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param mesChiefdataDockettype
	 * @return
	 */
	@AutoLog(value = "主数据—单据类型配置-添加")
	@ApiOperation(value="主数据—单据类型配置-添加", notes="主数据—单据类型配置-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MesChiefdataDockettype mesChiefdataDockettype) {
		mesChiefdataDockettypeService.save(mesChiefdataDockettype);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param mesChiefdataDockettype
	 * @return
	 */
	@AutoLog(value = "主数据—单据类型配置-编辑")
	@ApiOperation(value="主数据—单据类型配置-编辑", notes="主数据—单据类型配置-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody MesChiefdataDockettype mesChiefdataDockettype) {
		mesChiefdataDockettypeService.updateById(mesChiefdataDockettype);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "主数据—单据类型配置-通过id删除")
	@ApiOperation(value="主数据—单据类型配置-通过id删除", notes="主数据—单据类型配置-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		mesChiefdataDockettypeService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "主数据—单据类型配置-批量删除")
	@ApiOperation(value="主数据—单据类型配置-批量删除", notes="主数据—单据类型配置-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.mesChiefdataDockettypeService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "主数据—单据类型配置-通过id查询")
	@ApiOperation(value="主数据—单据类型配置-通过id查询", notes="主数据—单据类型配置-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		MesChiefdataDockettype mesChiefdataDockettype = mesChiefdataDockettypeService.getById(id);
		if(mesChiefdataDockettype==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(mesChiefdataDockettype);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param mesChiefdataDockettype
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesChiefdataDockettype mesChiefdataDockettype) {
        return super.exportXls(request, mesChiefdataDockettype, MesChiefdataDockettype.class, "主数据—单据类型配置");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesChiefdataDockettype.class);
    }

}
