package org.jeecg.modules.mes.chiefdata.service;

import org.jeecg.modules.mes.chiefdata.entity.MesElectronicbusBaseinfo;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 主数据—电子料车基本信息
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
public interface IMesElectronicbusBaseinfoService extends IService<MesElectronicbusBaseinfo> {

	public List<MesElectronicbusBaseinfo> selectByMainId(String mainId);
}
