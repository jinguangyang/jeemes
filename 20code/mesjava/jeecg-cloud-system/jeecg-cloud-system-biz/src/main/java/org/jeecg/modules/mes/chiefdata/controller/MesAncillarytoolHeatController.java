package org.jeecg.modules.mes.chiefdata.controller;

import java.util.*;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.api.ISysBaseAPI;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.mes.chiefdata.entity.MesAncillarytoolHeat;
import org.jeecg.modules.mes.chiefdata.service.IMesAncillarytoolHeatService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 制造中心-辅料回温
 * @Author: jeecg-boot
 * @Date:   2020-11-04
 * @Version: V1.0
 */
@Api(tags="制造中心-辅料回温")
@RestController
@RequestMapping("/chiefdata/mesAncillarytoolHeat")
@Slf4j
public class MesAncillarytoolHeatController extends JeecgController<MesAncillarytoolHeat, IMesAncillarytoolHeatService> {
	@Autowired
	private IMesAncillarytoolHeatService mesAncillarytoolHeatService;
	@Autowired
	RedisUtil redisUtil;

	/**
	 * 分页列表查询
	 *
	 * @param mesAncillarytoolHeat
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "制造中心-辅料回温-分页列表查询")
	@ApiOperation(value="制造中心-辅料回温-分页列表查询", notes="制造中心-辅料回温-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesAncillarytoolHeat mesAncillarytoolHeat,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesAncillarytoolHeat> queryWrapper = QueryGenerator.initQueryWrapper(mesAncillarytoolHeat, req.getParameterMap());
		Page<MesAncillarytoolHeat> page = new Page<MesAncillarytoolHeat>(pageNo, pageSize);
		IPage<MesAncillarytoolHeat> pageList = mesAncillarytoolHeatService.page(page, queryWrapper);
		return Result.ok(pageList);
	}

	/**
	 *   添加
	 *
	 * @param mesAncillarytoolHeat
	 * @return
	 */
	@AutoLog(value = "制造中心-辅料回温-添加")
	@ApiOperation(value="制造中心-辅料回温-添加", notes="制造中心-辅料回温-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MesAncillarytoolHeat mesAncillarytoolHeat) {
		Date heatBegintime = mesAncillarytoolHeat.getHeatBegintime();
		if(heatBegintime == null){
			return Result.error("请输入回温开始时间！");
		}
		String heatTime = mesAncillarytoolHeat.getHeatTime();
		if(StringUtils.isBlank(heatTime)){
			return Result.error("请输入回温时间！");
		}
		int amount = Integer.valueOf(heatTime);
		Date finishTime = addTime(heatBegintime,amount);
		mesAncillarytoolHeat.setExpectFinishtime(finishTime);
		mesAncillarytoolHeatService.save(mesAncillarytoolHeat);

		redisUtil.set(mesAncillarytoolHeat.getId(),JSON.toJSONString(mesAncillarytoolHeat),amount*60);
		return Result.ok("添加成功！");
	}

	 public static Date addTime(Date date, int amount){
		 Calendar calendar = new GregorianCalendar();
		 calendar.setTime(date);
		 calendar.add(calendar.MINUTE,amount);
		 date=calendar.getTime();
		 return date;
	 }

	/**
	 *  编辑
	 *
	 * @param mesAncillarytoolHeat
	 * @return
	 */
	@AutoLog(value = "制造中心-辅料回温-编辑")
	@ApiOperation(value="制造中心-辅料回温-编辑", notes="制造中心-辅料回温-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody MesAncillarytoolHeat mesAncillarytoolHeat) {
		mesAncillarytoolHeatService.updateById(mesAncillarytoolHeat);
		return Result.ok("编辑成功!");
	}

	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "制造中心-辅料回温-通过id删除")
	@ApiOperation(value="制造中心-辅料回温-通过id删除", notes="制造中心-辅料回温-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		mesAncillarytoolHeatService.removeById(id);
		return Result.ok("删除成功!");
	}

	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "制造中心-辅料回温-批量删除")
	@ApiOperation(value="制造中心-辅料回温-批量删除", notes="制造中心-辅料回温-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.mesAncillarytoolHeatService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}

	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "制造中心-辅料回温-通过id查询")
	@ApiOperation(value="制造中心-辅料回温-通过id查询", notes="制造中心-辅料回温-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		MesAncillarytoolHeat mesAncillarytoolHeat = mesAncillarytoolHeatService.getById(id);
		if(mesAncillarytoolHeat==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(mesAncillarytoolHeat);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param mesAncillarytoolHeat
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesAncillarytoolHeat mesAncillarytoolHeat) {
        return super.exportXls(request, mesAncillarytoolHeat, MesAncillarytoolHeat.class, "制造中心-辅料回温");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesAncillarytoolHeat.class);
    }

}
