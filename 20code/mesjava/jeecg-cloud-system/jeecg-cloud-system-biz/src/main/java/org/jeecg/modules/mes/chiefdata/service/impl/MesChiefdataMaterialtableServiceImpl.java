package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataMaterialtable;
import org.jeecg.modules.mes.chiefdata.entity.MesMaterialtableItem;
import org.jeecg.modules.mes.chiefdata.mapper.MesMaterialtableItemMapper;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataMaterialtableMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataMaterialtableService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Collection;

/**
 * @Description: 主数据—料表管理
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Service
public class MesChiefdataMaterialtableServiceImpl extends ServiceImpl<MesChiefdataMaterialtableMapper, MesChiefdataMaterialtable> implements IMesChiefdataMaterialtableService {

	@Autowired
	private MesChiefdataMaterialtableMapper mesChiefdataMaterialtableMapper;
	@Autowired
	private MesMaterialtableItemMapper mesMaterialtableItemMapper;
	
	@Override
	@Transactional
	public void saveMain(MesChiefdataMaterialtable mesChiefdataMaterialtable, List<MesMaterialtableItem> mesMaterialtableItemList) {
		mesChiefdataMaterialtableMapper.insert(mesChiefdataMaterialtable);
		if(mesMaterialtableItemList!=null && mesMaterialtableItemList.size()>0) {
			for(MesMaterialtableItem entity:mesMaterialtableItemList) {
				//外键设置
				entity.setMtableId(mesChiefdataMaterialtable.getId());
				mesMaterialtableItemMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void updateMain(MesChiefdataMaterialtable mesChiefdataMaterialtable,List<MesMaterialtableItem> mesMaterialtableItemList) {
		mesChiefdataMaterialtableMapper.updateById(mesChiefdataMaterialtable);
		
		//1.先删除子表数据
		mesMaterialtableItemMapper.deleteByMainId(mesChiefdataMaterialtable.getId());
		
		//2.子表数据重新插入
		if(mesMaterialtableItemList!=null && mesMaterialtableItemList.size()>0) {
			for(MesMaterialtableItem entity:mesMaterialtableItemList) {
				//外键设置
				entity.setMtableId(mesChiefdataMaterialtable.getId());
				mesMaterialtableItemMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void delMain(String id) {
		mesMaterialtableItemMapper.deleteByMainId(id);
		mesChiefdataMaterialtableMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			mesMaterialtableItemMapper.deleteByMainId(id.toString());
			mesChiefdataMaterialtableMapper.deleteById(id);
		}
	}
	
}
