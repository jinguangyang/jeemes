package org.jeecg.modules.mes.chiefdata.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataBadcode;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 主数据—不良代码
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
public interface MesChiefdataBadcodeMapper extends BaseMapper<MesChiefdataBadcode> {

}
