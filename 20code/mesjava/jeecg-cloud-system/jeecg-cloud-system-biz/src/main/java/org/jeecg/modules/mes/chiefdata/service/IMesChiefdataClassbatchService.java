package org.jeecg.modules.mes.chiefdata.service;

import org.jeecg.modules.mes.chiefdata.entity.MesClassbatchTimetable;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataClassbatch;
import com.baomidou.mybatisplus.extension.service.IService;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 主数据—班组
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
public interface IMesChiefdataClassbatchService extends IService<MesChiefdataClassbatch> {

	/**
	 * 添加一对多
	 * 
	 */
	public void saveMain(MesChiefdataClassbatch mesChiefdataClassbatch,List<MesClassbatchTimetable> mesClassbatchTimetableList) ;
	
	/**
	 * 修改一对多
	 * 
	 */
	public void updateMain(MesChiefdataClassbatch mesChiefdataClassbatch,List<MesClassbatchTimetable> mesClassbatchTimetableList);
	
	/**
	 * 删除一对多
	 */
	public void delMain (String id);
	
	/**
	 * 批量删除一对多
	 */
	public void delBatchMain (Collection<? extends Serializable> idList);
	
}
