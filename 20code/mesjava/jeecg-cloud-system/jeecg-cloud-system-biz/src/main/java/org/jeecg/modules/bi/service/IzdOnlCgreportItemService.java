package org.jeecg.modules.bi.service;

import org.jeecg.modules.bi.entity.zdOnlCgreportItem;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: onl_cgreport_item
 * @Author: wms-cloud
 * @Date:   2020-12-09
 * @Version: V1.0
 */
public interface IzdOnlCgreportItemService extends IService<zdOnlCgreportItem> {

	public List<zdOnlCgreportItem> selectByMainId(String mainId);
}
