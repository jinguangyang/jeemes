package org.jeecg.modules.mes.chiefdata.service;

import org.jeecg.modules.mes.chiefdata.entity.MesPatrolcheckTimespan;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 主数据—巡检方案时段
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
public interface IMesPatrolcheckTimespanService extends IService<MesPatrolcheckTimespan> {

	public List<MesPatrolcheckTimespan> selectByMainId(String mainId);
}
