package org.jeecg.modules.mes.chiefdata.service;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataLogisticalcompany;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 主数据—物流公司
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
public interface IMesChiefdataLogisticalcompanyService extends IService<MesChiefdataLogisticalcompany> {

}
