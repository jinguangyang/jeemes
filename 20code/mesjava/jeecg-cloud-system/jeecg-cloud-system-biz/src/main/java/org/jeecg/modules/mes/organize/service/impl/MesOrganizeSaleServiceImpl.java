package org.jeecg.modules.mes.organize.service.impl;

import org.jeecg.modules.mes.organize.entity.MesOrganizeSale;
import org.jeecg.modules.mes.organize.mapper.MesOrganizeSaleMapper;
import org.jeecg.modules.mes.organize.service.IMesOrganizeSaleService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 组织—销售
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
@Service
public class MesOrganizeSaleServiceImpl extends ServiceImpl<MesOrganizeSaleMapper, MesOrganizeSale> implements IMesOrganizeSaleService {

}
