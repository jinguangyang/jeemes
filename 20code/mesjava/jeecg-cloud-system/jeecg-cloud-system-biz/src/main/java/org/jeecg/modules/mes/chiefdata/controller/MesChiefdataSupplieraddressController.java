package org.jeecg.modules.mes.chiefdata.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataSupplieraddress;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataSupplieraddressService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 主数据—供应商地址
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Api(tags="主数据—供应商地址")
@RestController
@RequestMapping("/chiefdata/mesChiefdataSupplieraddress")
@Slf4j
public class MesChiefdataSupplieraddressController extends JeecgController<MesChiefdataSupplieraddress, IMesChiefdataSupplieraddressService> {
	@Autowired
	private IMesChiefdataSupplieraddressService mesChiefdataSupplieraddressService;
	
	/**
	 * 分页列表查询
	 *
	 * @param mesChiefdataSupplieraddress
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "主数据—供应商地址-分页列表查询")
	@ApiOperation(value="主数据—供应商地址-分页列表查询", notes="主数据—供应商地址-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesChiefdataSupplieraddress mesChiefdataSupplieraddress,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesChiefdataSupplieraddress> queryWrapper = QueryGenerator.initQueryWrapper(mesChiefdataSupplieraddress, req.getParameterMap());
		Page<MesChiefdataSupplieraddress> page = new Page<MesChiefdataSupplieraddress>(pageNo, pageSize);
		IPage<MesChiefdataSupplieraddress> pageList = mesChiefdataSupplieraddressService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param mesChiefdataSupplieraddress
	 * @return
	 */
	@AutoLog(value = "主数据—供应商地址-添加")
	@ApiOperation(value="主数据—供应商地址-添加", notes="主数据—供应商地址-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MesChiefdataSupplieraddress mesChiefdataSupplieraddress) {
		mesChiefdataSupplieraddressService.save(mesChiefdataSupplieraddress);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param mesChiefdataSupplieraddress
	 * @return
	 */
	@AutoLog(value = "主数据—供应商地址-编辑")
	@ApiOperation(value="主数据—供应商地址-编辑", notes="主数据—供应商地址-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody MesChiefdataSupplieraddress mesChiefdataSupplieraddress) {
		mesChiefdataSupplieraddressService.updateById(mesChiefdataSupplieraddress);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "主数据—供应商地址-通过id删除")
	@ApiOperation(value="主数据—供应商地址-通过id删除", notes="主数据—供应商地址-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		mesChiefdataSupplieraddressService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "主数据—供应商地址-批量删除")
	@ApiOperation(value="主数据—供应商地址-批量删除", notes="主数据—供应商地址-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.mesChiefdataSupplieraddressService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "主数据—供应商地址-通过id查询")
	@ApiOperation(value="主数据—供应商地址-通过id查询", notes="主数据—供应商地址-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		MesChiefdataSupplieraddress mesChiefdataSupplieraddress = mesChiefdataSupplieraddressService.getById(id);
		if(mesChiefdataSupplieraddress==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(mesChiefdataSupplieraddress);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param mesChiefdataSupplieraddress
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesChiefdataSupplieraddress mesChiefdataSupplieraddress) {
        return super.exportXls(request, mesChiefdataSupplieraddress, MesChiefdataSupplieraddress.class, "主数据—供应商地址");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesChiefdataSupplieraddress.class);
    }

}
