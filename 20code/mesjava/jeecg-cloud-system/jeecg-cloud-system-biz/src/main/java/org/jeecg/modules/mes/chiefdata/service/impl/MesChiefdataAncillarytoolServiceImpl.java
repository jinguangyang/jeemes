package org.jeecg.modules.mes.chiefdata.service.impl;

import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataAncillarytool;
import org.jeecg.modules.mes.chiefdata.mapper.MesChiefdataAncillarytoolMapper;
import org.jeecg.modules.mes.chiefdata.service.IMesChiefdataAncillarytoolService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 主数据—辅料信息
 * @Author: jeecg-boot
 * @Date:   2020-09-17
 * @Version: V1.0
 */
@Service
public class MesChiefdataAncillarytoolServiceImpl extends ServiceImpl<MesChiefdataAncillarytoolMapper, MesChiefdataAncillarytool> implements IMesChiefdataAncillarytoolService {

}
