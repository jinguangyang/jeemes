package org.jeecg.modules.mes.chiefdata.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.mes.chiefdata.entity.MesTransactionLock;
import org.jeecg.modules.mes.chiefdata.service.IMesTransactionLockService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 业务锁定
 * @Author: jeecg-boot
 * @Date:   2020-10-24
 * @Version: V1.0
 */
@Api(tags="业务锁定")
@RestController
@RequestMapping("/chiefdata/mesTransactionLock")
@Slf4j
public class MesTransactionLockController extends JeecgController<MesTransactionLock, IMesTransactionLockService> {
	@Autowired
	private IMesTransactionLockService mesTransactionLockService;

	/**
	 * 分页列表查询
	 *
	 * @param mesTransactionLock
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "业务锁定-分页列表查询")
	@ApiOperation(value="业务锁定-分页列表查询", notes="业务锁定-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesTransactionLock mesTransactionLock,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesTransactionLock> queryWrapper = QueryGenerator.initQueryWrapper(mesTransactionLock, req.getParameterMap());
		Page<MesTransactionLock> page = new Page<MesTransactionLock>(pageNo, pageSize);
		IPage<MesTransactionLock> pageList = mesTransactionLockService.page(page, queryWrapper);
		return Result.ok(pageList);
	}

	/**
	 *   添加
	 *
	 * @param mesTransactionLock
	 * @return
	 */
	@AutoLog(value = "业务锁定-添加")
	@ApiOperation(value="业务锁定-添加", notes="业务锁定-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MesTransactionLock mesTransactionLock) {
		mesTransactionLockService.save(mesTransactionLock);
		return Result.ok("添加成功！");
	}

	/**
	 *  编辑
	 *
	 * @param mesTransactionLock
	 * @return
	 */
	@AutoLog(value = "业务锁定-编辑")
	@ApiOperation(value="业务锁定-编辑", notes="业务锁定-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody MesTransactionLock mesTransactionLock) {
		mesTransactionLockService.updateById(mesTransactionLock);
		return Result.ok("编辑成功!");
	}

	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "业务锁定-通过id删除")
	@ApiOperation(value="业务锁定-通过id删除", notes="业务锁定-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		mesTransactionLockService.removeById(id);
		return Result.ok("删除成功!");
	}

	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "业务锁定-批量删除")
	@ApiOperation(value="业务锁定-批量删除", notes="业务锁定-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.mesTransactionLockService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}

	 /**
	  * 通过单号或用户查询
	  *
	  * @param
	  * @return
	  */
	 @AutoLog(value = "业务锁定-通过单号或用户查询")
	 @ApiOperation(value="业务锁定-通过单号或用户查询", notes="业务锁定-通过单号或用户查询")
	 @GetMapping(value = "/queryByCodeOrUser")
	 public Result<?> queryByCodeOrUser(@RequestParam(name="code",required=false) String code,
										@RequestParam(name="userName",required=false) String userName) {
		 QueryWrapper<MesTransactionLock> wrapper = new QueryWrapper<>();
		 wrapper.eq("order_code", code).or().eq("user_name", userName);
		 MesTransactionLock transactionLock = mesTransactionLockService.getOne(wrapper);
		 return Result.ok(transactionLock.getInform());
	 }

	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "业务锁定-通过id查询")
	@ApiOperation(value="业务锁定-通过id查询", notes="业务锁定-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		MesTransactionLock mesTransactionLock = mesTransactionLockService.getById(id);
		if(mesTransactionLock==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(mesTransactionLock);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param mesTransactionLock
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesTransactionLock mesTransactionLock) {
        return super.exportXls(request, mesTransactionLock, MesTransactionLock.class, "业务锁定");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesTransactionLock.class);
    }

}
