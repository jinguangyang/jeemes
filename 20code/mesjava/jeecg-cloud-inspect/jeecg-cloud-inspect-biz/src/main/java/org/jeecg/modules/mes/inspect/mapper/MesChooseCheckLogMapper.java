package org.jeecg.modules.mes.inspect.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.inspect.entity.MesChooseCheckLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 抽检抽测表
 * @Author: jeecg-boot
 * @Date:   2021-03-31
 * @Version: V1.0
 */
public interface MesChooseCheckLogMapper extends BaseMapper<MesChooseCheckLog> {

}
