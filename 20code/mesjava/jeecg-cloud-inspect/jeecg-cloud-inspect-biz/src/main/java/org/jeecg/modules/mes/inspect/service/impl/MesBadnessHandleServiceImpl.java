package org.jeecg.modules.mes.inspect.service.impl;

import org.jeecg.modules.mes.inspect.entity.MesBadnessHandle;
import org.jeecg.modules.mes.inspect.entity.MesBadnessInfo;
import org.jeecg.modules.mes.inspect.mapper.MesBadnessInfoMapper;
import org.jeecg.modules.mes.inspect.mapper.MesBadnessHandleMapper;
import org.jeecg.modules.mes.inspect.service.IMesBadnessHandleService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Collection;

/**
 * @Description: 质检中心-不良处理
 * @Author: jeecg-boot
 * @Date:   2020-10-29
 * @Version: V1.0
 */
@Service
public class MesBadnessHandleServiceImpl extends ServiceImpl<MesBadnessHandleMapper, MesBadnessHandle> implements IMesBadnessHandleService {

	@Autowired
	private MesBadnessHandleMapper mesBadnessHandleMapper;
	@Autowired
	private MesBadnessInfoMapper mesBadnessInfoMapper;

	@Override
	@Transactional
	public void saveMain(MesBadnessHandle mesBadnessHandle, List<MesBadnessInfo> mesBadnessInfoList) {
		mesBadnessHandleMapper.insert(mesBadnessHandle);
		if(mesBadnessInfoList!=null && mesBadnessInfoList.size()>0) {
			for(MesBadnessInfo entity:mesBadnessInfoList) {
				//外键设置
				entity.setBadnessId(mesBadnessHandle.getId());
				mesBadnessInfoMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void delMain(String id) {
		mesBadnessInfoMapper.deleteByMainId(id);
		mesBadnessHandleMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			mesBadnessInfoMapper.deleteByMainId(id.toString());
			mesBadnessHandleMapper.deleteById(id);
		}
	}

}
