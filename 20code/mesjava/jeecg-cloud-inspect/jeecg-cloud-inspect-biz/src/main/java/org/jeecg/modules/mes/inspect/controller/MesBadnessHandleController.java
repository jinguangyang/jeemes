package org.jeecg.modules.mes.inspect.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.DateUtils;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.mes.client.CertificateClient;
import org.jeecg.modules.mes.client.OrderClient;
import org.jeecg.modules.mes.client.ProduceClient;
import org.jeecg.modules.mes.inspect.entity.MesBadnessHandle;
import org.jeecg.modules.mes.inspect.entity.MesBadnessInfo;
import org.jeecg.modules.mes.inspect.entity.MesInspectionOut;
import org.jeecg.modules.mes.inspect.service.IMesBadnessHandleService;
import org.jeecg.modules.mes.inspect.service.IMesBadnessInfoService;
import org.jeecg.modules.mes.inspect.service.IMesInspectionOutService;
import org.jeecg.modules.mes.inspect.vo.MesBadnessHandlePage;
import org.jeecg.modules.mes.iqc.entity.MesIqcInfo;
import org.jeecg.modules.mes.iqc.service.IMesIqcInfoService;
import org.jeecg.modules.mes.order.entity.MesOrderPurchase;
import org.jeecg.modules.mes.produce.entity.MesCommandbillInfo;
import org.jeecg.modules.mes.storage.entity.MesCertificateItem;
import org.jeecg.modules.mes.storage.entity.MesCertificatePerk;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

 /**
 * @Description: 质检中心-不良处理
 * @Author: jeecg-boot
 * @Date:   2020-10-29
 * @Version: V1.0
 */
@Api(tags="质检中心-不良处理")
@RestController
@RequestMapping("/inspect/mesBadnessHandle")
@Slf4j
public class MesBadnessHandleController extends JeecgController<MesBadnessHandle, IMesBadnessHandleService> {

	 @Autowired
	 private IMesBadnessHandleService mesBadnessHandleService;

	 @Autowired
	 private IMesBadnessInfoService mesBadnessInfoService;

	 @Autowired
	 private IMesIqcInfoService mesIqcInfoService;

	 @Autowired
	 private IMesInspectionOutService mesInspectionOutService;

	 @Autowired
	 CertificateClient certificateClient;

	 @Autowired
	 OrderClient orderClient;

	 @Autowired
	 ProduceClient produceClient;

	/*---------------------------------主表处理-begin-------------------------------------*/

	/**
	 * 分页列表查询
	 * @param mesBadnessHandle
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "质检中心-不良处理-分页列表查询")
	@ApiOperation(value="质检中心-不良处理-分页列表查询", notes="质检中心-不良处理-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesBadnessHandle mesBadnessHandle,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesBadnessHandle> queryWrapper = QueryGenerator.initQueryWrapper(mesBadnessHandle, req.getParameterMap());
		Page<MesBadnessHandle> page = new Page<MesBadnessHandle>(pageNo, pageSize);
		IPage<MesBadnessHandle> pageList = mesBadnessHandleService.page(page, queryWrapper);
		return Result.ok(pageList);
	}

	/**
     *   添加
     * @param mesBadnessHandle
     * @return
     */
    @AutoLog(value = "质检中心-不良处理-添加")
    @ApiOperation(value="质检中心-不良处理-添加", notes="质检中心-不良处理-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody MesBadnessHandle mesBadnessHandle) {
        mesBadnessHandleService.save(mesBadnessHandle);
        return Result.ok("添加成功！");
    }

     /**
	  *   添加
	  *
	  * @param mesBadnessHandlePage
	  * @return
	  */
	 @AutoLog(value = "质检中心-不良处理-APP添加")
	 @ApiOperation(value="质检中心-不良处理-APP添加", notes="质检中心-不良处理-APP添加")
	 @PostMapping(value = "/appAdd")
	 public Result<?> appAdd(@RequestBody MesBadnessHandlePage mesBadnessHandlePage) {
	 	System.err.println(mesBadnessHandlePage);
		 LoginUser loginUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
//	 	if(StringUtils.isBlank(mesBadnessHandlePage.getRelatedCode())){
//	 		//如果没有制令单入库的凭证id，那么重新查找
//			QueryWrapper<MesCertificateItem> w1 = new QueryWrapper();
//			w1.eq("command_id",mesBadnessHandlePage.getRelatedCode());
//			w1.eq("mobile_type","制令单入库");
//			w1.eq("materiel_code",mesBadnessHandlePage.getMaterielCode());
//			MesCertificateItem item = certificateClient.queryCertificateItemByWrapper(w1);
//			mesBadnessHandlePage.setRelatedCode(item.getId());
//		}
	 	//收货和制令单入库，会添加物料凭证；质检模块，检查的就是收货和入库的物料凭证
		 if(StringUtils.isNotBlank(mesBadnessHandlePage.getRelatedCode()) && !"262".equals(mesBadnessHandlePage.getInspectGrade())){
		 	//关联单号：物料凭证项目id
		 	String certificateItemCode = mesBadnessHandlePage.getRelatedCode();
		 	//通过物料凭证项目id，远程调用物料凭证模块，获取收货/制令单入库信息
			 MesCertificateItem mesCertificateItem = certificateClient.queryCertificateItemById(certificateItemCode);
			 java.util.Date enrollDate= DateUtils.getDate();
			 if(mesCertificateItem != null){

				 // 不良品总数
				 int totalBadnum = 0;
				 List<MesBadnessInfo> badnessInfos = mesBadnessHandlePage.getMesBadnessInfoList();
				 for (MesBadnessInfo badnessInfo : badnessInfos) {
					 int badNum = Integer.valueOf(badnessInfo.getQuery2());
					 System.err.println(badNum);
					 totalBadnum += badNum;
				 }
				 //主表的不良数量 =  子表的不良品数量之和
				 mesBadnessHandlePage.setBadNum(String.valueOf(totalBadnum));
				 if(StringUtils.isNotBlank(mesCertificateItem.getInputNum())){
				 	//计算不良率
					 float inputNum = Float.parseFloat(mesCertificateItem.getInputNum());
					 // 创建一个数值格式化对象
					 NumberFormat numberFormat = NumberFormat.getInstance();
					 numberFormat.setMaximumFractionDigits(2); // 设置精确到小数点后2位
					 String result = numberFormat.format((float)totalBadnum/(float)inputNum*100);
					 mesBadnessHandlePage.setBadRate(result+"%");
				 }
				 //检测合格之后，才进入下一步的入库，否则一直在质检这步
				 if("是".equals(mesBadnessHandlePage.getInspectQualify())){
					 //通过凭证抬头id，获取物料凭证抬头信息
					 String perkId = mesCertificateItem.getPerkId();
					 MesCertificatePerk mesCertificatePerk = certificateClient.getCertificateById(perkId);
					 if(mesCertificatePerk != null){
						 //设置入库状态为：未完成，app显示质检后的入库信息
						 mesCertificatePerk.setIfInput("未完成");
						 certificateClient.editCertificate(mesCertificatePerk);
						 enrollDate=mesCertificatePerk.getEnrollDate();
					 }

					 //改变物料凭证子表的质检状态，修改后app模块隐藏该条数据
					 mesCertificateItem.setIfInspect("是");
					 certificateClient.editCertificateItem(mesCertificateItem);
				 }
			 }
			 MesBadnessHandle mesBadnessHandle = new MesBadnessHandle();
			 BeanUtils.copyProperties(mesBadnessHandlePage, mesBadnessHandle);
			 mesBadnessHandle.setInspectNum(mesCertificateItem.getInputNum());//检测数量
			 if(StringUtils.isNotBlank(mesCertificateItem.getMaterielGauge())){
			 	mesBadnessHandle.setMaterielGague(mesCertificateItem.getMaterielGauge());//物料规格
			 }
			 //收货质检：物料凭证项目的保留编号：销售订单id
			 MesOrderPurchase purchase = orderClient.getPurchaseNameById(mesCertificateItem.getReserveCode());
			 if(purchase != null){
			 	//设置不良处理的订单号和订单名称 = 销售单号和销售单名称
			 	mesBadnessHandle.setInspectCode(purchase.getOrderCode());//订单编号
			 	mesBadnessHandle.setMatproSn(purchase.getOrderName());//订单名称
			 }
			 //入库检验：物料凭证项目的保留编号：制令单id
			 if(StringUtils.isNotBlank(mesCertificateItem.getCommandId())){
			 	String commandId = mesCertificateItem.getCommandId();//制令单id
				 //通过制令单id，远程调用生产模块，获取制令单数据
				 MesCommandbillInfo commandbillInfo = produceClient.getById(commandId);
				 if(commandbillInfo != null){
					 //设置不良处理的订单号和订单名称 = 制令单号和制令单生产
					 mesBadnessHandle.setInspectCode(commandbillInfo.getCommandbillCode());//制令单号
					 mesBadnessHandle.setMatproSn("制令单生产");//名称
				 }
			 }
			 mesBadnessHandleService.saveMain(mesBadnessHandle, mesBadnessHandlePage.getMesBadnessInfoList());

			 //判断是收货质检编码还是制令单入库质检编码添加相应的检验报告
			 if("103".equals(mesBadnessHandlePage.getInspectGrade())) {//收货质检编码
				 MesIqcInfo mesIqcInfo = mesBadnessHandlePage.getMesIqcInfo();
				 mesIqcInfo.setInMaterialNum(mesBadnessHandle.getInspectCode());//来料单号
				 //来料日期 默认当前时间，如果有凭证输入时间，就直接获取凭证输入时间
				 mesIqcInfo.setInDate(enrollDate);
				 //来料检验报告添加
				 mesBadnessHandlePage.getMesIqcMaterial().setMaterialId(mesBadnessHandle.getMaterielCode());
				 mesBadnessHandlePage.getMesIqcMaterial().setMaterialName(mesBadnessHandle.getMaterielName());
				 mesBadnessHandlePage.getMesIqcMaterial().setInNum(mesBadnessHandle.getInspectNum());
				 mesBadnessHandlePage.getMesIqcMaterial().setInBatch(purchase.getOrderCode());
				 mesBadnessHandlePage.getMesIqcMaterial().setSupName(purchase.getSupplierName());

				 mesIqcInfoService.saveMain(mesIqcInfo, mesBadnessHandlePage.getMesIqcDefect(), mesBadnessHandlePage.getMesIqcMaterial());
			 }

			 return Result.ok("添加成功！");
		 }else if("262".equals(mesBadnessHandlePage.getInspectGrade())){

			 String certificateItemCode = mesBadnessHandlePage.getRelatedCode();
			 MesCommandbillInfo command = produceClient.getById(certificateItemCode);
			 //通过物料凭证项目id，远程调用物料凭证模块，获取收货/制令单入库信息
//			 MesCertificateItem mesCertificateItem = certificateClient.queryCertificateItemById(certificateItemCode);
			 //送检总数
			 String sjnum = mesBadnessHandlePage.getBadRate();
			 java.util.Date enrollDate= DateUtils.getDate();
			 if(command != null){
				 // 不良品总数
				 int totalBadnum = 0;
				 List<MesBadnessInfo> badnessInfos = mesBadnessHandlePage.getMesBadnessInfoList();
				 for (MesBadnessInfo badnessInfo : badnessInfos) {
					 int badNum = Integer.valueOf(badnessInfo.getQuery2());
					 System.err.println(badNum);
					 totalBadnum += badNum;
				 }
				 //主表的不良数量 =  子表的不良品数量之和
				 mesBadnessHandlePage.setBadNum(String.valueOf(totalBadnum));
				 if(StringUtils.isNotBlank(sjnum)){
					 //计算不良率
					 float inputNum = Float.parseFloat(sjnum);
					 // 创建一个数值格式化对象
					 NumberFormat numberFormat = NumberFormat.getInstance();
					 numberFormat.setMaximumFractionDigits(2); // 设置精确到小数点后2位
					 String result = numberFormat.format((float)totalBadnum/(float)inputNum*100);
					 mesBadnessHandlePage.setBadRate(result+"%");
				 }
//				 //检测合格之后，需要生成制令单入库的物料凭证，让这批产品可以入库
//				 if("是".equals(mesBadnessHandlePage.getInspectQualify())){
//					 //通过凭证抬头id，获取物料凭证抬头信息
//					 String perkId = mesCertificateItem.getPerkId();
//					 MesCertificatePerk mesCertificatePerk = certificateClient.getCertificateById(perkId);
//					 if(mesCertificatePerk != null){
//						 //设置入库状态为：未完成，app显示质检后的入库信息
//						 mesCertificatePerk.setIfInput("未完成");
//						 certificateClient.editCertificate(mesCertificatePerk);
//						 enrollDate=mesCertificatePerk.getEnrollDate();
//					 }
//
//					 //改变物料凭证子表的质检状态，修改后app模块隐藏该条数据
//					 mesCertificateItem.setIfInspect("是");
//					 certificateClient.editCertificateItem(mesCertificateItem);
//				 }
			 }
			 MesBadnessHandle mesBadnessHandle = new MesBadnessHandle();
			 BeanUtils.copyProperties(mesBadnessHandlePage, mesBadnessHandle);
			 mesBadnessHandle.setInspectNum(sjnum);//检测数量
			 if(StringUtils.isNotBlank(command.getGague())){
				 mesBadnessHandle.setMaterielGague(command.getGague());//物料规格
			 }
			 //入库检验：物料凭证项目的保留编号：制令单id
			 if(StringUtils.isNotBlank(command.getId())){
				 String commandId = command.getId();//制令单id
				 //通过制令单id，远程调用生产模块，获取制令单数据
				 MesCommandbillInfo commandbillInfo = produceClient.getById(commandId);
				 if(commandbillInfo != null){
					 //设置不良处理的订单号和订单名称 = 制令单号和制令单生产
					 mesBadnessHandle.setInspectCode(commandbillInfo.getCommandbillCode());//制令单号
					 mesBadnessHandle.setMatproSn("制令单生产");//名称
//					 if("是".equals(mesBadnessHandlePage.getInspectQualify())){
//						 commandbillInfo.setCheckState("已完成");
//						 //保存制令单 质检完成
//						 produceClient.editCommand(commandbillInfo);
//					 }
				 }
			 }
			 mesBadnessHandleService.saveMain(mesBadnessHandle, mesBadnessHandlePage.getMesBadnessInfoList());

			 if("262".equals(mesBadnessHandlePage.getInspectGrade())) {//制令单入库质检编码
				 MesInspectionOut mesInspectionOut = mesBadnessHandlePage.getMesInspectionOut();
				 mesInspectionOut.setOrderNum(mesBadnessHandle.getInspectCode());//订单号
				 mesInspectionOut.setBadNum(mesBadnessHandle.getBadNum());
				 mesInspectionOut.setBadRate(mesBadnessHandle.getBadRate());
				 mesInspectionOut.setProductNum(mesBadnessHandle.getMaterielCode());//产品型号
				 mesInspectionOut.setOrderCount(sjnum);//送检数量

				 mesInspectionOut.setInspectDay(DateUtils.getDate());//检验日期
				 //远程调用制令单信息，查询制令单生产阶别
				 MesCommandbillInfo commandbillInfo= produceClient.queryByCode(mesBadnessHandle.getInspectCode());
				 mesInspectionOut.setProductLevel(commandbillInfo.getProduceGrade());//生产阶别
				 mesInspectionOut.setProductLine(commandbillInfo.getLineType());
				 mesInspectionOut.setInspectMan(loginUser.getRealname());
				 mesInspectionOut.setThisCount(commandbillInfo.getPlantNum());//本批数量
				 mesInspectionOutService.saveMain(mesInspectionOut,mesBadnessHandlePage.getBodLogs());
			 }
			 return Result.ok("添加成功！");
		 } else {
		 	//关联单号为物料凭证项目id
		 	return Result.error("关联单号为空！请检查！");
		 }
	 }

    /**
     *  编辑
     * @param mesBadnessHandle
     * @return
     */
    @AutoLog(value = "质检中心-不良处理-编辑")
    @ApiOperation(value="质检中心-不良处理-编辑", notes="质检中心-不良处理-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody MesBadnessHandle mesBadnessHandle) {
        mesBadnessHandleService.updateById(mesBadnessHandle);
        return Result.ok("编辑成功!");
    }

    /**
     * 通过id删除
     * @param id
     * @return
     */
    @AutoLog(value = "质检中心-不良处理-通过id删除")
    @ApiOperation(value="质检中心-不良处理-通过id删除", notes="质检中心-不良处理-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name="id",required=true) String id) {
        mesBadnessHandleService.delMain(id);
        return Result.ok("删除成功!");
    }

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @AutoLog(value = "质检中心-不良处理-批量删除")
    @ApiOperation(value="质检中心-不良处理-批量删除", notes="质检中心-不良处理-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
        this.mesBadnessHandleService.delBatchMain(Arrays.asList(ids.split(",")));
        return Result.ok("批量删除成功!");
    }

    /**
     * 导出
     * @return
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesBadnessHandle mesBadnessHandle) {
        return super.exportXls(request, mesBadnessHandle, MesBadnessHandle.class, "质检中心-不良处理");
    }

    /**
     * 导入
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesBadnessHandle.class);
    }
	/*---------------------------------主表处理-end-------------------------------------*/


    /*--------------------------------子表处理-检验不良信息-begin----------------------------------------------*/
	/**
	 * 通过主表ID查询
	 * @return
	 */
	@AutoLog(value = "检验不良信息-通过主表ID查询")
	@ApiOperation(value="检验不良信息-通过主表ID查询", notes="检验不良信息-通过主表ID查询")
	@GetMapping(value = "/listMesBadnessInfoByMainId")
    public Result<?> listMesBadnessInfoByMainId(MesBadnessInfo mesBadnessInfo,
                                                    @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                                    @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                                    HttpServletRequest req) {
        QueryWrapper<MesBadnessInfo> queryWrapper = QueryGenerator.initQueryWrapper(mesBadnessInfo, req.getParameterMap());
        Page<MesBadnessInfo> page = new Page<MesBadnessInfo>(pageNo, pageSize);
        IPage<MesBadnessInfo> pageList = mesBadnessInfoService.page(page, queryWrapper);
        return Result.ok(pageList);
    }

	/**
	 * 添加
	 * @param mesBadnessInfo
	 * @return
	 */
	@AutoLog(value = "检验不良信息-添加")
	@ApiOperation(value="检验不良信息-添加", notes="检验不良信息-添加")
	@PostMapping(value = "/addMesBadnessInfo")
	public Result<?> addMesBadnessInfo(@RequestBody MesBadnessInfo mesBadnessInfo) {
		mesBadnessInfoService.save(mesBadnessInfo);
		return Result.ok("添加成功！");
	}

    /**
	 * 编辑
	 * @param mesBadnessInfo
	 * @return
	 */
	@AutoLog(value = "检验不良信息-编辑")
	@ApiOperation(value="检验不良信息-编辑", notes="检验不良信息-编辑")
	@PutMapping(value = "/editMesBadnessInfo")
	public Result<?> editMesBadnessInfo(@RequestBody MesBadnessInfo mesBadnessInfo) {
		mesBadnessInfoService.updateById(mesBadnessInfo);
		return Result.ok("编辑成功!");
	}

	/**
	 * 通过id删除
	 * @param id
	 * @return
	 */
	@AutoLog(value = "检验不良信息-通过id删除")
	@ApiOperation(value="检验不良信息-通过id删除", notes="检验不良信息-通过id删除")
	@DeleteMapping(value = "/deleteMesBadnessInfo")
	public Result<?> deleteMesBadnessInfo(@RequestParam(name="id",required=true) String id) {
		mesBadnessInfoService.removeById(id);
		return Result.ok("删除成功!");
	}

	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "检验不良信息-批量删除")
	@ApiOperation(value="检验不良信息-批量删除", notes="检验不良信息-批量删除")
	@DeleteMapping(value = "/deleteBatchMesBadnessInfo")
	public Result<?> deleteBatchMesBadnessInfo(@RequestParam(name="ids",required=true) String ids) {
	    this.mesBadnessInfoService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}

    /**
     * 导出
     * @return
     */
    @RequestMapping(value = "/exportMesBadnessInfo")
    public ModelAndView exportMesBadnessInfo(HttpServletRequest request, MesBadnessInfo mesBadnessInfo) {
		 // Step.1 组装查询条件
		 QueryWrapper<MesBadnessInfo> queryWrapper = QueryGenerator.initQueryWrapper(mesBadnessInfo, request.getParameterMap());
		 LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

		 // Step.2 获取导出数据
		 List<MesBadnessInfo> pageList = mesBadnessInfoService.list(queryWrapper);
		 List<MesBadnessInfo> exportList = null;

		 // 过滤选中数据
		 String selections = request.getParameter("selections");
		 if (oConvertUtils.isNotEmpty(selections)) {
			 List<String> selectionList = Arrays.asList(selections.split(","));
			 exportList = pageList.stream().filter(item -> selectionList.contains(item.getId())).collect(Collectors.toList());
		 } else {
			 exportList = pageList;
		 }

		 // Step.3 AutoPoi 导出Excel
		 ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
		 mv.addObject(NormalExcelConstants.FILE_NAME, "检验不良信息"); //此处设置的filename无效 ,前端会重更新设置一下
		 mv.addObject(NormalExcelConstants.CLASS, MesBadnessInfo.class);
		 mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("检验不良信息报表", "导出人:" + sysUser.getRealname(), "检验不良信息"));
		 mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
		 return mv;
    }

    /**
     * 导入
     * @return
     */
    @RequestMapping(value = "/importMesBadnessInfo/{mainId}")
    public Result<?> importMesBadnessInfo(HttpServletRequest request, HttpServletResponse response, @PathVariable("mainId") String mainId) {
		 MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		 Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
		 for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
			 MultipartFile file = entity.getValue();// 获取上传文件对象
			 ImportParams params = new ImportParams();
			 params.setTitleRows(2);
			 params.setHeadRows(1);
			 params.setNeedSave(true);
			 try {
				 List<MesBadnessInfo> list = ExcelImportUtil.importExcel(file.getInputStream(), MesBadnessInfo.class, params);
				 for (MesBadnessInfo temp : list) {
                    temp.setBadnessId(mainId);
				 }
				 long start = System.currentTimeMillis();
				 mesBadnessInfoService.saveBatch(list);
				 log.info("消耗时间" + (System.currentTimeMillis() - start) + "毫秒");
				 return Result.ok("文件导入成功！数据行数：" + list.size());
			 } catch (Exception e) {
				 log.error(e.getMessage(), e);
				 return Result.error("文件导入失败:" + e.getMessage());
			 } finally {
				 try {
					 file.getInputStream().close();
				 } catch (IOException e) {
					 e.printStackTrace();
				 }
			 }
		 }
		 return Result.error("文件导入失败！");
    }

    /*--------------------------------子表处理-检验不良信息-end----------------------------------------------*/




}
