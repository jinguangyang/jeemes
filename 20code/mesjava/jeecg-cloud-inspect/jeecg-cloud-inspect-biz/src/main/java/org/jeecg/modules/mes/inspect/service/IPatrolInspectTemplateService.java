package org.jeecg.modules.mes.inspect.service;

import org.jeecg.modules.mes.inspect.entity.PatrolInspectTemplate;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 巡检模板表
 * @Author: jeecg-boot
 * @Date:   2021-03-20
 * @Version: V1.0
 */
public interface IPatrolInspectTemplateService extends IService<PatrolInspectTemplate> {

}
