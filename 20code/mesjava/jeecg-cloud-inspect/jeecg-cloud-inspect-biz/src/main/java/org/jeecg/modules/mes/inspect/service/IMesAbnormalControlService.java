package org.jeecg.modules.mes.inspect.service;

import org.jeecg.modules.mes.inspect.entity.MesAbnormalControl;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 品质报表—HOLD产品异常控制单
 * @Author: jeecg-boot
 * @Date:   2021-01-20
 * @Version: V1.0
 */
public interface IMesAbnormalControlService extends IService<MesAbnormalControl> {

}
