package org.jeecg.modules.mes.inspect.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.inspect.entity.ZsFirstPatrolRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 宗申首、巡、末件检验记录表
 * @Author: jeecg-boot
 * @Date:   2021-05-15
 * @Version: V1.0
 */
public interface ZsFirstPatrolRecordMapper extends BaseMapper<ZsFirstPatrolRecord> {

}
