package org.jeecg.modules.mes.inspect.service;

import org.jeecg.modules.mes.inspect.entity.MaterielRetreatReason;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 退料原因表
 * @Author: jeecg-boot
 * @Date:   2021-04-25
 * @Version: V1.0
 */
public interface IMaterielRetreatReasonService extends IService<MaterielRetreatReason> {

	public List<MaterielRetreatReason> selectByMainId(String mainId);
}
