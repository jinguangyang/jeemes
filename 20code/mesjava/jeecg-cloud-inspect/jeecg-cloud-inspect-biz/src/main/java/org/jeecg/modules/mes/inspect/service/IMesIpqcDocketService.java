package org.jeecg.modules.mes.inspect.service;

import org.jeecg.modules.mes.inspect.entity.MesIpqcdocketItem;
import org.jeecg.modules.mes.inspect.entity.MesIpqcDocket;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.beans.factory.annotation.Autowired;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 质检中心-IPQC单据
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
public interface IMesIpqcDocketService extends IService<MesIpqcDocket> {

	/**
	 * 删除一对多
	 */
	public void delMain (String id);

	/**
	 * 批量删除一对多
	 */
	public void delBatchMain (Collection<? extends Serializable> idList);
	/**
	 * 添加一对多
	 *
	 */
	public void saveMain(MesIpqcDocket mesIpqcDocket,List<MesIpqcdocketItem> mesIpqcdocketItemList) ;

}
