package org.jeecg.modules.mes.inspect.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.mes.inspect.entity.PatrolInspectTemplate;
import org.jeecg.modules.mes.inspect.service.IPatrolInspectTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;

 /**
 * @Description: 巡检模板表
 * @Author: jeecg-boot
 * @Date:   2021-03-20
 * @Version: V1.0
 */
@Api(tags="巡检模板表")
@RestController
@RequestMapping("/inspect/patrolInspectTemplate")
@Slf4j
public class PatrolInspectTemplateController extends JeecgController<PatrolInspectTemplate, IPatrolInspectTemplateService> {
	@Autowired
	private IPatrolInspectTemplateService patrolInspectTemplateService;

	/**
	 * 分页列表查询
	 *
	 * @param patrolInspectTemplate
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "巡检模板表-分页列表查询")
	@ApiOperation(value="巡检模板表-分页列表查询", notes="巡检模板表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(PatrolInspectTemplate patrolInspectTemplate,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<PatrolInspectTemplate> queryWrapper = QueryGenerator.initQueryWrapper(patrolInspectTemplate, req.getParameterMap());
		Page<PatrolInspectTemplate> page = new Page<PatrolInspectTemplate>(pageNo, pageSize);
		IPage<PatrolInspectTemplate> pageList = patrolInspectTemplateService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param patrolInspectTemplate
	 * @return
	 */
	@AutoLog(value = "巡检模板表-添加")
	@ApiOperation(value="巡检模板表-添加", notes="巡检模板表-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody PatrolInspectTemplate patrolInspectTemplate) {
		patrolInspectTemplateService.save(patrolInspectTemplate);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param patrolInspectTemplate
	 * @return
	 */
	@AutoLog(value = "巡检模板表-编辑")
	@ApiOperation(value="巡检模板表-编辑", notes="巡检模板表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody PatrolInspectTemplate patrolInspectTemplate) {
		patrolInspectTemplateService.updateById(patrolInspectTemplate);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "巡检模板表-通过id删除")
	@ApiOperation(value="巡检模板表-通过id删除", notes="巡检模板表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		patrolInspectTemplateService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "巡检模板表-批量删除")
	@ApiOperation(value="巡检模板表-批量删除", notes="巡检模板表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.patrolInspectTemplateService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "巡检模板表-通过id查询")
	@ApiOperation(value="巡检模板表-通过id查询", notes="巡检模板表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		PatrolInspectTemplate patrolInspectTemplate = patrolInspectTemplateService.getById(id);
		if(patrolInspectTemplate==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(patrolInspectTemplate);
	}

	 /**
	  * 通过id查询
	  *
	  * @param type
	  * @return
	  */
	 @AutoLog(value = "巡检模板表-通过类型查询")
	 @ApiOperation(value="巡检模板表-通过类型查询", notes="巡检模板表-通过类型查询,类型分别有 Assembly、SMT、DIP")
	 @GetMapping(value = "/queryByType")
	 public Result<?> queryByType(@RequestParam(name="type",required=true) String type) {
		 QueryWrapper<PatrolInspectTemplate> QueryWrapper = new QueryWrapper<>();
		 QueryWrapper.eq("patrol_inspect_type",type);
		 List<PatrolInspectTemplate> list = patrolInspectTemplateService.list(QueryWrapper);
		 return Result.ok(list);
	 }

    /**
    * 导出excel
    *
    * @param request
    * @param patrolInspectTemplate
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, PatrolInspectTemplate patrolInspectTemplate) {
        return super.exportXls(request, patrolInspectTemplate, PatrolInspectTemplate.class, "巡检模板表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, PatrolInspectTemplate.class);
    }

}
