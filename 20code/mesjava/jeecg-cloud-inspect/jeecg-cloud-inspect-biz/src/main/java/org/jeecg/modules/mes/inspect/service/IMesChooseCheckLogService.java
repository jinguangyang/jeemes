package org.jeecg.modules.mes.inspect.service;

import org.jeecg.modules.mes.inspect.entity.MesChooseCheckLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 抽检抽测表
 * @Author: jeecg-boot
 * @Date:   2021-03-31
 * @Version: V1.0
 */
public interface IMesChooseCheckLogService extends IService<MesChooseCheckLog> {

}
