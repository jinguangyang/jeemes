package org.jeecg.modules.mes.inspect.service.impl;

import org.jeecg.modules.mes.inspect.entity.MesDipsmtFirstpiece;
import org.jeecg.modules.mes.inspect.mapper.MesDipsmtFirstpieceMapper;
import org.jeecg.modules.mes.inspect.service.IMesDipsmtFirstpieceService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 品质报表—DIP_SMT首件
 * @Author: jeecg-boot
 * @Date:   2021-01-20
 * @Version: V1.0
 */
@Service
public class MesDipsmtFirstpieceServiceImpl extends ServiceImpl<MesDipsmtFirstpieceMapper, MesDipsmtFirstpiece> implements IMesDipsmtFirstpieceService {

}
