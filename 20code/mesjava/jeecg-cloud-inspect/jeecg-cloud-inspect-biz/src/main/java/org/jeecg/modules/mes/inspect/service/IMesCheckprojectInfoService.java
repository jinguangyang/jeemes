package org.jeecg.modules.mes.inspect.service;

import org.jeecg.modules.mes.inspect.entity.MesCheckprojectInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 检测项目信息
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
public interface IMesCheckprojectInfoService extends IService<MesCheckprojectInfo> {

	public List<MesCheckprojectInfo> selectByMainId(String mainId);
}
