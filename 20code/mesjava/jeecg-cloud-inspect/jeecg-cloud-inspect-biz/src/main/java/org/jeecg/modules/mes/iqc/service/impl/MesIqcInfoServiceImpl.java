package org.jeecg.modules.mes.iqc.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.iqc.entity.MesIqcDefect;
import org.jeecg.modules.mes.iqc.entity.MesIqcInfo;
import org.jeecg.modules.mes.iqc.entity.MesIqcMaterial;
import org.jeecg.modules.mes.iqc.mapper.MesIqcDefectMapper;
import org.jeecg.modules.mes.iqc.mapper.MesIqcInfoMapper;
import org.jeecg.modules.mes.iqc.mapper.MesIqcMaterialMapper;
import org.jeecg.modules.mes.iqc.service.IMesIqcInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Description: 来料检验报告
 * @Author: jeecg-boot
 * @Date:   2021-03-19
 * @Version: V1.0
 */
@Service
public class MesIqcInfoServiceImpl extends ServiceImpl<MesIqcInfoMapper, MesIqcInfo> implements IMesIqcInfoService {


    @Autowired
    private MesIqcDefectMapper mesIqcDefectMapper;

    @Autowired
    private MesIqcInfoMapper mesIqcInfoMapper;

    @Autowired
    private MesIqcMaterialMapper mesIqcMaterialMapper;

    /**
     * 根据iqc物料id查询检验缺陷描述信息
     * @param iqcMaterialId  iqc物料id
     * @return
     */
    @Override
    public List<MesIqcDefect> selectDefectByMainId(String iqcMaterialId) {
        return mesIqcDefectMapper.selectDefectByMainId(iqcMaterialId);
    }

    /**
     * 根据iqc主表id查询多条物料记录
     * @param mainId
     * @return
     */
    @Override
    public List<MesIqcMaterial> selectMaterialByMainId(String mainId) {
        return mesIqcMaterialMapper.selectMaterialByMainId(mainId);
    }

    /**
     * 新增iqc来料检验报告
     * @param info1
     * @param defectlist
     * @param info3
     */
    @Override
    @Transactional
    public void saveMain(MesIqcInfo info1,List<MesIqcDefect> defectlist,MesIqcMaterial info3) {
        mesIqcInfoMapper.insert(info1);
        info3.setIqcId(info1.getId());
        if(StringUtils.isBlank(info3.getAccepNum1())){
            info3.setAccepNum1("0");
        }else if(StringUtils.isBlank(info3.getAccepNum2())){
            info3.setAccepNum2("0");
        }else if(StringUtils.isBlank(info3.getAccepNum3())){
            info3.setAccepNum3("0");
        }else if(StringUtils.isBlank(info3.getRejectionNum1())){
            info3.setRejectionNum1("0");
        }else if(StringUtils.isBlank(info3.getRejectionNum2())){
            info3.setRejectionNum2("0");
        }else if(StringUtils.isBlank(info3.getRejectionNum3())){
            info3.setRejectionNum3("0");
        }

        mesIqcMaterialMapper.insert(info3);
        for (MesIqcDefect info2:defectlist) {
            info2.setIqcMaterialId(info3.getId());

            mesIqcDefectMapper.insert(info2);
        }
    }

}
