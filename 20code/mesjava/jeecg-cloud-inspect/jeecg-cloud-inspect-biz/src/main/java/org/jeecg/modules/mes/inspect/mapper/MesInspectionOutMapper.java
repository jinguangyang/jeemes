package org.jeecg.modules.mes.inspect.mapper;

import java.util.List;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.inspect.entity.MesInspectionOut;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.mes.inspect.vo.InspectVo;
import org.jeecg.modules.mes.inspect.vo.ReportVo;

/**
 * @Description: 出货检验报告
 * @Author: jeecg-boot
 * @Date:   2021-03-20
 * @Version: V1.0
 */
public interface MesInspectionOutMapper extends BaseMapper<MesInspectionOut> {


    //获取良品率图表-按月
    public List<InspectVo> selectLplList(@Param("level") String level,@Param("goal") String goal,@Param("begindate") String begindate,@Param("enddate") String enddate,@Param("linecode") String linecode,@Param("procode") String procode);

    //获取良品率图表--按天
    public List<InspectVo> selectLplListByDay(@Param("level") String level,@Param("goal") String goal,@Param("begindate") String begindate,@Param("enddate") String enddate,@Param("linecode") String linecode,@Param("procode") String procode);

    //获取良品率图表--按周
    public List<InspectVo> selectLplListByWeek(@Param("level") String level,@Param("goal") String goal,@Param("begindate") String begindate,@Param("enddate") String enddate,@Param("linecode") String linecode,@Param("procode") String procode);

    //获取良品率图表--按年
    public List<InspectVo> selectLplListByYear(@Param("level") String level,@Param("goal") String goal,@Param("begindate") String begindate,@Param("enddate") String enddate,@Param("linecode") String linecode,@Param("procode") String procode);


    //获取产线排行数据
    public List<InspectVo> selectLineLplList(@Param("level") String level,@Param("begindate") String begindate,@Param("enddate") String enddate,@Param("linecode") String linecode,@Param("procode") String procode);

    //获取项目不良列表
    public List<InspectVo> selectTopIssue(@Param("level") String level,@Param("begindate") String begindate,@Param("enddate") String enddate,@Param("linecode") String linecode,@Param("procode") String procode);

    //获取部门不良图表
    public List<InspectVo> selectPicLoss(@Param("level") String level,@Param("begindate") String begindate,@Param("enddate") String enddate,@Param("linecode") String linecode,@Param("procode") String procode);

    //获取部门不良列表
    public List<InspectVo> selectDeptPicLoss(@Param("level") String level,@Param("dept") String dept,@Param("begindate") String begindate,@Param("enddate") String enddate,@Param("linecode") String linecode,@Param("procode") String procode);


    public List<ReportVo> selectTld(Page<ReportVo> page, @Param("begindate") String begindate);
}
