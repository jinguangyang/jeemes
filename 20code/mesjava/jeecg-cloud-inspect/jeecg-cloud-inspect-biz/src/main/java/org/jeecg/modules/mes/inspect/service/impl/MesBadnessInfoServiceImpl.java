package org.jeecg.modules.mes.inspect.service.impl;

import org.jeecg.modules.mes.inspect.entity.MesBadnessInfo;
import org.jeecg.modules.mes.inspect.mapper.MesBadnessInfoMapper;
import org.jeecg.modules.mes.inspect.service.IMesBadnessInfoService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 检验不良信息
 * @Author: jeecg-boot
 * @Date:   2020-10-29
 * @Version: V1.0
 */
@Service
public class MesBadnessInfoServiceImpl extends ServiceImpl<MesBadnessInfoMapper, MesBadnessInfo> implements IMesBadnessInfoService {
	
	@Autowired
	private MesBadnessInfoMapper mesBadnessInfoMapper;
	
	@Override
	public List<MesBadnessInfo> selectByMainId(String mainId) {
		return mesBadnessInfoMapper.selectByMainId(mainId);
	}
}
