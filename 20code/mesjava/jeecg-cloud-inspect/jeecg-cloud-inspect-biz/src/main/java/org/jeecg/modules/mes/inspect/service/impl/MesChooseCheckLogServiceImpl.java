package org.jeecg.modules.mes.inspect.service.impl;

import org.jeecg.modules.mes.inspect.entity.MesChooseCheckLog;
import org.jeecg.modules.mes.inspect.mapper.MesChooseCheckLogMapper;
import org.jeecg.modules.mes.inspect.service.IMesChooseCheckLogService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 抽检抽测表
 * @Author: jeecg-boot
 * @Date:   2021-03-31
 * @Version: V1.0
 */
@Service
public class MesChooseCheckLogServiceImpl extends ServiceImpl<MesChooseCheckLogMapper, MesChooseCheckLog> implements IMesChooseCheckLogService {

}
