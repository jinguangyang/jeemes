package org.jeecg.modules.mes.iqc.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.mes.iqc.entity.MesIqcDefect;
import org.jeecg.modules.mes.iqc.entity.MesIqcInfo;
import org.jeecg.modules.mes.iqc.entity.MesIqcMaterial;
import org.jeecg.modules.mes.iqc.service.IMesIqcDefectService;
import org.jeecg.modules.mes.iqc.service.IMesIqcInfoService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.mes.iqc.service.IMesIqcMaterialService;
import org.jeecg.modules.mes.iqc.vo.MesIqcInfoPage;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 来料检验报告
 * @Author: jeecg-boot
 * @Date:   2021-03-19
 * @Version: V1.0
 */
@Api(tags="来料检验报告")
@RestController
@RequestMapping("/iqc/mesIqcInfo")
@Slf4j
public class MesIqcInfoController extends JeecgController<MesIqcInfo, IMesIqcInfoService> {
	@Autowired
	private IMesIqcInfoService mesIqcInfoService;

	 @Autowired
	 private IMesIqcDefectService mesIqcDefectService;

	 @Autowired
	 private IMesIqcMaterialService mesIqcMaterialService;
	
	/**
	 * 分页列表查询
	 *
	 * @param mesIqcInfo
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "来料检验报告-分页列表查询")
	@ApiOperation(value="来料检验报告-分页列表查询", notes="来料检验报告-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesIqcInfo mesIqcInfo,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesIqcInfo> queryWrapper = QueryGenerator.initQueryWrapper(mesIqcInfo, req.getParameterMap());
		Page<MesIqcInfo> page = new Page<MesIqcInfo>(pageNo, pageSize);
		IPage<MesIqcInfo> pageList = mesIqcInfoService.page(page, queryWrapper);
		//System.out.println(pageList.getRecords().get());
//		List<MesIqcInfo> list =  pageList.getRecords();
//		List<MesIqcInfoPage> pagelist1 = new ArrayList<>();

//		for (int i=0;i<list.size();i++) {
//			MesIqcInfo iqcInfo=list.get(i);
//			MesIqcInfoPage pageinfo = new MesIqcInfoPage();
//
//			BeanUtils.copyProperties(iqcInfo, pageinfo);
//			QueryWrapper<MesIqcMaterial> queryWrapper1 = new QueryWrapper<>();
//			queryWrapper1.eq("iqc_id",pageinfo.getId());
//			MesIqcMaterial mesIqcMaterial =mesIqcMaterialService.getOne(queryWrapper1);
//			pageinfo.setMaterialinfo(mesIqcMaterial);
//
//			if(mesIqcMaterial!=null) {
//				QueryWrapper<MesIqcDefect> queryWrapper2 = new QueryWrapper<>();
//				queryWrapper2.eq("iqc_material_id",mesIqcMaterial.getId());
//				List<MesIqcDefect> list1=  mesIqcDefectService.list(queryWrapper2);
//				pageinfo.setDefectlist(list1);
//			}
//			pagelist1.add(pageinfo);
//		}
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param mesIqcInfo
	 * @return
	 */
	/*@AutoLog(value = "来料检验报告-添加")
	@ApiOperation(value="来料检验报告-添加", notes="来料检验报告-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MesIqcInfo mesIqcInfo) {
		mesIqcInfoService.save(mesIqcInfo);
		return Result.ok("添加成功！");
	}*/

	 /**
	  *   添加
	  *
	  * @param mesIqcInfo
	  * @return
	  */
	 @AutoLog(value = "来料检验报告-添加")
	 @ApiOperation(value="来料检验报告-添加", notes="来料检验报告-添加")
	 @PostMapping(value = "/addMain")
	 public Result<?> addMain(@RequestBody MesIqcInfo mesIqcInfo, List<MesIqcDefect> defectlist, MesIqcMaterial mesIqcMaterial) {
		 mesIqcInfoService.saveMain(mesIqcInfo,defectlist,mesIqcMaterial);
		 return Result.ok("添加成功！");
	 }
	
	/**
	 *  编辑
	 *
	 * @param mesIqcInfo
	 * @return
	 */
	@AutoLog(value = "来料检验报告-编辑")
	@ApiOperation(value="来料检验报告-编辑", notes="来料检验报告-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody MesIqcInfo mesIqcInfo) {
		mesIqcInfoService.updateById(mesIqcInfo);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "来料检验报告-通过id删除")
	@ApiOperation(value="来料检验报告-通过id删除", notes="来料检验报告-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		mesIqcInfoService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "来料检验报告-批量删除")
	@ApiOperation(value="来料检验报告-批量删除", notes="来料检验报告-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.mesIqcInfoService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "来料检验报告-通过id查询")
	@ApiOperation(value="来料检验报告-通过id查询", notes="来料检验报告-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		MesIqcInfo mesIqcInfo = mesIqcInfoService.getById(id);
		if(mesIqcInfo==null) {
			return Result.error("未找到对应数据");
		}
		MesIqcInfoPage pageinfo = new MesIqcInfoPage();
		BeanUtils.copyProperties(mesIqcInfo, pageinfo);
		//来料物料信息
		QueryWrapper<MesIqcMaterial> queryWrapper1 = new QueryWrapper<>();
		queryWrapper1.eq("iqc_id",pageinfo.getId());
		MesIqcMaterial mesIqcMaterial =mesIqcMaterialService.getOne(queryWrapper1);
		pageinfo.setMaterialinfo(mesIqcMaterial);

		if(mesIqcMaterial!=null) {
			QueryWrapper<MesIqcDefect> queryWrapper2 = new QueryWrapper<>();
			queryWrapper2.eq("iqc_material_id",mesIqcMaterial.getId());
			List<MesIqcDefect> list1=  mesIqcDefectService.list(queryWrapper2);
			pageinfo.setDefectlist(list1);
		}

		return Result.ok(pageinfo);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param mesIqcInfo
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesIqcInfo mesIqcInfo) {
        return super.exportXls(request, mesIqcInfo, MesIqcInfo.class, "来料检验报告");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesIqcInfo.class);
    }

}
