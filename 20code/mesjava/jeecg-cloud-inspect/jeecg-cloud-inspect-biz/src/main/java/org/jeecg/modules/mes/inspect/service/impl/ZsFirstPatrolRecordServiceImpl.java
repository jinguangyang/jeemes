package org.jeecg.modules.mes.inspect.service.impl;

import org.jeecg.modules.mes.inspect.entity.ZsFirstPatrolRecord;
import org.jeecg.modules.mes.inspect.mapper.ZsFirstPatrolRecordMapper;
import org.jeecg.modules.mes.inspect.service.IZsFirstPatrolRecordService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 宗申首、巡、末件检验记录表
 * @Author: jeecg-boot
 * @Date:   2021-05-15
 * @Version: V1.0
 */
@Service
public class ZsFirstPatrolRecordServiceImpl extends ServiceImpl<ZsFirstPatrolRecordMapper, ZsFirstPatrolRecord> implements IZsFirstPatrolRecordService {

}
