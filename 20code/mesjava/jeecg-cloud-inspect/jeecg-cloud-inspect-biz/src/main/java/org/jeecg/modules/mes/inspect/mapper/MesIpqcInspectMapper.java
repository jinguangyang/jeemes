package org.jeecg.modules.mes.inspect.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.jeecg.modules.mes.inspect.entity.MesIpqcInspect;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 质检中心-IPQC检测项
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
public interface MesIpqcInspectMapper extends BaseMapper<MesIpqcInspect> {

    @Select("select COUNT(*) FROM mes_iqc_info")
    public String inspectTotle();
    @Select("select COUNT(*) FROM mes_inspection_out")
    public String inspectOutTotle();
    @Select("select SUM(o.bad_num+m.samples_num) FROM mes_inspection_out o,mes_iqc_material m")
    public String inspectIqcOutSum();


}
