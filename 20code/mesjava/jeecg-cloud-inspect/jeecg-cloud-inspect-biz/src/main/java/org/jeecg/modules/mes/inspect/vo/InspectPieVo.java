package org.jeecg.modules.mes.inspect.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class InspectPieVo implements Serializable {

    private Long value;
    private String name;
}
