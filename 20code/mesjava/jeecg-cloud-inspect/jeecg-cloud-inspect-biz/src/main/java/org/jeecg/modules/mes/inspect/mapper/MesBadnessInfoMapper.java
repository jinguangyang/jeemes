package org.jeecg.modules.mes.inspect.mapper;

import java.util.List;
import org.jeecg.modules.mes.inspect.entity.MesBadnessInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 检验不良信息
 * @Author: jeecg-boot
 * @Date:   2020-10-29
 * @Version: V1.0
 */
public interface MesBadnessInfoMapper extends BaseMapper<MesBadnessInfo> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<MesBadnessInfo> selectByMainId(@Param("mainId") String mainId);

}
