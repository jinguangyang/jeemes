package org.jeecg.modules.mes.inspect.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.mes.inspect.entity.MesIpqcInspect;
import org.jeecg.modules.mes.inspect.service.IMesIpqcInspectService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 质检中心-IPQC检测项
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
@Api(tags="质检中心-IPQC检测项")
@RestController
@RequestMapping("/inspect/mesIpqcInspect")
@Slf4j
public class MesIpqcInspectController extends JeecgController<MesIpqcInspect, IMesIpqcInspectService> {
	@Autowired
	private IMesIpqcInspectService mesIpqcInspectService;


	/**
	 * 分页列表查询
	 *
	 * @param mesIpqcInspect
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "质检中心-IPQC检测项-分页列表查询")
	@ApiOperation(value="质检中心-IPQC检测项-分页列表查询", notes="质检中心-IPQC检测项-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesIpqcInspect mesIpqcInspect,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesIpqcInspect> queryWrapper = QueryGenerator.initQueryWrapper(mesIpqcInspect, req.getParameterMap());
		Page<MesIpqcInspect> page = new Page<MesIpqcInspect>(pageNo, pageSize);
		IPage<MesIpqcInspect> pageList = mesIpqcInspectService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param mesIpqcInspect
	 * @return
	 */
	@AutoLog(value = "质检中心-IPQC检测项-添加")
	@ApiOperation(value="质检中心-IPQC检测项-添加", notes="质检中心-IPQC检测项-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MesIpqcInspect mesIpqcInspect) {
		mesIpqcInspectService.save(mesIpqcInspect);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param mesIpqcInspect
	 * @return
	 */
	@AutoLog(value = "质检中心-IPQC检测项-编辑")
	@ApiOperation(value="质检中心-IPQC检测项-编辑", notes="质检中心-IPQC检测项-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody MesIpqcInspect mesIpqcInspect) {
		mesIpqcInspectService.updateById(mesIpqcInspect);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "质检中心-IPQC检测项-通过id删除")
	@ApiOperation(value="质检中心-IPQC检测项-通过id删除", notes="质检中心-IPQC检测项-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		mesIpqcInspectService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "质检中心-IPQC检测项-批量删除")
	@ApiOperation(value="质检中心-IPQC检测项-批量删除", notes="质检中心-IPQC检测项-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.mesIpqcInspectService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "质检中心-IPQC检测项-通过id查询")
	@ApiOperation(value="质检中心-IPQC检测项-通过id查询", notes="质检中心-IPQC检测项-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		MesIpqcInspect mesIpqcInspect = mesIpqcInspectService.getById(id);
		if(mesIpqcInspect==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(mesIpqcInspect);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param mesIpqcInspect
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesIpqcInspect mesIpqcInspect) {
        return super.exportXls(request, mesIpqcInspect, MesIpqcInspect.class, "质检中心-IPQC检测项");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesIpqcInspect.class);
    }

	/**
	 * 来料质检总单数
	 */
	@GetMapping(value = "/inspectTotle")
	public String inspectTotle(){
		return mesIpqcInspectService.inspectTotle();
	}
	/**
	 * 出库质检总单数
	 */
	@GetMapping(value = "/inspectOutTotle")
	public String inspectOutTotle(){
		return mesIpqcInspectService.inspectOutTotle();
	}
	/**
	 * 出入库质检总单数
	 */
	@GetMapping(value = "/inspectIqcOutSum")
	public String inspectIqcOutSum(){
		return mesIpqcInspectService.inspectIqcOutSum();
	}

}
