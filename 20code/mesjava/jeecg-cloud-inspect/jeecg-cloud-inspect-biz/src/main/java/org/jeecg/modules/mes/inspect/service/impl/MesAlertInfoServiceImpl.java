package org.jeecg.modules.mes.inspect.service.impl;

import org.jeecg.modules.mes.inspect.entity.MesAlertInfo;
import org.jeecg.modules.mes.inspect.mapper.MesAlertInfoMapper;
import org.jeecg.modules.mes.inspect.service.IMesAlertInfoService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 质检中心-报警信息
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
@Service
public class MesAlertInfoServiceImpl extends ServiceImpl<MesAlertInfoMapper, MesAlertInfo> implements IMesAlertInfoService {

}
