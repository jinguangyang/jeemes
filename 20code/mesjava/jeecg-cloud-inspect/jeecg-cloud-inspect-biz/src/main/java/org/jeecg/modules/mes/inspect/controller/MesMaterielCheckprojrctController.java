package org.jeecg.modules.mes.inspect.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.mes.inspect.entity.MesCheckprojectInfo;
import org.jeecg.modules.mes.inspect.entity.MesMaterielCheckprojrct;
import org.jeecg.modules.mes.inspect.service.IMesCheckprojectInfoService;
import org.jeecg.modules.mes.inspect.service.IMesMaterielCheckprojrctService;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

 /**
 * @Description: 质检中心-物料检测项目
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
@Api(tags="质检中心-物料检测项目")
@RestController
@RequestMapping("/inspect/mesMaterielCheckprojrct")
@Slf4j
public class MesMaterielCheckprojrctController extends JeecgController<MesMaterielCheckprojrct, IMesMaterielCheckprojrctService> {

	@Autowired
	private IMesMaterielCheckprojrctService mesMaterielCheckprojrctService;

	@Autowired
	private IMesCheckprojectInfoService mesCheckprojectInfoService;


	/*---------------------------------主表处理-begin-------------------------------------*/

	/**
	 * 分页列表查询
	 * @param mesMaterielCheckprojrct
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "质检中心-物料检测项目-分页列表查询")
	@ApiOperation(value="质检中心-物料检测项目-分页列表查询", notes="质检中心-物料检测项目-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesMaterielCheckprojrct mesMaterielCheckprojrct,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesMaterielCheckprojrct> queryWrapper = QueryGenerator.initQueryWrapper(mesMaterielCheckprojrct, req.getParameterMap());
		Page<MesMaterielCheckprojrct> page = new Page<MesMaterielCheckprojrct>(pageNo, pageSize);
		IPage<MesMaterielCheckprojrct> pageList = mesMaterielCheckprojrctService.page(page, queryWrapper);
		if(pageList.getSize() != 0){
			List<MesMaterielCheckprojrct> checkprojrctList = pageList.getRecords();
			for (MesMaterielCheckprojrct checkprojrct : checkprojrctList) {
				//如果检测阶别不为空，调用查询接口时，自动补全检测类型：103收货检验、262入库检验、201扫描入库检验
				System.err.println(checkprojrct.getCheckType());
				String checkType = checkprojrct.getCheckType();//检测类型
				String checkGrade = checkprojrct.getCheckGrade();//检测阶别
				if(StringUtils.isBlank(checkType) && StringUtils.isNotBlank(checkGrade)){
					if(checkGrade.equals("103")){
						checkprojrct.setCheckType("收货检验");
						mesMaterielCheckprojrctService.updateById(checkprojrct);
					}else if(checkGrade.equals("262")){
						checkprojrct.setCheckType("入库检验");
						mesMaterielCheckprojrctService.updateById(checkprojrct);
					}else if(checkGrade.equals("201")){
						checkprojrct.setCheckType("扫描入库检验");
						mesMaterielCheckprojrctService.updateById(checkprojrct);
					}
				}
			}
		}
		IPage<MesMaterielCheckprojrct> pageList1 = mesMaterielCheckprojrctService.page(page, queryWrapper);
		return Result.ok(pageList1);
	}

	 /**
	  * 通过检测阶别和料号查询
	  *
	  * @param
	  * @param inspectGrade 检测阶别
	  * @param materielCode 机种/物料
	  * @return
	  */
	 @AutoLog(value = "质检中心-物料检测项目-通过检测阶别和料号查询")
	 @ApiOperation(value="质检中心-物料检测项目-通过检测阶别和料号查询", notes="质检中心-物料检测项目-通过检测阶别和料号查询")
	 @GetMapping(value = "/getByGrade")
	 public Result<?> getByGrade(@RequestParam(name="inspectGrade",required=true) String inspectGrade,
							     @RequestParam(name="materielCode",required=true) String materielCode) {
		 System.out.println("=========inspectGrade=" + inspectGrade + "=========materielCode=" + materielCode);
		 QueryWrapper<MesMaterielCheckprojrct> wrapper = new QueryWrapper<>();
		 wrapper.eq("check_grade", inspectGrade).eq("mat_or_mac", materielCode);
		 MesMaterielCheckprojrct materielCheckprojrct = mesMaterielCheckprojrctService.getOne(wrapper);
		 if (materielCheckprojrct == null) {
			 List<MesCheckprojectInfo> checkprojects = definspectGrade(inspectGrade);
			 if(checkprojects!=null){
			 	return Result.OK(checkprojects);
			 }
			 return Result.error("找不到物料检测信息的相应数据,请配置检测项目！");
		 }
		 QueryWrapper<MesCheckprojectInfo> queryWrapper = new QueryWrapper<>();
		 queryWrapper.eq("mcheckproject_id", materielCheckprojrct.getId());
		 List<MesCheckprojectInfo> checkprojectInfo = mesCheckprojectInfoService.list(queryWrapper);
		 if (checkprojectInfo == null||(checkprojectInfo != null&&checkprojectInfo.size()==0)){
			 List<MesCheckprojectInfo> checkprojects = definspectGrade(inspectGrade);
			 if(checkprojects!=null){
				 return Result.OK(checkprojects);
			 }
			 return Result.error("找不到检测项目信息的相应数据,请配置检测项目！");
		 }else{
			 List<MesCheckprojectInfo> checkprojects = definspectGrade(inspectGrade);
			 if(checkprojects!=null){
				 System.out.println(checkprojectInfo.size()+" "+checkprojects.size());
				 checkprojects.addAll(checkprojectInfo);
				 return Result.OK(checkprojects);
			 }
			 return Result.ok(checkprojectInfo);
		 }
	 }

	 /**
	  * 根据物料阶别编码获取默认项目
	  * @param inspectGrade
	  * @return
	  */
    public List<MesCheckprojectInfo> definspectGrade(String inspectGrade){
		List<MesCheckprojectInfo> checkprojects = new ArrayList<>();
		if("103".equals(inspectGrade)||"302".equals(inspectGrade)){//物料检测项目为空，且阶别103的时候
			MesCheckprojectInfo mesCheckprojectInfo = new MesCheckprojectInfo();
			mesCheckprojectInfo.setCheckGrade(inspectGrade);
			mesCheckprojectInfo.setProjectName("外观");
			mesCheckprojectInfo.setQuery2("品质部");
			checkprojects.add(mesCheckprojectInfo);
			MesCheckprojectInfo mesCheckprojectInfo1 = new MesCheckprojectInfo();
			mesCheckprojectInfo1.setCheckGrade(inspectGrade);
			mesCheckprojectInfo1.setProjectName("功能");
			mesCheckprojectInfo1.setQuery2("品质部");
			checkprojects.add(mesCheckprojectInfo1);
			MesCheckprojectInfo mesCheckprojectInfo2 = new MesCheckprojectInfo();
			mesCheckprojectInfo2.setCheckGrade(inspectGrade);
			mesCheckprojectInfo2.setProjectName("尺寸");
			mesCheckprojectInfo2.setQuery2("品质部");
			checkprojects.add(mesCheckprojectInfo2);
			if ("302".equals(inspectGrade)) {
				MesCheckprojectInfo mesCheckprojectInfo3 = new MesCheckprojectInfo();
				mesCheckprojectInfo3.setCheckGrade(inspectGrade);
				mesCheckprojectInfo3.setProjectName("包装");
				mesCheckprojectInfo3.setQuery2("品质部");
				checkprojects.add(mesCheckprojectInfo3);
			}
		}
		if("262".equals(inspectGrade)||"301".equals(inspectGrade)){//不良现象
			MesCheckprojectInfo mcpInfo = new MesCheckprojectInfo();
			mcpInfo.setCheckGrade(inspectGrade);
			mcpInfo.setProjectName("极反");
			mcpInfo.setQuery2("品质部");
			MesCheckprojectInfo mcpInfo2 = new MesCheckprojectInfo();
			mcpInfo2.setCheckGrade(inspectGrade);
			mcpInfo2.setProjectName("短路");
			mcpInfo2.setQuery2("品质部");
			MesCheckprojectInfo mcpInfo3 = new MesCheckprojectInfo();
			mcpInfo3.setCheckGrade(inspectGrade);
			mcpInfo3.setProjectName("缺件");
			mcpInfo3.setQuery2("品质部");
			MesCheckprojectInfo mcpInfo4 = new MesCheckprojectInfo();
			mcpInfo4.setCheckGrade(inspectGrade);
			mcpInfo4.setProjectName("错件");
			mcpInfo4.setQuery2("品质部");
			MesCheckprojectInfo mcpInfo5 = new MesCheckprojectInfo();
			mcpInfo5.setCheckGrade(inspectGrade);
			mcpInfo5.setProjectName("空焊");
			mcpInfo5.setQuery2("品质部");
			MesCheckprojectInfo mcpInfo6 = new MesCheckprojectInfo();
			mcpInfo6.setCheckGrade(inspectGrade);
			mcpInfo6.setProjectName("冷焊");
			mcpInfo6.setQuery2("品质部");
			MesCheckprojectInfo mcpInfo7 = new MesCheckprojectInfo();
			mcpInfo7.setCheckGrade(inspectGrade);
			mcpInfo7.setProjectName("假焊");
			mcpInfo7.setQuery2("品质部");
			MesCheckprojectInfo mcpInfo8 = new MesCheckprojectInfo();
			mcpInfo8.setCheckGrade(inspectGrade);
			mcpInfo8.setProjectName("刮伤");
			mcpInfo8.setQuery2("品质部");
			MesCheckprojectInfo mcpInfo9 = new MesCheckprojectInfo();
			mcpInfo9.setCheckGrade(inspectGrade);
			mcpInfo9.setProjectName("脏污");
			mcpInfo9.setQuery2("品质部");
			MesCheckprojectInfo mcpInfo10 = new MesCheckprojectInfo();
			mcpInfo10.setCheckGrade(inspectGrade);
			mcpInfo10.setProjectName("高翘");
			mcpInfo10.setQuery2("品质部");
			MesCheckprojectInfo mcpInfo11 = new MesCheckprojectInfo();
			mcpInfo11.setCheckGrade(inspectGrade);
			mcpInfo11.setProjectName("侧立");
			mcpInfo11.setQuery2("品质部");
			MesCheckprojectInfo mcpInfo12 = new MesCheckprojectInfo();
			mcpInfo12.setCheckGrade(inspectGrade);
			mcpInfo12.setProjectName("浮高");
			mcpInfo12.setQuery2("品质部");
			MesCheckprojectInfo mcpInfo13 = new MesCheckprojectInfo();
			mcpInfo13.setCheckGrade(inspectGrade);
			mcpInfo13.setProjectName("损件");
			mcpInfo13.setQuery2("品质部");
			MesCheckprojectInfo mcpInfo14 = new MesCheckprojectInfo();
			mcpInfo14.setCheckGrade(inspectGrade);
			mcpInfo14.setProjectName("少锡");
			mcpInfo14.setQuery2("品质部");
			MesCheckprojectInfo mcpInfo15 = new MesCheckprojectInfo();
			mcpInfo15.setCheckGrade(inspectGrade);
			mcpInfo15.setProjectName("偏移");
			mcpInfo15.setQuery2("品质部");
			MesCheckprojectInfo mcpInfo16 = new MesCheckprojectInfo();
			mcpInfo16.setCheckGrade(inspectGrade);
			mcpInfo16.setProjectName("多件");
			mcpInfo16.setQuery2("品质部");
			MesCheckprojectInfo mcpInfo17 = new MesCheckprojectInfo();
			mcpInfo17.setCheckGrade(inspectGrade);
			mcpInfo17.setProjectName("变形");
			mcpInfo17.setQuery2("品质部");
			MesCheckprojectInfo mcpInfo18 = new MesCheckprojectInfo();
			mcpInfo18.setCheckGrade(inspectGrade);
			mcpInfo18.setProjectName("针孔");
			mcpInfo18.setQuery2("品质部");
			MesCheckprojectInfo mcpInfo19 = new MesCheckprojectInfo();
			mcpInfo19.setCheckGrade(inspectGrade);
			mcpInfo19.setProjectName("锡珠");
			mcpInfo19.setQuery2("品质部");
			MesCheckprojectInfo mcpInfo20 = new MesCheckprojectInfo();
			mcpInfo20.setCheckGrade(inspectGrade);
			mcpInfo20.setProjectName("立碑");
			mcpInfo20.setQuery2("品质部");
			checkprojects=new ArrayList<>(Arrays.asList(mcpInfo,mcpInfo2,mcpInfo3,
					mcpInfo4,mcpInfo5,mcpInfo6,mcpInfo7,mcpInfo8,mcpInfo9,mcpInfo10,mcpInfo11,
					mcpInfo12,mcpInfo13,mcpInfo14,mcpInfo15,mcpInfo16,mcpInfo17,mcpInfo18,mcpInfo19,mcpInfo20));
		}
		if("201".equals(inspectGrade)) {//首件
			MesCheckprojectInfo mcpInfo = new MesCheckprojectInfo();
			mcpInfo.setCheckGrade(inspectGrade);
			mcpInfo.setProjectName("上板检查");
			mcpInfo.setQuery2("品质部");
			MesCheckprojectInfo mcpInfo1 = new MesCheckprojectInfo();
			mcpInfo1.setCheckGrade(inspectGrade);
			mcpInfo1.setProjectName("检查助焊剂/锡膏/物料是否有ROSH标识");
			mcpInfo1.setQuery2("品质部");
			MesCheckprojectInfo mcpInfo2 = new MesCheckprojectInfo();
			mcpInfo2.setCheckGrade(inspectGrade);
			mcpInfo2.setProjectName("锡膏状态/钢网编码检查");
			mcpInfo2.setQuery2("品质部");
			MesCheckprojectInfo mcpInfo3 = new MesCheckprojectInfo();
			mcpInfo3.setCheckGrade(inspectGrade);
			mcpInfo3.setProjectName("锡膏解冻时间/登记时间确认");
			mcpInfo3.setQuery2("品质部");
			MesCheckprojectInfo mcpInfo4 = new MesCheckprojectInfo();
			mcpInfo4.setCheckGrade(inspectGrade);
			mcpInfo4.setProjectName("辅料/刮刀/搅拌时间确认");
			mcpInfo4.setQuery2("品质部");
			MesCheckprojectInfo mcpInfo5 = new MesCheckprojectInfo();
			mcpInfo5.setCheckGrade(inspectGrade);
			mcpInfo5.setProjectName("印刷参数确认（需记录数据）");
			mcpInfo5.setQuery2("品质部");
			MesCheckprojectInfo mcpInfo6 = new MesCheckprojectInfo();
			mcpInfo6.setCheckGrade(inspectGrade);
			mcpInfo6.setProjectName("印刷品质检查");
			mcpInfo6.setQuery2("品质部");
			MesCheckprojectInfo mcpInfo7= new MesCheckprojectInfo();
			mcpInfo7.setCheckGrade(inspectGrade);
			mcpInfo7.setProjectName("锡膏厚度测试（记录SPI数据）");
			mcpInfo7.setQuery2("品质部");
			MesCheckprojectInfo mcpInfo8 = new MesCheckprojectInfo();
			mcpInfo8.setCheckGrade(inspectGrade);
			mcpInfo8.setProjectName("BOM/贴片图校对");
			mcpInfo8.setQuery2("品质部");
			MesCheckprojectInfo mcpInfo9 = new MesCheckprojectInfo();
			mcpInfo9.setCheckGrade(inspectGrade);
			mcpInfo9.setProjectName("贴片程序及版本核对");
			mcpInfo9.setQuery2("品质部");
			MesCheckprojectInfo mcpInfo10 = new MesCheckprojectInfo();
			mcpInfo10.setCheckGrade(inspectGrade);
			mcpInfo10.setProjectName("物料核对（物料名称/规格/供应商/批次号）");
			mcpInfo10.setQuery2("品质部");
			MesCheckprojectInfo mcpInfo11 = new MesCheckprojectInfo();
			mcpInfo11.setCheckGrade(inspectGrade);
			mcpInfo11.setProjectName("料站排位表核对");
			mcpInfo11.setQuery2("品质部");
			MesCheckprojectInfo mcpInfo12 = new MesCheckprojectInfo();
			mcpInfo12.setCheckGrade(inspectGrade);
			mcpInfo12.setProjectName("元件极性核对（注明位号、标注极性的方向）");
			mcpInfo12.setQuery2("品质部");
			MesCheckprojectInfo mcpInfo13 = new MesCheckprojectInfo();
			mcpInfo13.setCheckGrade(inspectGrade);
			mcpInfo13.setProjectName("回流焊曲线和参数（记录文件名，核对炉温参数）");
			mcpInfo13.setQuery2("品质部");
			MesCheckprojectInfo mcpInfo14 = new MesCheckprojectInfo();
			mcpInfo14.setCheckGrade(inspectGrade);
			mcpInfo14.setProjectName("炉前样机（新产品需拍照确认）");
			mcpInfo14.setQuery2("品质部");
			MesCheckprojectInfo mcpInfo15 = new MesCheckprojectInfo();
			mcpInfo15.setCheckGrade(inspectGrade);
			mcpInfo15.setProjectName("炉后可靠性检验（工艺要求、熔锡状况、）");
			mcpInfo15.setQuery2("品质部");
			MesCheckprojectInfo mcpInfo16 = new MesCheckprojectInfo();
			mcpInfo16.setCheckGrade(inspectGrade);
			mcpInfo16.setProjectName("炉后外观检查（移位、漏件、立碑）");
			mcpInfo16.setQuery2("品质部");

			checkprojects=new ArrayList<>(Arrays.asList(mcpInfo,mcpInfo1,mcpInfo2,mcpInfo3,
					mcpInfo4,mcpInfo5,mcpInfo6,mcpInfo7,mcpInfo8,mcpInfo9,mcpInfo10,mcpInfo11,
					mcpInfo12,mcpInfo13,mcpInfo14,mcpInfo15,mcpInfo16));
		}

		return checkprojects;
	}
	/**
     *   添加
     * @param mesMaterielCheckprojrct
     * @return
     */
    @AutoLog(value = "质检中心-物料检测项目-添加")
    @ApiOperation(value="质检中心-物料检测项目-添加", notes="质检中心-物料检测项目-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody MesMaterielCheckprojrct mesMaterielCheckprojrct) {
    	if(StringUtils.isNotBlank(mesMaterielCheckprojrct.getMatOrMac()) && StringUtils.isNotBlank(mesMaterielCheckprojrct.getCheckGrade())){
    		String mCode = mesMaterielCheckprojrct.getMatOrMac();//物料料号
    		String grade = mesMaterielCheckprojrct.getCheckGrade();//检测阶别
			//一个物料料号和一个检测阶别，确定唯一的物料检测项目
			QueryWrapper<MesMaterielCheckprojrct> queryWrapper = new QueryWrapper<>();
			queryWrapper.eq("mat_or_mac",mCode).eq("check_grade",grade);
			List<MesMaterielCheckprojrct> checkprojrctList = mesMaterielCheckprojrctService.list(queryWrapper);
			if(checkprojrctList.size() == 0){
				mesMaterielCheckprojrctService.save(mesMaterielCheckprojrct);
			}else {
				return Result.error("出现重复数据！请检查！");
			}
		}else {
    		return Result.error("料号和检测阶别不能为空！请检查！");
		}
        return Result.ok("添加成功！");
    }

    /**
     *  编辑
     * @param mesMaterielCheckprojrct
     * @return
     */
    @AutoLog(value = "质检中心-物料检测项目-编辑")
    @ApiOperation(value="质检中心-物料检测项目-编辑", notes="质检中心-物料检测项目-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody MesMaterielCheckprojrct mesMaterielCheckprojrct) {
		if(StringUtils.isNotBlank(mesMaterielCheckprojrct.getMatOrMac()) && StringUtils.isNotBlank(mesMaterielCheckprojrct.getCheckGrade())){
			String mCode = mesMaterielCheckprojrct.getMatOrMac();//物料料号
			String grade = mesMaterielCheckprojrct.getCheckGrade();//检测阶别
			//一个物料料号和一个检测阶别，确定唯一的物料检测项目
			QueryWrapper<MesMaterielCheckprojrct> queryWrapper = new QueryWrapper<>();
			queryWrapper.eq("mat_or_mac",mCode).eq("check_grade",grade);
			List<MesMaterielCheckprojrct> checkprojrctList = mesMaterielCheckprojrctService.list(queryWrapper);
			if(checkprojrctList.size() == 0){
				mesMaterielCheckprojrctService.updateById(mesMaterielCheckprojrct);
			}else {
				return Result.error("数据的料号或检测阶别重复！请检查！");
			}
		}else {
			return Result.error("料号和检测阶别不能为空！请检查！");
		}
        return Result.ok("编辑成功!");
    }

    /**
     * 通过id删除
     * @param id
     * @return
     */
    @AutoLog(value = "质检中心-物料检测项目-通过id删除")
    @ApiOperation(value="质检中心-物料检测项目-通过id删除", notes="质检中心-物料检测项目-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name="id",required=true) String id) {
        mesMaterielCheckprojrctService.delMain(id);
        return Result.ok("删除成功!");
    }

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @AutoLog(value = "质检中心-物料检测项目-批量删除")
    @ApiOperation(value="质检中心-物料检测项目-批量删除", notes="质检中心-物料检测项目-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
        this.mesMaterielCheckprojrctService.delBatchMain(Arrays.asList(ids.split(",")));
        return Result.ok("批量删除成功!");
    }

    /**
     * 导出
     * @return
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesMaterielCheckprojrct mesMaterielCheckprojrct) {
        return super.exportXls(request, mesMaterielCheckprojrct, MesMaterielCheckprojrct.class, "质检中心-物料检测项目");
    }

//    /**
//     * 导入
//     * @return
//     */
//    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
//    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
//        return super.importExcel(request, response, MesMaterielCheckprojrct.class);
//    }

	 @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
	 public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
		 MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		 Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
		 for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
			 MultipartFile file = entity.getValue();// 获取上传文件对象
			 ImportParams params = new ImportParams();
			 params.setTitleRows(2);
			 params.setHeadRows(1);
			 params.setNeedSave(true);
			 try {
				 List<MesMaterielCheckprojrct> list = ExcelImportUtil.importExcel(file.getInputStream(), MesMaterielCheckprojrct.class, params);
				 int i = 0;
				 for (MesMaterielCheckprojrct checkprojrct : list) {
					 System.err.println("料号================================="+checkprojrct.getMatOrMac());
					 if(StringUtils.isNotBlank(checkprojrct.getMatOrMac()) && StringUtils.isNotBlank(checkprojrct.getCheckGrade())){
						 String mCode = checkprojrct.getMatOrMac();//物料料号
						 String grade = checkprojrct.getCheckGrade();//检测阶别
						 //通过料号和检测阶别，进行数据去重
						 QueryWrapper<MesMaterielCheckprojrct> queryWrapper = new QueryWrapper<>();
						 queryWrapper.eq("mat_or_mac",mCode).eq("check_grade",grade);;
						 List<MesMaterielCheckprojrct> checkprojrctList = mesMaterielCheckprojrctService.list(queryWrapper);
						 if(checkprojrctList.size() == 0){
							 mesMaterielCheckprojrctService.save(checkprojrct);
							 i+=1;
						 }
					 }else {
						 return Result.error("料号和检测阶别不能为空！请检查！");
					 }
				 }
				 //update-begin-author:taoyan date:20190528 for:批量插入数据
				 long start = System.currentTimeMillis();
				 //400条 saveBatch消耗时间1592毫秒  循环插入消耗时间1947毫秒
				 //1200条  saveBatch消耗时间3687毫秒 循环插入消耗时间5212毫秒
				 log.info("消耗时间" + (System.currentTimeMillis() - start) + "毫秒");
				 //update-end-author:taoyan date:20190528 for:批量插入数据
				 return Result.ok("文件导入成功！数据行数：" + String.valueOf(i));
			 } catch (Exception e) {
				 log.error(e.getMessage(), e);
				 return Result.error("文件导入失败:" + e.getMessage());
			 } finally {
				 try {
					 file.getInputStream().close();
				 } catch (IOException e) {
					 e.printStackTrace();
				 }
			 }
		 }
		 return Result.error("文件导入失败！");
	 }
	/*---------------------------------主表处理-end-------------------------------------*/


    /*--------------------------------子表处理-检测项目信息-begin----------------------------------------------*/
	/**
	 * 通过主表ID查询
	 * @return
	 */
	@AutoLog(value = "检测项目信息-通过主表ID查询")
	@ApiOperation(value="检测项目信息-通过主表ID查询", notes="检测项目信息-通过主表ID查询")
	@GetMapping(value = "/listMesCheckprojectInfoByMainId")
    public Result<?> listMesCheckprojectInfoByMainId(MesCheckprojectInfo mesCheckprojectInfo,
                                                    @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                                    @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                                    HttpServletRequest req) {
        QueryWrapper<MesCheckprojectInfo> queryWrapper = QueryGenerator.initQueryWrapper(mesCheckprojectInfo, req.getParameterMap());
        Page<MesCheckprojectInfo> page = new Page<MesCheckprojectInfo>(pageNo, pageSize);
        IPage<MesCheckprojectInfo> pageList = mesCheckprojectInfoService.page(page, queryWrapper);
        return Result.ok(pageList);
    }

	/**
	 * 添加
	 * @param mesCheckprojectInfo
	 * @return
	 */
	@AutoLog(value = "检测项目信息-添加")
	@ApiOperation(value="检测项目信息-添加", notes="检测项目信息-添加")
	@PostMapping(value = "/addMesCheckprojectInfo")
	public Result<?> addMesCheckprojectInfo(@RequestBody MesCheckprojectInfo mesCheckprojectInfo) {
		mesCheckprojectInfoService.save(mesCheckprojectInfo);
		return Result.ok("添加成功！");
	}

    /**
	 * 编辑
	 * @param mesCheckprojectInfo
	 * @return
	 */
	@AutoLog(value = "检测项目信息-编辑")
	@ApiOperation(value="检测项目信息-编辑", notes="检测项目信息-编辑")
	@PutMapping(value = "/editMesCheckprojectInfo")
	public Result<?> editMesCheckprojectInfo(@RequestBody MesCheckprojectInfo mesCheckprojectInfo) {
		mesCheckprojectInfoService.updateById(mesCheckprojectInfo);
		return Result.ok("编辑成功!");
	}

	/**
	 * 通过id删除
	 * @param id
	 * @return
	 */
	@AutoLog(value = "检测项目信息-通过id删除")
	@ApiOperation(value="检测项目信息-通过id删除", notes="检测项目信息-通过id删除")
	@DeleteMapping(value = "/deleteMesCheckprojectInfo")
	public Result<?> deleteMesCheckprojectInfo(@RequestParam(name="id",required=true) String id) {
		mesCheckprojectInfoService.removeById(id);
		return Result.ok("删除成功!");
	}

	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "检测项目信息-批量删除")
	@ApiOperation(value="检测项目信息-批量删除", notes="检测项目信息-批量删除")
	@DeleteMapping(value = "/deleteBatchMesCheckprojectInfo")
	public Result<?> deleteBatchMesCheckprojectInfo(@RequestParam(name="ids",required=true) String ids) {
	    this.mesCheckprojectInfoService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}

    /**
     * 导出
     * @return
     */
    @RequestMapping(value = "/exportMesCheckprojectInfo")
    public ModelAndView exportMesCheckprojectInfo(HttpServletRequest request, MesCheckprojectInfo mesCheckprojectInfo) {
		 // Step.1 组装查询条件
		 QueryWrapper<MesCheckprojectInfo> queryWrapper = QueryGenerator.initQueryWrapper(mesCheckprojectInfo, request.getParameterMap());
		 LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

		 // Step.2 获取导出数据
		 List<MesCheckprojectInfo> pageList = mesCheckprojectInfoService.list(queryWrapper);
		 List<MesCheckprojectInfo> exportList = null;

		 // 过滤选中数据
		 String selections = request.getParameter("selections");
		 if (oConvertUtils.isNotEmpty(selections)) {
			 List<String> selectionList = Arrays.asList(selections.split(","));
			 exportList = pageList.stream().filter(item -> selectionList.contains(item.getId())).collect(Collectors.toList());
		 } else {
			 exportList = pageList;
		 }

		 // Step.3 AutoPoi 导出Excel
		 ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
		 mv.addObject(NormalExcelConstants.FILE_NAME, "检测项目信息"); //此处设置的filename无效 ,前端会重更新设置一下
		 mv.addObject(NormalExcelConstants.CLASS, MesCheckprojectInfo.class);
		 mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("检测项目信息报表", "导出人:" + sysUser.getRealname(), "检测项目信息"));
		 mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
		 return mv;
    }

    /**
     * 导入
     * @return
     */
    @RequestMapping(value = "/importMesCheckprojectInfo/{mainId}")
    public Result<?> importMesCheckprojectInfo(HttpServletRequest request, HttpServletResponse response, @PathVariable("mainId") String mainId) {
		 MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		 Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
		 for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
			 MultipartFile file = entity.getValue();// 获取上传文件对象
			 ImportParams params = new ImportParams();
			 params.setTitleRows(2);
			 params.setHeadRows(1);
			 params.setNeedSave(true);
			 try {
				 List<MesCheckprojectInfo> list = ExcelImportUtil.importExcel(file.getInputStream(), MesCheckprojectInfo.class, params);
				 for (MesCheckprojectInfo temp : list) {
                    temp.setMcheckprojectId(mainId);
				 }
				 long start = System.currentTimeMillis();
				 mesCheckprojectInfoService.saveBatch(list);
				 log.info("消耗时间" + (System.currentTimeMillis() - start) + "毫秒");
				 return Result.ok("文件导入成功！数据行数：" + list.size());
			 } catch (Exception e) {
				 log.error(e.getMessage(), e);
				 return Result.error("文件导入失败:" + e.getMessage());
			 } finally {
				 try {
					 file.getInputStream().close();
				 } catch (IOException e) {
					 e.printStackTrace();
				 }
			 }
		 }
		 return Result.error("文件导入失败！");
    }

    /*--------------------------------子表处理-检测项目信息-end----------------------------------------------*/




}
