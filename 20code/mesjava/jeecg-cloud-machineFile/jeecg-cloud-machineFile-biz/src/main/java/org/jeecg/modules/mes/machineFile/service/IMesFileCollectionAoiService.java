package org.jeecg.modules.mes.machineFile.service;

import org.jeecg.modules.mes.produce.entity.MesFileCollectionAoi;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.servlet.http.HttpServletRequest;

/**
 * @Description: mes_file_collection_aoi
 * @Author: jeecg-boot
 * @Date:   2021-04-19
 * @Version: V1.0
 */
public interface IMesFileCollectionAoiService extends IService<MesFileCollectionAoi> {

    /**
     * 接收aoi文件
     * @param request request
     * @param line 产线
     */
    void receiveAoiFile(HttpServletRequest request, String line);
}
