package org.jeecg.modules.mes.machineFile.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.produce.entity.MesFileCollectionTpj;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: mes_file_collection_tpj
 * @Author: jeecg-boot
 * @Date:   2021-04-19
 * @Version: V1.0
 */
public interface MesFileCollectionTpjMapper extends BaseMapper<MesFileCollectionTpj> {

}
