package org.jeecg.modules.mes.machineFile.service;

import org.jeecg.modules.mes.produce.entity.MesFileCollectionSpi;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @Description: mes_file_collection_spi
 * @Author: jeecg-boot
 * @Date:   2021-04-16
 * @Version: V1.0
 */
public interface IMesFileCollectionSpiService extends IService<MesFileCollectionSpi> {

    /**
     * 接收spi文件
     * @param request
     * @param line
     */
    void receiveSpiFile(HttpServletRequest request,String line);


}
