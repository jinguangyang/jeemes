package org.jeecg.modules.mes.machineFile.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.mes.produce.entity.MesFileCollectionSpiCopy;

/**
 * @Description: spi文件采集
 * @Author: jeecg-boot
 * @Date:   2021-05-31
 * @Version: V1.0
 */
public interface IMesFileCollectionSpiCopyService extends IService<MesFileCollectionSpiCopy> {

}
