package org.jeecg.modules.mes.machineFile.service.impl;

import org.jeecg.modules.mes.machineFile.mapper.MesFileCollectionAoiDetailMapper;
import org.jeecg.modules.mes.machineFile.service.IMesFileCollectionAoiDetailService;
import org.jeecg.modules.mes.produce.entity.MesFileCollectionAoiDetail;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: mes_file_collection_aoi_detail
 * @Author: jeecg-boot
 * @Date:   2021-04-19
 * @Version: V1.0
 */
@Service
public class MesFileCollectionAoiDetailServiceImpl extends ServiceImpl<MesFileCollectionAoiDetailMapper, MesFileCollectionAoiDetail> implements IMesFileCollectionAoiDetailService {

}
