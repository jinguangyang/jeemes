package org.jeecg.modules.mes.machineFile.service;

import org.jeecg.modules.mes.produce.entity.MesFileCollectionTpj;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.servlet.http.HttpServletRequest;

/**
 * @Description: mes_file_collection_tpj
 * @Author: jeecg-boot
 * @Date:   2021-04-19
 * @Version: V1.0
 */
public interface IMesFileCollectionTpjService extends IService<MesFileCollectionTpj> {

    /**
     * 接收贴片机文件
     * @param request request
     * @param line 产线
     */
    void receiveTpjFile(HttpServletRequest request, String line);

}
