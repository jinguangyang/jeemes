package org.jeecg.modules.ems.service;

import org.jeecg.modules.ems.entity.IndustryTest;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @Description: 测试表
 * @Author: jeecg-boot
 * @Date:   2020-08-31
 * @Version: V1.0
 */
public interface IIndustryTestService extends IService<IndustryTest> {

}
