package org.jeecg.modules.mes.client;

import org.jeecg.common.constant.ServiceNameConstants;
import org.jeecg.modules.mes.order.entity.MesOrderProduce;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

@Component
@FeignClient(contextId = "WorkbillServiceClient", value = ServiceNameConstants.PRODUCE_SERVICE)
public interface WorkbillClient {

    @DeleteMapping("produce/mesWorkbillInfo/deleteByPorderId")
    public String deleteByPorderId(@RequestParam(name = "id", required = true) String id);

    @DeleteMapping("produce/mesCommandbillPitem/deleteCommandBomByorderId")
    public String deleteCommandBomByorderId(@RequestParam(name = "orderId", required = true) String orderId);


    @GetMapping("produce/mesCommandbillPitem//getCommandBomByorderId")
    public boolean getCommandBomByorderId(@RequestParam("proorderId") String proorderId);


    @PostMapping("produce/mesCommandbillInfo/produceAddInstrution")
    public String produceAddInstrution(@RequestBody MesOrderProduce mesOrderProduce);


}
