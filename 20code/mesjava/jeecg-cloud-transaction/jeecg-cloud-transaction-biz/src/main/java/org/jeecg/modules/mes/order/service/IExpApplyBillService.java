package org.jeecg.modules.mes.order.service;

import org.jeecg.modules.mes.order.entity.ExpApplyBill;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 出口申请单
 * @Author: jeecg-boot
 * @Date:   2021-04-07
 * @Version: V1.0
 */
public interface IExpApplyBillService extends IService<ExpApplyBill> {

	public List<ExpApplyBill> selectByMainId(String mainId);
}
