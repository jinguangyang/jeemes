package org.jeecg.modules.mes.storage.service;

import org.jeecg.modules.mes.storage.entity.MesCountItem;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 仓库管理—盘点子表
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
public interface IMesCountItemService extends IService<MesCountItem> {

	public List<MesCountItem> selectByMainId(String mainId);
}
