package org.jeecg.modules.mes.storage.service.impl;

import org.jeecg.modules.mes.storage.entity.MesLackMaterial;
import org.jeecg.modules.mes.storage.mapper.MesLackMaterialMapper;
import org.jeecg.modules.mes.storage.service.IMesLackMaterialService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 仓库管理-缺料管理
 * @Author: jeecg-boot
 * @Date:   2020-11-11
 * @Version: V1.0
 */
@Service
public class MesLackMaterialServiceImpl extends ServiceImpl<MesLackMaterialMapper, MesLackMaterial> implements IMesLackMaterialService {

}
