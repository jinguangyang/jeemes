package org.jeecg.modules.mes.storage.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.mes.storage.entity.MesStorageWholesale;
import org.jeecg.modules.mes.storage.entity.MesWarehouseAreaLocation;
import org.jeecg.modules.mes.storage.mapper.MesStorageWholesaleMapper;
import org.jeecg.modules.mes.storage.mapper.MesWarehouseAreaLocationMapper;
import org.jeecg.modules.mes.storage.service.IMesWarehouseAreaLocationService;
import org.springframework.stereotype.Service;
import com.epms.util.ObjectHelper;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Description: 仓库区域位置表
 * @Author: jeecg-boot
 * @Date: 2021-05-13
 * @Version: V1.0
 */
@Service
public class MesWarehouseAreaLocationServiceImpl extends ServiceImpl<MesWarehouseAreaLocationMapper, MesWarehouseAreaLocation> implements IMesWarehouseAreaLocationService {

    @Autowired
    private MesWarehouseAreaLocationMapper mesWarehouseAreaLocationMapper;
    @Autowired
    private MesStorageWholesaleMapper mesStorageWholesaleMapper;

    @Override
    public List<MesWarehouseAreaLocation> selectByMainId(String mainId) {
        return mesWarehouseAreaLocationMapper.selectByMainId(mainId);
    }

    @Override
    @Transactional
    public Result<?> addMesWarehouseAreaLocation(MesWarehouseAreaLocation mesWarehouseAreaLocation) {
        if (ObjectHelper.isEmpty(mesWarehouseAreaLocation.getLocationCode())) {
            return Result.error("位置编码不能为空！");
        }

        if (ObjectHelper.isEmpty(mesWarehouseAreaLocation.getClientName())) {
            return Result.error("客户名称不能为空！");
        }

        List<MesWarehouseAreaLocation> newEntity = mesWarehouseAreaLocationMapper.selectByLocationCode(mesWarehouseAreaLocation.getLocationCode());
        if (ObjectHelper.isNotEmpty(newEntity)) {
            return Result.error("位置编码【" + mesWarehouseAreaLocation.getLocationCode() + "】以存在，请修改为其他位置编码！");
        }

        QueryWrapper<MesWarehouseAreaLocation> queryWrapper = new QueryWrapper();
        queryWrapper.eq("client_name", mesWarehouseAreaLocation.getClientName());
        if (ObjectHelper.isNotEmpty(mesWarehouseAreaLocationMapper.selectList(queryWrapper))) {
            return Result.error("客户名称【" + mesWarehouseAreaLocation.getClientName() + "】以存在，请修改为其他客户名称！");
        }

        mesWarehouseAreaLocationMapper.insert(mesWarehouseAreaLocation);
        return Result.ok("添加成功！");
    }

    @Override
    @Transactional
    public Result<?> editMesWarehouseAreaLocation(MesWarehouseAreaLocation mesWarehouseAreaLocation) {
        if (ObjectHelper.isEmpty(mesWarehouseAreaLocation.getLocationCode())) {
            return Result.error("位置编码不能为空！");
        }

        MesWarehouseAreaLocation newMesWarehouseAreaLocation = mesWarehouseAreaLocationMapper.selectById(mesWarehouseAreaLocation.getId());

        if (!mesWarehouseAreaLocation.getLocationCode().equals(newMesWarehouseAreaLocation.getLocationCode())) {//新位置编码和原位置编码不一样需要验证是否有重复编码
            if (checkAreaLocation(newMesWarehouseAreaLocation.getLocationCode())) {
                return Result.error("该区位下有物料，不允许修改位置编码！");
            }
            List<MesWarehouseAreaLocation> newEntity = mesWarehouseAreaLocationMapper.selectByLocationCode(mesWarehouseAreaLocation.getLocationCode());
            if (null != newEntity && newEntity.size() > 0) {
                return Result.error("位置编码【" + mesWarehouseAreaLocation.getLocationCode() + "】以存在，请修改为其他位置编码！");
            }
        }

        QueryWrapper<MesWarehouseAreaLocation> queryWrapper = new QueryWrapper();
        queryWrapper.eq("client_name", mesWarehouseAreaLocation.getClientName());
        List<MesWarehouseAreaLocation> list = mesWarehouseAreaLocationMapper.selectList(queryWrapper);
        if (ObjectHelper.isNotEmpty(list) && !mesWarehouseAreaLocation.getId().equals(list.get(0).getId())) {
            return Result.error("客户名称【" + mesWarehouseAreaLocation.getClientName() + "】以存在，请修改为其他客户名称！");
        }

        mesWarehouseAreaLocationMapper.updateById(mesWarehouseAreaLocation);
        return Result.ok("编辑成功！");
    }

    @Override
    public Result<?> deleteMesWarehouseAreaLocation(String id) {
        MesWarehouseAreaLocation newMesWarehouseAreaLocation = mesWarehouseAreaLocationMapper.selectById(id);
        if (checkAreaLocation(newMesWarehouseAreaLocation.getLocationCode())) {
            return Result.error("该区位下有物料，不允允许删除！");
        }
        mesWarehouseAreaLocationMapper.deleteById(id);
        return Result.ok("删除成功!");
    }

    @Override
    public Result<?> deleteBatchMesWarehouseAreaLocation(List<String> asList) {
        mesWarehouseAreaLocationMapper.deleteBatchIds(asList);
        return Result.ok("批量删除成功!");
    }

    /**
     * 根据客户查询位置编码
     */
    @Override
    public MesWarehouseAreaLocation getAreaLocationByClient(String clientName) {
        QueryWrapper<MesWarehouseAreaLocation> queryWrapper = new QueryWrapper();
        queryWrapper.eq("client_name", clientName);
        return mesWarehouseAreaLocationMapper.selectOne(queryWrapper);
    }

    /**
     * 验证该区位下有无物料
     *
     * @param locationCode
     * @return
     */
    private boolean checkAreaLocation(String locationCode) {
        QueryWrapper<MesStorageWholesale> queryWrapper = new QueryWrapper();
        queryWrapper.eq("location_code", locationCode);
        List<MesStorageWholesale> list = mesStorageWholesaleMapper.selectList(queryWrapper);
        if (com.epms.util.ObjectHelper.isNotEmpty(list)) {
            return true;
        }
        return false;
    }
}
