package org.jeecg.modules.mes.storage.service;

import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.mes.storage.entity.MesMaterielTraceability;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 物料追溯表
 * @Author: jeecg-boot
 * @Date:   2021-06-02
 * @Version: V1.0
 */
public interface IMesMaterielTraceabilityService extends IService<MesMaterielTraceability> {

    Result<?> queryPageList(MesMaterielTraceability mesMaterielTraceability, Integer pageNo, Integer pageSize);
}
