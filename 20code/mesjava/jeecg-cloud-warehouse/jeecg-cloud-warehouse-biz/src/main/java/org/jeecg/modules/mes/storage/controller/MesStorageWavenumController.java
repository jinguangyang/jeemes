package org.jeecg.modules.mes.storage.controller;

import java.io.UnsupportedEncodingException;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.vo.LoginUser;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.mes.storage.entity.MesWavenumItem;
import org.jeecg.modules.mes.storage.entity.MesStorageWavenum;
import org.jeecg.modules.mes.storage.vo.MesStorageWavenumPage;
import org.jeecg.modules.mes.storage.service.IMesStorageWavenumService;
import org.jeecg.modules.mes.storage.service.IMesWavenumItemService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 仓库管理—波次单
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Api(tags="仓库管理—波次单")
@RestController
@RequestMapping("/storage/mesStorageWavenum")
@Slf4j
public class MesStorageWavenumController {
	@Autowired
	private IMesStorageWavenumService mesStorageWavenumService;
	@Autowired
	private IMesWavenumItemService mesWavenumItemService;
	
	/**
	 * 分页列表查询
	 *
	 * @param mesStorageWavenum
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "仓库管理—波次单-分页列表查询")
	@ApiOperation(value="仓库管理—波次单-分页列表查询", notes="仓库管理—波次单-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesStorageWavenum mesStorageWavenum,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesStorageWavenum> queryWrapper = QueryGenerator.initQueryWrapper(mesStorageWavenum, req.getParameterMap());
		Page<MesStorageWavenum> page = new Page<MesStorageWavenum>(pageNo, pageSize);
		IPage<MesStorageWavenum> pageList = mesStorageWavenumService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param mesStorageWavenumPage
	 * @return
	 */
	@AutoLog(value = "仓库管理—波次单-添加")
	@ApiOperation(value="仓库管理—波次单-添加", notes="仓库管理—波次单-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MesStorageWavenumPage mesStorageWavenumPage) {
		MesStorageWavenum mesStorageWavenum = new MesStorageWavenum();
		BeanUtils.copyProperties(mesStorageWavenumPage, mesStorageWavenum);
		mesStorageWavenumService.saveMain(mesStorageWavenum, mesStorageWavenumPage.getMesWavenumItemList());
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param mesStorageWavenumPage
	 * @return
	 */
	@AutoLog(value = "仓库管理—波次单-编辑")
	@ApiOperation(value="仓库管理—波次单-编辑", notes="仓库管理—波次单-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody MesStorageWavenumPage mesStorageWavenumPage) {
		MesStorageWavenum mesStorageWavenum = new MesStorageWavenum();
		BeanUtils.copyProperties(mesStorageWavenumPage, mesStorageWavenum);
		MesStorageWavenum mesStorageWavenumEntity = mesStorageWavenumService.getById(mesStorageWavenum.getId());
		if(mesStorageWavenumEntity==null) {
			return Result.error("未找到对应数据");
		}
		mesStorageWavenumService.updateMain(mesStorageWavenum, mesStorageWavenumPage.getMesWavenumItemList());
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "仓库管理—波次单-通过id删除")
	@ApiOperation(value="仓库管理—波次单-通过id删除", notes="仓库管理—波次单-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		mesStorageWavenumService.delMain(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "仓库管理—波次单-批量删除")
	@ApiOperation(value="仓库管理—波次单-批量删除", notes="仓库管理—波次单-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.mesStorageWavenumService.delBatchMain(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功！");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "仓库管理—波次单-通过id查询")
	@ApiOperation(value="仓库管理—波次单-通过id查询", notes="仓库管理—波次单-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		MesStorageWavenum mesStorageWavenum = mesStorageWavenumService.getById(id);
		if(mesStorageWavenum==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(mesStorageWavenum);

	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "仓库管理—波次单子表通过主表ID查询")
	@ApiOperation(value="仓库管理—波次单子表主表ID查询", notes="仓库管理—波次单子表-通主表ID查询")
	@GetMapping(value = "/queryMesWavenumItemByMainId")
	public Result<?> queryMesWavenumItemListByMainId(@RequestParam(name="id",required=true) String id) {
		List<MesWavenumItem> mesWavenumItemList = mesWavenumItemService.selectByMainId(id);
		return Result.ok(mesWavenumItemList);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param mesStorageWavenum
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesStorageWavenum mesStorageWavenum) {
      // Step.1 组装查询条件查询数据
      QueryWrapper<MesStorageWavenum> queryWrapper = QueryGenerator.initQueryWrapper(mesStorageWavenum, request.getParameterMap());
      LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

      //Step.2 获取导出数据
      List<MesStorageWavenum> queryList = mesStorageWavenumService.list(queryWrapper);
      // 过滤选中数据
      String selections = request.getParameter("selections");
      List<MesStorageWavenum> mesStorageWavenumList = new ArrayList<MesStorageWavenum>();
      if(oConvertUtils.isEmpty(selections)) {
          mesStorageWavenumList = queryList;
      }else {
          List<String> selectionList = Arrays.asList(selections.split(","));
          mesStorageWavenumList = queryList.stream().filter(item -> selectionList.contains(item.getId())).collect(Collectors.toList());
      }

      // Step.3 组装pageList
      List<MesStorageWavenumPage> pageList = new ArrayList<MesStorageWavenumPage>();
      for (MesStorageWavenum main : mesStorageWavenumList) {
          MesStorageWavenumPage vo = new MesStorageWavenumPage();
          BeanUtils.copyProperties(main, vo);
          List<MesWavenumItem> mesWavenumItemList = mesWavenumItemService.selectByMainId(main.getId());
          vo.setMesWavenumItemList(mesWavenumItemList);
          pageList.add(vo);
      }

      // Step.4 AutoPoi 导出Excel
      ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
      mv.addObject(NormalExcelConstants.FILE_NAME, "仓库管理—波次单列表");
      mv.addObject(NormalExcelConstants.CLASS, MesStorageWavenumPage.class);
      mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("仓库管理—波次单数据", "导出人:"+sysUser.getRealname(), "仓库管理—波次单"));
      mv.addObject(NormalExcelConstants.DATA_LIST, pageList);
      return mv;
    }

    /**
    * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
      MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
      Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
      for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
          MultipartFile file = entity.getValue();// 获取上传文件对象
          ImportParams params = new ImportParams();
          params.setTitleRows(2);
          params.setHeadRows(1);
          params.setNeedSave(true);
          try {
              List<MesStorageWavenumPage> list = ExcelImportUtil.importExcel(file.getInputStream(), MesStorageWavenumPage.class, params);
              for (MesStorageWavenumPage page : list) {
                  MesStorageWavenum po = new MesStorageWavenum();
                  BeanUtils.copyProperties(page, po);
                  mesStorageWavenumService.saveMain(po, page.getMesWavenumItemList());
              }
              return Result.ok("文件导入成功！数据行数:" + list.size());
          } catch (Exception e) {
              log.error(e.getMessage(),e);
              return Result.error("文件导入失败:"+e.getMessage());
          } finally {
              try {
                  file.getInputStream().close();
              } catch (IOException e) {
                  e.printStackTrace();
              }
          }
      }
      return Result.ok("文件导入失败！");
    }

}
