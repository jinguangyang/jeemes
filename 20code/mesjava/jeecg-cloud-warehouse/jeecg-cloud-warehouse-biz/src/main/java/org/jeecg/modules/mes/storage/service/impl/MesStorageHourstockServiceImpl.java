package org.jeecg.modules.mes.storage.service.impl;

import org.jeecg.modules.mes.storage.entity.MesStorageHourstock;
import org.jeecg.modules.mes.storage.mapper.MesStorageHourstockMapper;
import org.jeecg.modules.mes.storage.service.IMesStorageHourstockService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 仓库管理—时点库存
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Service
public class MesStorageHourstockServiceImpl extends ServiceImpl<MesStorageHourstockMapper, MesStorageHourstock> implements IMesStorageHourstockService {

}
