package org.jeecg.modules.mes.storage.mapper;

import java.util.List;
import org.jeecg.modules.mes.storage.entity.MesAcceptanceSite;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 仓库管理—库位子表
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
public interface MesAcceptanceSiteMapper extends BaseMapper<MesAcceptanceSite> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<MesAcceptanceSite> selectByMainId(@Param("mainId") String mainId);
}
