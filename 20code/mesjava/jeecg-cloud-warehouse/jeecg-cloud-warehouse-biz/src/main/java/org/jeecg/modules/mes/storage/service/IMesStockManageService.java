package org.jeecg.modules.mes.storage.service;

import org.jeecg.modules.mes.storage.entity.MesStockManage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 仓库管理-库存管理
 * @Author: jeecg-boot
 * @Date:   2020-11-11
 * @Version: V1.0
 */
public interface IMesStockManageService extends IService<MesStockManage> {

    /**
     * 根据物料编号查询库存信息
     */
    MesStockManage getStockManageByMaterielCode(String materielCode);

    MesStockManage queryByMcodeAndClient(String mCode, String clientName);

    /**
     * 根据物料料号和客户名称查询库存不良品
     */
    MesStockManage queryByMcodeAndClientblp(String mCode, String clientName);
}
