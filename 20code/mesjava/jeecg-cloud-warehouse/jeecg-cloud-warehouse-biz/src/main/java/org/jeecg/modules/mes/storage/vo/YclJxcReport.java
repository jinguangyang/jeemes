package org.jeecg.modules.mes.storage.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

@Data
public class YclJxcReport implements Serializable {
    @Excel(name = "内部料号", width = 15)
    @ApiModelProperty(value = "内部料号")
    private String attr1;
    @Excel(name = "客户料号", width = 15)
    @ApiModelProperty(value = "客户料号")
    private String attr2;
    @Excel(name = "产品名称", width = 15)
    @ApiModelProperty(value = "产品名称")
    private String attr3;
    @Excel(name = "规格", width = 15)
    @ApiModelProperty(value = "规格")
    private String attr4;
    @Excel(name = "伏数", width = 15)
    @ApiModelProperty(value = "伏数")
    private String attr5;
    @Excel(name = "正负值", width = 15)
    @ApiModelProperty(value = "正负值")
    private String attr6;
    @Excel(name = "期初库存", width = 15)
    @ApiModelProperty(value = "期初库存")
    private String attr7;
    @Excel(name = "本期收入", width = 15)
    @ApiModelProperty(value = "本期收入")
    private String attr8;
    @Excel(name = "本年累计收入", width = 15)
    @ApiModelProperty(value = "本年累计收入")
    private String attr9;
    @Excel(name = "本月发出", width = 15)
    @ApiModelProperty(value = "本月发出")
    private String attr10;
    @Excel(name = "本年累计发出", width = 15)
    @ApiModelProperty(value = "本年累计发出")
    private String attr11;
    @Excel(name = "库存数量", width = 15)
    @ApiModelProperty(value = "库存数量")
    private String attr12;
    @Excel(name = "物料类型", width = 15)
    @ApiModelProperty(value = "物料类型")
    private String attr13;
    @Excel(name = "区域编码", width = 15)
    @ApiModelProperty(value = "区域编码")
    private String attr14;
    @Excel(name = "位置编码", width = 15)
    @ApiModelProperty(value = "位置编码")
    private String attr15;
    //@Excel(name = "列名16", width = 15)
    @ApiModelProperty(value = "列名16")
    private String areaCode;
    //@Excel(name = "列名17", width = 15)
    @ApiModelProperty(value = "列名17")
    private String locationCode;
}
