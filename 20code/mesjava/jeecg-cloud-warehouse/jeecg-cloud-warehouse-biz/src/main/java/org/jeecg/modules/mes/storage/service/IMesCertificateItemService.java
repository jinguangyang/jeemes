package org.jeecg.modules.mes.storage.service;

import org.jeecg.modules.mes.storage.entity.MesCertificateItem;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 物料凭证项目
 * @Author: jeecg-boot
 * @Date:   2020-10-15
 * @Version: V1.0
 */
public interface IMesCertificateItemService extends IService<MesCertificateItem> {

	public List<MesCertificateItem> selectByMainId(String mainId);

	public List<MesCertificateItem> selectbqByItem(String query4);

	public boolean wholeChangeProduce(MesCertificateItem mesCertificateItem);
}
