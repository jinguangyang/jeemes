package org.jeecg.modules.mes.storage.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

@Data
public class PcbUseReport implements Serializable {

    @Excel(name = "制令单号", width = 15)
    @ApiModelProperty(value = "制令单号")
    private String baseRownum;
    @Excel(name = "产品编号", width = 15)
    @ApiModelProperty(value = "产品编号")
    private String productCode;
    @Excel(name = "产品名称", width = 15)
    @ApiModelProperty(value = "产品名称")
    private String productName;
    @Excel(name = "PCB二维码", width = 15)
    @ApiModelProperty(value = "PCB二维码")
    private String id;
    @Excel(name = "数量", width = 15)
    @ApiModelProperty(value = "数量")
    private String inwareNum;
    @Excel(name = "单位", width = 15)
    @ApiModelProperty(value = "单位")
    private String unit;
    @Excel(name = "AOI结果", width = 15)
    @ApiModelProperty(value = "AOI结果")
    private String query1;
    @Excel(name = "SPI结果", width = 15)
    @ApiModelProperty(value = "SPI结果")
    private String query2;
    @Excel(name = "AOI结果描述", width = 15)
    @ApiModelProperty(value = "AOI结果描述")
    private String query3;
    @Excel(name = "SPI结果描述", width = 15)
    @ApiModelProperty(value = "SPI结果描述")
    private String query4;
    @Excel(name = "操作人", width = 15)
    @ApiModelProperty(value = "操作人")
    private String createBy;
    @Excel(name = "操作时间", width = 15 , format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "操作时间")
    private java.util.Date createTime;
}
