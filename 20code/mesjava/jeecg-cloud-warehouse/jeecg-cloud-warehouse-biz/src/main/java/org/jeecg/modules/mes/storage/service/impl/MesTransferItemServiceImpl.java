package org.jeecg.modules.mes.storage.service.impl;

import org.jeecg.modules.mes.storage.entity.MesTransferItem;
import org.jeecg.modules.mes.storage.mapper.MesTransferItemMapper;
import org.jeecg.modules.mes.storage.service.IMesTransferItemService;
import org.springframework.stereotype.Service;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Description: 仓库管理—调拨移动子表
 * @Author: jeecg-boot
 * @Date:   2020-09-14
 * @Version: V1.0
 */
@Service
public class MesTransferItemServiceImpl extends ServiceImpl<MesTransferItemMapper, MesTransferItem> implements IMesTransferItemService {
	
	@Autowired
	private MesTransferItemMapper mesTransferItemMapper;
	
	@Override
	public List<MesTransferItem> selectByMainId(String mainId) {
		return mesTransferItemMapper.selectByMainId(mainId);
	}
}
