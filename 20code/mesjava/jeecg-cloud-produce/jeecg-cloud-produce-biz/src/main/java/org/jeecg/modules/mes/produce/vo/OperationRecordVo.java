package org.jeecg.modules.mes.produce.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class OperationRecordVo implements Serializable {
    @ApiModelProperty(value = "表名称编码")
    private String tableNameCode;

    @ApiModelProperty(value = "设备SN")
    private String equipmentSn;

    @ApiModelProperty(value = "文件编号")
    private String fileNumber;

    @ApiModelProperty(value = "作业日期")
    private String assignmentDate;

    @ApiModelProperty(value = "项目类型")
    private String projectType;

    @ApiModelProperty(value = "白班夜班/Time")
    private String dayOrNight;

    @ApiModelProperty(value = "异常现象")
    private String unusualPhenomenon;

    @ApiModelProperty(value = "改善对策")
    private String improveStrategy;

    @ApiModelProperty(value = "作业员")
    private String operator;

    @ApiModelProperty(value = "工程师")
    private String maintainer;

    @ApiModelProperty(value = "生产主管")
    private String reviewer;

    @ApiModelProperty(value = "检查项目")
    private List<MaintenanceRecordProjectVo> projectVoList;
}
