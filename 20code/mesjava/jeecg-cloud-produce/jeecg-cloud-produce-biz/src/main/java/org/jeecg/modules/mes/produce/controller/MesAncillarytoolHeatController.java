package org.jeecg.modules.mes.produce.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataMateriel;
import org.jeecg.modules.mes.chiefdata.entity.MesMaterielRedgum;
import org.jeecg.modules.mes.client.StockClient;
import org.jeecg.modules.mes.client.SystemClient;
import org.jeecg.modules.mes.produce.entity.MesAncillarytoolHeat;
import org.jeecg.modules.mes.produce.service.IMesAncillarytoolHeatService;
import org.jeecg.modules.mes.storage.entity.MesStorageWholesale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 * @Description: 制造中心-辅料回温
 * @Author: jeecg-boot
 * @Date: 2020-10-16
 * @Version: V1.0
 */
@Api(tags = "制造中心-辅料回温")
@RestController
@RequestMapping("/produce/mesAncillarytoolHeat")
@Slf4j
public class MesAncillarytoolHeatController extends JeecgController<MesAncillarytoolHeat, IMesAncillarytoolHeatService> {
    @Autowired
    private IMesAncillarytoolHeatService mesAncillarytoolHeatService;
    @Autowired
    RedisUtil redisUtil;
    @Autowired
    SystemClient systemClient;
    @Autowired
    StockClient stockClient;


    /**
     * 分页列表查询
     *
     * @param mesAncillarytoolHeat
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "制造中心-辅料回温-分页列表查询")
    @ApiOperation(value = "制造中心-辅料回温-分页列表查询", notes = "制造中心-辅料回温-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(MesAncillarytoolHeat mesAncillarytoolHeat,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   HttpServletRequest req) {
        QueryWrapper<MesAncillarytoolHeat> queryWrapper = QueryGenerator.initQueryWrapper(mesAncillarytoolHeat, req.getParameterMap());
        Page<MesAncillarytoolHeat> page = new Page<MesAncillarytoolHeat>(pageNo, pageSize);
        IPage<MesAncillarytoolHeat> pageList = mesAncillarytoolHeatService.page(page, queryWrapper);
        return Result.ok(pageList);
    }

    /**
     * 通过结束时间查询
     *
     * @return
     */
    @AutoLog(value = "制造中心-辅料回温-通过结束时间查询")
    @ApiOperation(value = "制造中心-辅料回温-通过结束时间查询", notes = "制造中心-辅料回温-通过结束时间查询")
    @GetMapping(value = "/listByFinishTime")
    public List<MesAncillarytoolHeat> listByFinishTime() {
        QueryWrapper<MesAncillarytoolHeat> queryWrapper = new QueryWrapper<>();
        queryWrapper.isNull("end_heatime");
        List<MesAncillarytoolHeat> ancillarytoolHeats = mesAncillarytoolHeatService.list(queryWrapper);
        for (MesAncillarytoolHeat heat : ancillarytoolHeats) {
            //系统当前时间
            Date date = new Date();
            if (date.after(heat.getExpectFinishtime())) {
                System.err.println("已到达预计结束时间！请注意！");
                systemClient.sendCheckMessagess(heat.getCreateBy(),heat.getAncillaryCode());
            }
        }
        return ancillarytoolHeats;
    }

    /**
     * 添加
     *
     * @param mesAncillarytoolHeat
     * @return
     */
    @AutoLog(value = "制造中心-辅料回温-添加")
    @ApiOperation(value = "制造中心-辅料回温-添加", notes = "制造中心-辅料回温-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody MesAncillarytoolHeat mesAncillarytoolHeat) {
        Date heatBegintime = mesAncillarytoolHeat.getHeatBegintime();
        if (heatBegintime == null) {
            return Result.error("请输入回温开始时间！");
        }
        String heatTime = mesAncillarytoolHeat.getHeatTime();
        if (StringUtils.isBlank(heatTime)) {
            return Result.error("请输入回温时间！");
        }
        int amount = Integer.valueOf(heatTime);
        Date finishTime = addTime(heatBegintime, amount);
        mesAncillarytoolHeat.setExpectFinishtime(finishTime);
        mesAncillarytoolHeatService.save(mesAncillarytoolHeat);

        redisUtil.set(mesAncillarytoolHeat.getId(), "我在这里", 30);
        return Result.ok("添加成功！");
    }


    public static Date addTime(Date date, int amount) {
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(calendar.MINUTE, amount);
        date = calendar.getTime();
        return date;
    }

    /**
     * 编辑
     *
     * @param mesAncillarytoolHeat
     * @return
     */
    @AutoLog(value = "制造中心-辅料回温-编辑")
    @ApiOperation(value = "制造中心-辅料回温-编辑", notes = "制造中心-辅料回温-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody MesAncillarytoolHeat mesAncillarytoolHeat) {
        //获取回温开始时间
        long heatBegintime = mesAncillarytoolHeat.getHeatBegintime().getTime();
        //获取当前时间
        long current = System.currentTimeMillis();
        //推算回温时间
        long heatTime = current - heatBegintime;
        long minute = heatTime / (60 * 1000);
        if (minute < Long.parseLong(mesAncillarytoolHeat.getHeatTime())) {
            //回温时间不够
            mesAncillarytoolHeat.setState("不达标");
        } else if (minute > Long.parseLong(mesAncillarytoolHeat.getMaxHeatime())) {
            //回温超时
            mesAncillarytoolHeat.setState("超时");
        } else {
            //回温达标
            mesAncillarytoolHeat.setState("达标");
        }
        //设置回温结束时间
        mesAncillarytoolHeat.setEndHeatime(new Date());
        mesAncillarytoolHeatService.updateById(mesAncillarytoolHeat);
        return Result.ok(mesAncillarytoolHeat.getState());
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "制造中心-辅料回温-通过id删除")
    @ApiOperation(value = "制造中心-辅料回温-通过id删除", notes = "制造中心-辅料回温-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        mesAncillarytoolHeatService.removeById(id);
        return Result.ok("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "制造中心-辅料回温-批量删除")
    @ApiOperation(value = "制造中心-辅料回温-批量删除", notes = "制造中心-辅料回温-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.mesAncillarytoolHeatService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.ok("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "制造中心-辅料回温-通过id查询")
    @ApiOperation(value = "制造中心-辅料回温-通过id查询", notes = "制造中心-辅料回温-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
        MesAncillarytoolHeat mesAncillarytoolHeat = mesAncillarytoolHeatService.getById(id);
        if (mesAncillarytoolHeat == null) {
            return Result.error("未找到对应数据");
        }
        return Result.ok(mesAncillarytoolHeat);
    }

    /**
     * 通过批号id查询辅料信息
     *
     * @param id 批号id
     * @return
     */
    @AutoLog(value = "制造中心-辅料回温-通过批号id查询")
    @ApiOperation(value = "制造中心-辅料回温-通过批号id查询", notes = "制造中心-辅料回温-通过批号id查询")
    @GetMapping(value = "/queryByBatchId")
    public Result<?> queryByBatchId(@RequestParam("id") String id) {
        //根据批号id查询批号交易的信息
        MesStorageWholesale mesStorageWholesale = stockClient.findById(id);

        //根据批号交易信息查询物料数据
        String productCode = mesStorageWholesale.getProductCode();
        MesChiefdataMateriel materiel = systemClient.queryByProductCode(productCode);
        //根据料号查询辅料信息
//        MesChiefdataAncillarytool tool = systemClient.queryBypCode(productCode);
//        if (Objects.nonNull(tool)) {
//            //搅拌上线时间
//            materiel.setStirTimelimit(tool.getStirTimelimit());
//            //搅拌时间
//            materiel.setStirTime(tool.getStirTime());
//            //回温时间
//            materiel.setHeatTime(tool.getHeatTime());
//            //回温上线时间
//            materiel.setHeatTimelimit(tool.getHeatTimelimit());
//            return Result.ok(materiel);
//        }
        //查询锡膏的信息
        MesMaterielRedgum bypCode = systemClient.getBypCode(materiel.getId());
        if (Objects.nonNull(bypCode)) {
            //搅拌上线时间
            materiel.setStirTimelimit(bypCode.getStirLimit());
            //搅拌时间
            materiel.setStirTime(bypCode.getStirTime());
            //回温时间
            materiel.setHeatTime(bypCode.getHeatTime());
            //回温上线时间
            materiel.setHeatTimelimit(bypCode.getHeatLimit());
            return Result.ok(materiel);
        }
        return Result.ok(materiel);
    }

    /**
     * 导出excel
     *
     * @param request
     * @param mesAncillarytoolHeat
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesAncillarytoolHeat mesAncillarytoolHeat) {
        return super.exportXls(request, mesAncillarytoolHeat, MesAncillarytoolHeat.class, "制造中心-辅料回温");
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesAncillarytoolHeat.class);
    }

}
