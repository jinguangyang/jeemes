package org.jeecg.modules.mes.produce.service;

import org.jeecg.modules.mes.produce.entity.MesXrayBadinfo;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: X-Ray检测-不良信息
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
public interface IMesXrayBadinfoService extends IService<MesXrayBadinfo> {

	public List<MesXrayBadinfo> selectByMainId(String mainId);
}
