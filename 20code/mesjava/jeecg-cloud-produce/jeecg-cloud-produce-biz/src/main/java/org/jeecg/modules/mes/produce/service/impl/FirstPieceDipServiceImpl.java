package org.jeecg.modules.mes.produce.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.commons.lang3.StringUtils;
import org.jeecg.common.util.DateUtils;
import org.jeecg.modules.mes.produce.entity.FirstPieceDip;
import org.jeecg.modules.mes.produce.entity.FirstPieceDipLog;
import org.jeecg.modules.mes.produce.entity.MesCommandbillInfo;
import org.jeecg.modules.mes.produce.mapper.FirstPieceDipLogMapper;
import org.jeecg.modules.mes.produce.mapper.FirstPieceDipMapper;
import org.jeecg.modules.mes.produce.mapper.MesCommandbillInfoMapper;
import org.jeecg.modules.mes.produce.service.IFirstPieceDipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Description: DIP首件确认表
 * @Author: jeecg-boot
 * @Date:   2021-03-22
 * @Version: V1.0
 */
@Service
public class FirstPieceDipServiceImpl extends ServiceImpl<FirstPieceDipMapper, FirstPieceDip> implements IFirstPieceDipService {

    @Autowired
    private FirstPieceDipMapper firstPieceDipMapper;

    @Autowired
    private FirstPieceDipLogMapper firstPieceDipLogMapper;

    @Autowired
    private MesCommandbillInfoMapper mesCommandbillInfoMapper;

    @Transactional
    public void saveMain(FirstPieceDip firstPieceDip){

        //修改制令单的首件状态 firstProject
        QueryWrapper<MesCommandbillInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("commandbill_code",firstPieceDip.getFirstProject());
        MesCommandbillInfo mesCommandbillInfo = mesCommandbillInfoMapper.selectOne(queryWrapper);
        mesCommandbillInfo.setFirstState("已完成");
        mesCommandbillInfoMapper.updateById(mesCommandbillInfo);

        firstPieceDip.setInTime(DateUtils.getDate());

        if("false".equals(firstPieceDip.getProtection())){
            firstPieceDip.setProtection("N");
        }else {
            firstPieceDip.setProtection("Y");
        }
        firstPieceDipMapper.insert(firstPieceDip);
        List<FirstPieceDipLog> firstPieceDipLogList = firstPieceDip.getFirstPieceDipLogList();
        for(FirstPieceDipLog firstPieceDipLog:firstPieceDipLogList){
            firstPieceDipLog.setFirstId(firstPieceDip.getId());
            if(StringUtils.isNotBlank(firstPieceDipLog.getContentType())){
                Integer ctype=Integer.parseInt(firstPieceDipLog.getContentType());
                if(ctype==1||ctype==3||ctype==5||ctype==7||ctype==9){
                    if("false".equals(firstPieceDipLog.getQuery1())){
                        firstPieceDipLog.setQuery1("否");
                    }else{
                        firstPieceDipLog.setQuery1("是");
                    }
                }
                if(ctype==2||ctype==4||ctype==6||ctype==8||ctype==10){
                    if("false".equals(firstPieceDipLog.getQuery1())){
                        firstPieceDipLog.setQuery1("不合格");
                    }else{
                        firstPieceDipLog.setQuery1("合格");
                    }
                }
            }
            firstPieceDipLogMapper.insert(firstPieceDipLog);
        }

    }
}
