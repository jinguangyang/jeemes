package org.jeecg.modules.mes.produce.service.impl;

import org.jeecg.modules.mes.produce.entity.MesSpitestInfo;
import org.jeecg.modules.mes.produce.mapper.MesSpitestInfoMapper;
import org.jeecg.modules.mes.produce.service.IMesSpitestInfoService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 制造中心-SPI测试信息
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Service
public class MesSpitestInfoServiceImpl extends ServiceImpl<MesSpitestInfoMapper, MesSpitestInfo> implements IMesSpitestInfoService {

}
