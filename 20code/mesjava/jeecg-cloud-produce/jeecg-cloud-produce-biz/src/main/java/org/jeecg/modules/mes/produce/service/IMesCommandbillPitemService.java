package org.jeecg.modules.mes.produce.service;

import org.jeecg.modules.mes.produce.entity.MesCommandbillPitem;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @Description: 制造中心—制令单BOM项目
 * @Author: jeecg-boot
 * @Date:   2020-12-17
 * @Version: V1.0
 */
public interface IMesCommandbillPitemService extends IService<MesCommandbillPitem> {


    /**
     * 制令单批量转产
     * @param mobileType 转产编号 268
     * @param mesCommandbillPitemList  原制令单信息
     * @param newCode  需要转的制令单code
     * @return
     */
    public boolean wholeChangeProduce(String mobileType, String newCode, List<MesCommandbillPitem> mesCommandbillPitemList);

    boolean checkProductStatusByCommandBillId(String commandBillId);
	
	public List<MesCommandbillPitem> selectByMainId(String mainId);
}
