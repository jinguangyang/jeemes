package org.jeecg.modules.mes.produce.service.impl;

import org.jeecg.modules.mes.produce.entity.MesMaketoolClean;
import org.jeecg.modules.mes.produce.mapper.MesMaketoolCleanMapper;
import org.jeecg.modules.mes.produce.service.IMesMaketoolCleanService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 制造中心-制具清洗
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Service
public class MesMaketoolCleanServiceImpl extends ServiceImpl<MesMaketoolCleanMapper, MesMaketoolClean> implements IMesMaketoolCleanService {

}
