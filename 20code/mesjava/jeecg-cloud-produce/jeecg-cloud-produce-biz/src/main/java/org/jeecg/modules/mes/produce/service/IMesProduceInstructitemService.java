package org.jeecg.modules.mes.produce.service;

import org.jeecg.modules.mes.produce.entity.MesProduceInstructitem;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;

/**
 * @Description: 生产指示单-明细信息
 * @Author: jeecg-boot
 * @Date:   2020-10-19
 * @Version: V1.0
 */
public interface IMesProduceInstructitemService extends IService<MesProduceInstructitem> {

	public List<MesProduceInstructitem> selectByMainId(String mainId);
}
