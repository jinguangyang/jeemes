package org.jeecg.modules.mes.produce.controller;

import org.jeecg.common.system.query.QueryGenerator;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.api.vo.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import java.util.Arrays;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.mes.produce.entity.MesProduceInstructitem;
import org.jeecg.modules.mes.produce.entity.MesProduceInstructinfo;
import org.jeecg.modules.mes.produce.service.IMesProduceInstructinfoService;
import org.jeecg.modules.mes.produce.service.IMesProduceInstructitemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

 /**
 * @Description: 生产指示单-基本信息
 * @Author: jeecg-boot
 * @Date:   2020-10-19
 * @Version: V1.0
 */
@Api(tags="生产指示单-基本信息")
@RestController
@RequestMapping("/produce/mesProduceInstructinfo")
@Slf4j
public class MesProduceInstructinfoController extends JeecgController<MesProduceInstructinfo, IMesProduceInstructinfoService> {

	@Autowired
	private IMesProduceInstructinfoService mesProduceInstructinfoService;

	@Autowired
	private IMesProduceInstructitemService mesProduceInstructitemService;


	/*---------------------------------主表处理-begin-------------------------------------*/

	/**
	 * 分页列表查询
	 * @param mesProduceInstructinfo
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "生产指示单-基本信息-分页列表查询")
	@ApiOperation(value="生产指示单-基本信息-分页列表查询", notes="生产指示单-基本信息-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesProduceInstructinfo mesProduceInstructinfo,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesProduceInstructinfo> queryWrapper = QueryGenerator.initQueryWrapper(mesProduceInstructinfo, req.getParameterMap());
		Page<MesProduceInstructinfo> page = new Page<MesProduceInstructinfo>(pageNo, pageSize);
		IPage<MesProduceInstructinfo> pageList = mesProduceInstructinfoService.page(page, queryWrapper);
		return Result.ok(pageList);
	}

	/**
     *   添加
     * @param mesProduceInstructinfo
     * @return
     */
    @AutoLog(value = "生产指示单-基本信息-添加")
    @ApiOperation(value="生产指示单-基本信息-添加", notes="生产指示单-基本信息-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody MesProduceInstructinfo mesProduceInstructinfo) {
        mesProduceInstructinfoService.save(mesProduceInstructinfo);
        return Result.ok("添加成功！");
    }

    /**
     *  编辑
     * @param mesProduceInstructinfo
     * @return
     */
    @AutoLog(value = "生产指示单-基本信息-编辑")
    @ApiOperation(value="生产指示单-基本信息-编辑", notes="生产指示单-基本信息-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody MesProduceInstructinfo mesProduceInstructinfo) {
        mesProduceInstructinfoService.updateById(mesProduceInstructinfo);
        return Result.ok("编辑成功!");
    }

    /**
     * 通过id删除
     * @param id
     * @return
     */
    @AutoLog(value = "生产指示单-基本信息-通过id删除")
    @ApiOperation(value="生产指示单-基本信息-通过id删除", notes="生产指示单-基本信息-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name="id",required=true) String id) {
        mesProduceInstructinfoService.delMain(id);
        return Result.ok("删除成功!");
    }

    /**
     * 批量删除
     * @param ids
     * @return
     */
    @AutoLog(value = "生产指示单-基本信息-批量删除")
    @ApiOperation(value="生产指示单-基本信息-批量删除", notes="生产指示单-基本信息-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
        this.mesProduceInstructinfoService.delBatchMain(Arrays.asList(ids.split(",")));
        return Result.ok("批量删除成功!");
    }

    /**
     * 导出
     * @return
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesProduceInstructinfo mesProduceInstructinfo) {
        return super.exportXls(request, mesProduceInstructinfo, MesProduceInstructinfo.class, "生产指示单-基本信息");
    }

    /**
     * 导入
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesProduceInstructinfo.class);
    }
	/*---------------------------------主表处理-end-------------------------------------*/
	

    /*--------------------------------子表处理-生产指示单-明细信息-begin----------------------------------------------*/
	/**
	 * 通过主表ID查询
	 * @return
	 */
	@AutoLog(value = "生产指示单-明细信息-通过主表ID查询")
	@ApiOperation(value="生产指示单-明细信息-通过主表ID查询", notes="生产指示单-明细信息-通过主表ID查询")
	@GetMapping(value = "/listMesProduceInstructitemByMainId")
    public Result<?> listMesProduceInstructitemByMainId(MesProduceInstructitem mesProduceInstructitem,
                                                    @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                                    @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                                    HttpServletRequest req) {
        QueryWrapper<MesProduceInstructitem> queryWrapper = QueryGenerator.initQueryWrapper(mesProduceInstructitem, req.getParameterMap());
        Page<MesProduceInstructitem> page = new Page<MesProduceInstructitem>(pageNo, pageSize);
        IPage<MesProduceInstructitem> pageList = mesProduceInstructitemService.page(page, queryWrapper);
        return Result.ok(pageList);
    }

	/**
	 * 添加
	 * @param mesProduceInstructitem
	 * @return
	 */
	@AutoLog(value = "生产指示单-明细信息-添加")
	@ApiOperation(value="生产指示单-明细信息-添加", notes="生产指示单-明细信息-添加")
	@PostMapping(value = "/addMesProduceInstructitem")
	public Result<?> addMesProduceInstructitem(@RequestBody MesProduceInstructitem mesProduceInstructitem) {
		mesProduceInstructitemService.save(mesProduceInstructitem);
		return Result.ok("添加成功！");
	}

    /**
	 * 编辑
	 * @param mesProduceInstructitem
	 * @return
	 */
	@AutoLog(value = "生产指示单-明细信息-编辑")
	@ApiOperation(value="生产指示单-明细信息-编辑", notes="生产指示单-明细信息-编辑")
	@PutMapping(value = "/editMesProduceInstructitem")
	public Result<?> editMesProduceInstructitem(@RequestBody MesProduceInstructitem mesProduceInstructitem) {
		mesProduceInstructitemService.updateById(mesProduceInstructitem);
		return Result.ok("编辑成功!");
	}

	/**
	 * 通过id删除
	 * @param id
	 * @return
	 */
	@AutoLog(value = "生产指示单-明细信息-通过id删除")
	@ApiOperation(value="生产指示单-明细信息-通过id删除", notes="生产指示单-明细信息-通过id删除")
	@DeleteMapping(value = "/deleteMesProduceInstructitem")
	public Result<?> deleteMesProduceInstructitem(@RequestParam(name="id",required=true) String id) {
		mesProduceInstructitemService.removeById(id);
		return Result.ok("删除成功!");
	}

	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "生产指示单-明细信息-批量删除")
	@ApiOperation(value="生产指示单-明细信息-批量删除", notes="生产指示单-明细信息-批量删除")
	@DeleteMapping(value = "/deleteBatchMesProduceInstructitem")
	public Result<?> deleteBatchMesProduceInstructitem(@RequestParam(name="ids",required=true) String ids) {
	    this.mesProduceInstructitemService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}

    /**
     * 导出
     * @return
     */
    @RequestMapping(value = "/exportMesProduceInstructitem")
    public ModelAndView exportMesProduceInstructitem(HttpServletRequest request, MesProduceInstructitem mesProduceInstructitem) {
		 // Step.1 组装查询条件
		 QueryWrapper<MesProduceInstructitem> queryWrapper = QueryGenerator.initQueryWrapper(mesProduceInstructitem, request.getParameterMap());
		 LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

		 // Step.2 获取导出数据
		 List<MesProduceInstructitem> pageList = mesProduceInstructitemService.list(queryWrapper);
		 List<MesProduceInstructitem> exportList = null;

		 // 过滤选中数据
		 String selections = request.getParameter("selections");
		 if (oConvertUtils.isNotEmpty(selections)) {
			 List<String> selectionList = Arrays.asList(selections.split(","));
			 exportList = pageList.stream().filter(item -> selectionList.contains(item.getId())).collect(Collectors.toList());
		 } else {
			 exportList = pageList;
		 }

		 // Step.3 AutoPoi 导出Excel
		 ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
		 mv.addObject(NormalExcelConstants.FILE_NAME, "生产指示单-明细信息"); //此处设置的filename无效 ,前端会重更新设置一下
		 mv.addObject(NormalExcelConstants.CLASS, MesProduceInstructitem.class);
		 mv.addObject(NormalExcelConstants.PARAMS, new ExportParams("生产指示单-明细信息报表", "导出人:" + sysUser.getRealname(), "生产指示单-明细信息"));
		 mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
		 return mv;
    }

    /**
     * 导入
     * @return
     */
    @RequestMapping(value = "/importMesProduceInstructitem/{mainId}")
    public Result<?> importMesProduceInstructitem(HttpServletRequest request, HttpServletResponse response, @PathVariable("mainId") String mainId) {
		 MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		 Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
		 for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
			 MultipartFile file = entity.getValue();// 获取上传文件对象
			 ImportParams params = new ImportParams();
			 params.setTitleRows(2);
			 params.setHeadRows(1);
			 params.setNeedSave(true);
			 try {
				 List<MesProduceInstructitem> list = ExcelImportUtil.importExcel(file.getInputStream(), MesProduceInstructitem.class, params);
				 for (MesProduceInstructitem temp : list) {
                    temp.setProduceId(mainId);
				 }
				 long start = System.currentTimeMillis();
				 mesProduceInstructitemService.saveBatch(list);
				 log.info("消耗时间" + (System.currentTimeMillis() - start) + "毫秒");
				 return Result.ok("文件导入成功！数据行数：" + list.size());
			 } catch (Exception e) {
				 log.error(e.getMessage(), e);
				 return Result.error("文件导入失败:" + e.getMessage());
			 } finally {
				 try {
					 file.getInputStream().close();
				 } catch (IOException e) {
					 e.printStackTrace();
				 }
			 }
		 }
		 return Result.error("文件导入失败！");
    }

    /*--------------------------------子表处理-生产指示单-明细信息-end----------------------------------------------*/




}
