package org.jeecg.modules.mes.produce.service;

import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.mes.produce.entity.MesProductionCapacityReport;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: NEW生产产能报表
 * @Author: jeecg-boot
 * @Date:   2021-05-25
 * @Version: V1.0
 */
public interface IMesProductionCapacityReportService extends IService<MesProductionCapacityReport> {

    Result<?> mesProductionCapacityReport(MesProductionCapacityReport mesProductionCapacityReport, Integer pageNo, Integer pageSize);

}
