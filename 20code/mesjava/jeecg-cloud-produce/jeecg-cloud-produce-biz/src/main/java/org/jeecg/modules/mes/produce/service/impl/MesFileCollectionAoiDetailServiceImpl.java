package org.jeecg.modules.mes.produce.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.modules.mes.produce.entity.MesFileCollectionAoiDetail;
import org.jeecg.modules.mes.produce.mapper.MesFileCollectionAoiDetailMapper;
import org.jeecg.modules.mes.produce.service.IMesFileCollectionAoiDetailService;
import org.springframework.stereotype.Service;

/**
 * @Description: mes_file_collection_aoi_detail
 * @Author: jeecg-boot
 * @Date:   2021-04-19
 * @Version: V1.0
 */
@Service
public class MesFileCollectionAoiDetailServiceImpl extends ServiceImpl<MesFileCollectionAoiDetailMapper, MesFileCollectionAoiDetail> implements IMesFileCollectionAoiDetailService {

}
