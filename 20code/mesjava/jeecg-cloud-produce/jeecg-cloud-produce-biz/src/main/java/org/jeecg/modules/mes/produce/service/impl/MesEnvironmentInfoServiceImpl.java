package org.jeecg.modules.mes.produce.service.impl;

import org.jeecg.modules.mes.produce.entity.MesEnvironmentInfo;
import org.jeecg.modules.mes.produce.mapper.MesEnvironmentInfoMapper;
import org.jeecg.modules.mes.produce.service.IMesEnvironmentInfoService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 制造中心-环境信息
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Service
public class MesEnvironmentInfoServiceImpl extends ServiceImpl<MesEnvironmentInfoMapper, MesEnvironmentInfo> implements IMesEnvironmentInfoService {

}
