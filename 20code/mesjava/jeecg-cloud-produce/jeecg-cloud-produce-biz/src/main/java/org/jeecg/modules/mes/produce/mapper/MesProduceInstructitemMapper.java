package org.jeecg.modules.mes.produce.mapper;

import java.util.List;
import org.jeecg.modules.mes.produce.entity.MesProduceInstructitem;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 生产指示单-明细信息
 * @Author: jeecg-boot
 * @Date:   2020-10-19
 * @Version: V1.0
 */
public interface MesProduceInstructitemMapper extends BaseMapper<MesProduceInstructitem> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<MesProduceInstructitem> selectByMainId(@Param("mainId") String mainId);

}
