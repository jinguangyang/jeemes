package org.jeecg.modules.mes.produce.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.modules.mes.produce.entity.MesFileCollectionTpjProduce;
import org.jeecg.modules.mes.produce.mapper.MesFileCollectionTpjProduceMapper;
import org.jeecg.modules.mes.produce.service.IMesFileCollectionTpjProduceService;
import org.springframework.stereotype.Service;

/**
 * @Description: mes_file_collection_tpj_produce
 * @Author: jeecg-boot
 * @Date:   2021-04-19
 * @Version: V1.0
 */
@Service
public class MesFileCollectionTpjProduceServiceImpl extends ServiceImpl<MesFileCollectionTpjProduceMapper, MesFileCollectionTpjProduce> implements IMesFileCollectionTpjProduceService {

}
