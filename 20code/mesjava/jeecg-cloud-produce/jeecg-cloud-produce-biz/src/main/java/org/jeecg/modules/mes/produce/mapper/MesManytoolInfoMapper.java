package org.jeecg.modules.mes.produce.mapper;

import java.util.List;
import org.jeecg.modules.mes.produce.entity.MesManytoolInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * @Description: 多个制具维保-制具信息
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
public interface MesManytoolInfoMapper extends BaseMapper<MesManytoolInfo> {

	public boolean deleteByMainId(@Param("mainId") String mainId);
    
	public List<MesManytoolInfo> selectByMainId(@Param("mainId") String mainId);
}
