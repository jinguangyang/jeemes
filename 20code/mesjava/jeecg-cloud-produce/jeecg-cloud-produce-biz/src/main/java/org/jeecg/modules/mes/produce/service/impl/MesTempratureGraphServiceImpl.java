package org.jeecg.modules.mes.produce.service.impl;

import org.jeecg.modules.mes.produce.entity.MesTempratureGraph;
import org.jeecg.modules.mes.produce.mapper.MesTempratureGraphMapper;
import org.jeecg.modules.mes.produce.service.IMesTempratureGraphService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 制造中心-温度曲线
 * @Author: jeecg-boot
 * @Date:   2020-10-16
 * @Version: V1.0
 */
@Service
public class MesTempratureGraphServiceImpl extends ServiceImpl<MesTempratureGraphMapper, MesTempratureGraph> implements IMesTempratureGraphService {

}
