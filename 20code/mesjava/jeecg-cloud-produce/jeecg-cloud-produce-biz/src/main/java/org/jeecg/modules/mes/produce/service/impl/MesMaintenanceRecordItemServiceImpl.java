package org.jeecg.modules.mes.produce.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.google.api.client.util.Lists;
import com.google.api.client.util.Sets;
import com.google.common.collect.Maps;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.mes.chiefdata.entity.MesChiefdataDevicearchive;
import org.jeecg.modules.mes.client.SystemClient;
import org.jeecg.modules.mes.produce.entity.MesMaintenanceRecordItem;
import org.jeecg.modules.mes.produce.entity.MesMaintenanceRecordMain;
import org.jeecg.modules.mes.produce.mapper.MesMaintenanceRecordItemMapper;
import org.jeecg.modules.mes.produce.mapper.MesMaintenanceRecordMainMapper;
import org.jeecg.modules.mes.produce.service.IMesMaintenanceRecordItemService;
import org.jeecg.modules.mes.produce.vo.MaintenanceRecordProjectVo;
import org.jeecg.modules.mes.produce.vo.MaintenanceRecordVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.epms.util.ObjectHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @Description: 维护保养点检记录子表
 * @Author: jeecg-boot
 * @Date: 2021-06-11
 * @Version: V1.0
 */
@Service
public class MesMaintenanceRecordItemServiceImpl extends ServiceImpl<MesMaintenanceRecordItemMapper, MesMaintenanceRecordItem> implements IMesMaintenanceRecordItemService {

    @Autowired
    private MesMaintenanceRecordItemMapper mesMaintenanceRecordItemMapper;

    @Autowired
    private MesMaintenanceRecordMainMapper mesMaintenanceRecordMainMapper;

    @Autowired
    private SystemClient systemClient;

    @Override
    public Result<?> addMesMaintenanceRecordItem(MesMaintenanceRecordItem mesMaintenanceRecordItem) {
        if (ObjectHelper.isEmpty(mesMaintenanceRecordItem.getMainId())) {
            return Result.error("未获取到主表ID，添加失败！");
        }
        MesMaintenanceRecordMain mesMaintenanceRecordMain = mesMaintenanceRecordMainMapper.selectById(mesMaintenanceRecordItem.getMainId());
        if (ObjectHelper.isEmpty(mesMaintenanceRecordMain)) {
            return Result.error("未获取到主数据，添加失败！");
        }
        QueryWrapper<MesMaintenanceRecordItem> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("record_type", mesMaintenanceRecordItem.getRecordType());
        queryWrapper.eq("type_value", mesMaintenanceRecordItem.getTypeValue());
        queryWrapper.eq("inspection_date", mesMaintenanceRecordMain.getInspectionDate());
        if (mesMaintenanceRecordItemMapper.selectCount(queryWrapper) > 0) {
            return Result.error("检查日期:" + new SimpleDateFormat("yyyy-MM").format(mesMaintenanceRecordMain.getInspectionDate()) + " 以存在记录类型=" + mesMaintenanceRecordItem.getRecordType() + " 类型值=" + mesMaintenanceRecordItem.getTypeValue() + "的数据，请修改后添加！");
        }
        this.setMesMaintenanceRecordItem(mesMaintenanceRecordItem, mesMaintenanceRecordMain);
        //子表添加逻辑
        mesMaintenanceRecordItemMapper.insert(mesMaintenanceRecordItem);
        return Result.ok("添加成功！");
    }

    /**
     * 编辑时只可编辑结果，不可编辑其他内容
     *
     * @param mesMaintenanceRecordItem
     * @return
     */
    @Override
    public Result<?> editMesMaintenanceRecordItem(MesMaintenanceRecordItem mesMaintenanceRecordItem) {
        MesMaintenanceRecordItem mesMaintenanceRecordItem1 = mesMaintenanceRecordItemMapper.selectById(mesMaintenanceRecordItem.getId());
        mesMaintenanceRecordItem1.setInspectionResults(mesMaintenanceRecordItem.getInspectionResults());//检查结果
        mesMaintenanceRecordItem1.setMaintainer(mesMaintenanceRecordItem.getMaintainer());//保养人
        mesMaintenanceRecordItem1.setReviewer(mesMaintenanceRecordItem.getReviewer());//审核人
        mesMaintenanceRecordItemMapper.updateById(mesMaintenanceRecordItem1);
        return Result.ok("编辑成功！");
    }

    @Override
    public Result<?> addMaintenanceRecordItemForApp(MaintenanceRecordVo maintenanceRecordVo) throws ParseException {
        if (ObjectHelper.isEmpty(maintenanceRecordVo.getProjectVoList())) {
            Result.error("获取检查项目失败，请检查传入参数都否正确！");
        }
        if (ObjectHelper.isEmpty(maintenanceRecordVo.getTableNameCode())) {
            Result.error("获取表名称编码失败，请检查传入参数都否正确！");
        }
        if (ObjectHelper.isEmpty(maintenanceRecordVo.getRecordType())) {
            Result.error("获取记录类型失败，请检查传入参数都否正确！");
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
        Date date = new Date();
        String inspectionDate = sdf.format(date) + "-01 00:00:00";
        MesMaintenanceRecordMain mesMaintenanceRecordMain = this.checkMaintenanceRecordMain(maintenanceRecordVo.getTableNameCode(), inspectionDate);

        if (ObjectHelper.isEmpty(mesMaintenanceRecordMain)) {//没有主表数据进行添加操作
            mesMaintenanceRecordMain = this.addMaintenanceRecordMain(maintenanceRecordVo, inspectionDate);
        }

        if (ObjectHelper.isEmpty(mesMaintenanceRecordMain)) {
            Result.error("添加主表信息时未找到对应的设备，请检查设备SN是否正确！");
        }

        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
        String typeValue = sdf1.format(date).split("-")[2];//日
        if ("week_maintain".equals(maintenanceRecordVo.getRecordType())) {//周
            typeValue = String.valueOf(Calendar.getInstance().get(Calendar.WEEK_OF_MONTH));// 当前日期是本月第几周(周日-周六  0-6)
        }
        if ("month_maintain".equals(maintenanceRecordVo.getRecordType())) {
            typeValue = sdf1.format(date).split("-")[1];//月
        }
        //验证是否有重复记录
        QueryWrapper<MesMaintenanceRecordItem> queryWrapper = new QueryWrapper();
        queryWrapper.eq("inspection_date", inspectionDate);
        queryWrapper.eq("record_type", maintenanceRecordVo.getRecordType());
        queryWrapper.eq("table_name_code", maintenanceRecordVo.getTableNameCode());
        queryWrapper.eq("type_value", typeValue);

        List<MesMaintenanceRecordItem> MesMaintenanceRecordItems = mesMaintenanceRecordItemMapper.selectList(queryWrapper);
        if (ObjectHelper.isNotEmpty(MesMaintenanceRecordItems)) {//存在记录
            List<String> ids = Lists.newArrayList();
            for (MesMaintenanceRecordItem item : MesMaintenanceRecordItems) {
                ids.add(item.getId());
            }
            mesMaintenanceRecordItemMapper.deleteBatchIds(ids);//执行删除
        }

        for (MaintenanceRecordProjectVo vo : maintenanceRecordVo.getProjectVoList()) {
            MesMaintenanceRecordItem mesMaintenanceRecordItem = new MesMaintenanceRecordItem();
            this.setMesMaintenanceRecordItem(mesMaintenanceRecordItem, mesMaintenanceRecordMain);
            mesMaintenanceRecordItem.setRecordType(maintenanceRecordVo.getRecordType());
            mesMaintenanceRecordItem.setTypeValue(typeValue);//类型值
            mesMaintenanceRecordItem.setInspectionItems(vo.getInspectionItems());//检查项目
            mesMaintenanceRecordItem.setMeasureStandard(vo.getMeasureStandard());//衡量标准
            mesMaintenanceRecordItem.setInspectionResults(vo.getInspectionResults());//检查结果
            mesMaintenanceRecordItem.setMaintainer(maintenanceRecordVo.getMaintainer());//保养人
            mesMaintenanceRecordItem.setReviewer(maintenanceRecordVo.getReviewer());//审核人
            mesMaintenanceRecordItemMapper.insert(mesMaintenanceRecordItem);
        }

        return Result.ok("添加成功！");
    }

    @Override
    public Result<?> queryList(String tableNameCode, String inspectionDate) {
        QueryWrapper<MesMaintenanceRecordMain> queryWrapper = new QueryWrapper();
        queryWrapper.eq("table_name_code", tableNameCode);
        queryWrapper.eq("inspection_date", inspectionDate + "-01 00:00:00");
        List<MesMaintenanceRecordMain> list = mesMaintenanceRecordMainMapper.selectList(queryWrapper);
        if (ObjectHelper.isEmpty(list)) {
            return Result.error("获取主表信息失败，请检查！");
        }
        MesMaintenanceRecordMain mesMaintenanceRecordMain = list.get(0);
        this.getMesMaintenanceRecordItem(mesMaintenanceRecordMain);
        return Result.ok(mesMaintenanceRecordMain);
    }

    @Override
    public Result<?> queryListByMainId(String mainId) {
        MesMaintenanceRecordMain mesMaintenanceRecordMain = mesMaintenanceRecordMainMapper.selectById(mainId);
        if (ObjectHelper.isEmpty(mesMaintenanceRecordMain)) {
            return Result.error("获取主表信息失败，请检查！");
        }
        this.getMaintenanceRecordItem(mesMaintenanceRecordMain);
        return Result.ok(mesMaintenanceRecordMain);
    }

    private void getMaintenanceRecordItem(MesMaintenanceRecordMain mesMaintenanceRecordMain) {
        QueryWrapper<MesMaintenanceRecordItem> queryWrapper = new QueryWrapper();
        queryWrapper.eq("main_id", mesMaintenanceRecordMain.getId()).orderByAsc("create_time");
        List<MesMaintenanceRecordItem> MesMaintenanceRecordItems = mesMaintenanceRecordItemMapper.selectList(queryWrapper);
        if ("printing_operation_record".equals(mesMaintenanceRecordMain.getTableNameCode())) {//印刷机作业记录表
            this.getPrintingRecordItem(mesMaintenanceRecordMain, MesMaintenanceRecordItems);
        }
        if ("reflow_operation_record".equals(mesMaintenanceRecordMain.getTableNameCode())) {//回焊炉作业记录表
            this.getReflowRecordItem(mesMaintenanceRecordMain, MesMaintenanceRecordItems);
        }
    }

    /**
     * 组装回焊炉作业记录表
     *
     * @param mesMaintenanceRecordMain
     * @param mesMaintenanceRecordItems
     */
    private void getReflowRecordItem(MesMaintenanceRecordMain mesMaintenanceRecordMain, List<MesMaintenanceRecordItem> mesMaintenanceRecordItems) {
        Set<String> day_set = Sets.newHashSet();
        Set<String> week_set = Sets.newHashSet();
        Set<String> inspectionMethod = Sets.newHashSet();//异常现象
        Set<String> inspectionCycle = Sets.newHashSet();//改善对策
        for (MesMaintenanceRecordItem item : mesMaintenanceRecordItems) {
            if ("开线前必须确认项目".equals(item.getRecordType())) {
                day_set.add(item.getInspectionItems().trim());//日项目
            }
            if ("生产过程必须确认项目".equals(item.getRecordType())) {
                week_set.add(item.getInspectionItems().trim());//周项目
            }
            if (ObjectHelper.isNotEmpty(item.getInspectionMethod())) {
                inspectionMethod.add(item.getInspectionMethod());
            }
            if (ObjectHelper.isNotEmpty(item.getInspectionCycle())) {
                inspectionCycle.add(item.getInspectionCycle());
            }
        }
        if (ObjectHelper.isNotEmpty(mesMaintenanceRecordMain.getInspectionMethod())) {
            inspectionMethod.add(mesMaintenanceRecordMain.getInspectionMethod());
        }
        if (ObjectHelper.isNotEmpty(mesMaintenanceRecordMain.getInspectionCycle())) {
            inspectionCycle.add(mesMaintenanceRecordMain.getInspectionCycle());
        }
        Map<String, List<MesMaintenanceRecordItem>> day_map = Maps.newHashMap();
        String[] dayArray = {"白班", "夜班"};
        for (String str : day_set) {
            List<MesMaintenanceRecordItem> list = new LinkedList<>();
            for (MesMaintenanceRecordItem item : mesMaintenanceRecordItems) {
                if ("开线前必须确认项目".equals(item.getRecordType()) && str.equals(item.getInspectionItems().trim())) {
                    list.add(item);//日项目
                }
            }
            day_map.put(str, this.addItem(dayArray, list));
        }
        Map<String, List<MesMaintenanceRecordItem>> week_map = Maps.newHashMap();
        for (String str : week_set) {//项目
            List<MesMaintenanceRecordItem> list = new LinkedList<>();
            for (MesMaintenanceRecordItem item : mesMaintenanceRecordItems) {
                if ("生产过程必须确认项目".equals(item.getRecordType()) && str.equals(item.getInspectionItems().trim())) {
                    list.add(item);//周项目
                }
            }
            week_map.put(str, this.addZoneItem(dayArray, list));
        }
        mesMaintenanceRecordMain.setInspectionMethod(ObjectHelper.isEmpty(inspectionMethod) ? "" : inspectionMethod.toString());
        mesMaintenanceRecordMain.setInspectionCycle(ObjectHelper.isEmpty(inspectionCycle) ? "" : inspectionCycle.toString());
        mesMaintenanceRecordMain.setDayList(day_map);
        mesMaintenanceRecordMain.setWeekList(week_map);
    }

    /**
     * 组装印刷机作业记录表
     *
     * @param mesMaintenanceRecordMain
     * @param mesMaintenanceRecordItems
     */
    private void getPrintingRecordItem(MesMaintenanceRecordMain mesMaintenanceRecordMain, List<MesMaintenanceRecordItem> mesMaintenanceRecordItems) {
        Set<String> day_set = Sets.newHashSet();
        Set<String> week_set = Sets.newHashSet();
        Set<String> month_set = Sets.newHashSet();
        for (MesMaintenanceRecordItem item : mesMaintenanceRecordItems) {
            if ("开线前必须确认项目".equals(item.getRecordType())) {
                day_set.add(item.getInspectionItems().trim());//日项目
            }
            if ("手动清洁钢板项目".equals(item.getRecordType())) {
                week_set.add(item.getInspectionItems().trim());//周项目
            }
            if ("钢板清洗后必须确认项目".equals(item.getRecordType())) {
                month_set.add(item.getInspectionItems().trim());//月项目
            }
        }
        Map<String, List<MesMaintenanceRecordItem>> day_map = Maps.newHashMap();
        String[] dayArray = {"白班", "夜班"};
        for (String str : day_set) {
            List<MesMaintenanceRecordItem> list = new LinkedList<>();
            for (MesMaintenanceRecordItem item : mesMaintenanceRecordItems) {
                if ("开线前必须确认项目".equals(item.getRecordType()) && str.equals(item.getInspectionItems().trim())) {
                    list.add(item);//日项目
                }
            }
            day_map.put(str, this.addItem(dayArray, list));
        }
        Map<String, List<MesMaintenanceRecordItem>> week_map = Maps.newHashMap();
        String[] weekArray = {"8:00", "10:00", "12:00", "14:00", "16:00", "18:00", "20:00", "22:00", "0:00", "2:00", "4:00", "6:00"};
        for (String str : week_set) {
            List<MesMaintenanceRecordItem> list = new LinkedList<>();
            for (MesMaintenanceRecordItem item : mesMaintenanceRecordItems) {
                if ("手动清洁钢板项目".equals(item.getRecordType()) && str.equals(item.getInspectionItems().trim())) {
                    list.add(item);//周项目
                }
            }
            week_map.put(str, this.addItem(weekArray, list));
        }
        Map<String, List<MesMaintenanceRecordItem>> month_map = Maps.newHashMap();
        for (String str : month_set) {
            List<MesMaintenanceRecordItem> list = Lists.newArrayList();
            for (MesMaintenanceRecordItem item : mesMaintenanceRecordItems) {
                if ("钢板清洗后必须确认项目".equals(item.getRecordType()) && str.equals(item.getInspectionItems().trim())) {
                    list.add(item);//月项目
                }
            }
            month_map.put(str, this.addItem(dayArray, list));
        }
        mesMaintenanceRecordMain.setDayList(day_map);
        mesMaintenanceRecordMain.setWeekList(week_map);
        mesMaintenanceRecordMain.setMonthList(month_map);
    }

    /**
     * 组装返回数据
     *
     * @param mesMaintenanceRecordMain
     */
    private void getMesMaintenanceRecordItem(MesMaintenanceRecordMain mesMaintenanceRecordMain) {
        QueryWrapper<MesMaintenanceRecordItem> queryWrapper = new QueryWrapper();
        queryWrapper.eq("main_id", mesMaintenanceRecordMain.getId()).orderByAsc("create_time");
        List<MesMaintenanceRecordItem> mesMaintenanceRecordItems = mesMaintenanceRecordItemMapper.selectList(queryWrapper);
        Set<String> day_set = Sets.newHashSet();
        Set<String> week_set = Sets.newHashSet();
        Set<String> month_set = Sets.newHashSet();
        for (MesMaintenanceRecordItem item : mesMaintenanceRecordItems) {
            if ("day_maintain".equals(item.getRecordType())) {
                day_set.add(item.getInspectionItems().trim());//日项目
            }
            if ("week_maintain".equals(item.getRecordType())) {
                week_set.add(item.getInspectionItems().trim());//周项目
            }
            if ("month_maintain".equals(item.getRecordType())) {
                month_set.add(item.getInspectionItems().trim());//月项目
            }
        }
        Map<String, List<MesMaintenanceRecordItem>> day_map = Maps.newHashMap();
        String[] dayArray = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"};
        for (String str : day_set) {
            List<MesMaintenanceRecordItem> list = new LinkedList<>();
            for (MesMaintenanceRecordItem item : mesMaintenanceRecordItems) {
                if ("day_maintain".equals(item.getRecordType()) && str.equals(item.getInspectionItems().trim())) {
                    list.add(item);//日项目
                }
            }
            day_map.put(str, this.addItem(dayArray, list));
        }
        Map<String, List<MesMaintenanceRecordItem>> week_map = Maps.newHashMap();
        String[] weekArray = {"1", "2", "3", "4"};
        for (String str : week_set) {
            List<MesMaintenanceRecordItem> list = new LinkedList<>();
            for (MesMaintenanceRecordItem item : mesMaintenanceRecordItems) {
                if ("week_maintain".equals(item.getRecordType()) && str.equals(item.getInspectionItems().trim())) {
                    list.add(item);//周项目
                }
            }
            week_map.put(str, this.addItem(weekArray, list));
        }
        Map<String, List<MesMaintenanceRecordItem>> month_map = Maps.newHashMap();
        for (String str : month_set) {
            List<MesMaintenanceRecordItem> list = Lists.newArrayList();
            for (MesMaintenanceRecordItem item : mesMaintenanceRecordItems) {
                if ("month_maintain".equals(item.getRecordType()) && str.equals(item.getInspectionItems().trim())) {
                    list.add(item);//月项目
                }
            }
            month_map.put(str, list);
        }
        mesMaintenanceRecordMain.setDayList(day_map);
        mesMaintenanceRecordMain.setWeekList(week_map);
        mesMaintenanceRecordMain.setMonthList(month_map);
    }

    /**
     * 添加item
     */
    private List<MesMaintenanceRecordItem> addItem(String[] array, List<MesMaintenanceRecordItem> list) {
        List<MesMaintenanceRecordItem> week_list = new LinkedList<>();
        for (String s : array) {
            boolean flag = true;
            for (MesMaintenanceRecordItem item : list) {
                if (s.equals(item.getTypeValue())) {
                    week_list.add(item);
                    flag = false;
                    break;
                }
            }
            if (flag) {
                week_list.add(initItem(s));
            }
        }
        return week_list;
    }

    /**
     * 添加ZoneItem
     */
    private List<MesMaintenanceRecordItem> addZoneItem(String[] array, List<MesMaintenanceRecordItem> list) {
        String[] zoneArray = {"Zone1", "Zone2", "Zone3", "Zone4", "Zone5", "Zone6", "Zone7", "Zone8", "Zone9", "Zone10"};
        List<MesMaintenanceRecordItem> zone_list = new LinkedList<>();
        for (String s : zoneArray) {
            for (String s1 : array) {//白班、夜班
                boolean flag = true;
                for (MesMaintenanceRecordItem item : list) {
                    if (s.equals(item.getAccountingMethod()) && s1.equals(item.getTypeValue())) {
                        zone_list.add(item);
                        flag = false;
                        break;
                    }
                }
                if (flag) {
                    zone_list.add(initItem(s1, s));
                }
            }
        }
        return zone_list;
    }


    /**
     * 初始化item
     *
     * @param typeValue
     * @return
     */
    private MesMaintenanceRecordItem initItem(String typeValue) {
        MesMaintenanceRecordItem item = new MesMaintenanceRecordItem();
        item.setTypeValue(typeValue);
        return item;
    }

    /**
     * 初始化item
     *
     * @param typeValue
     * @return
     */
    private MesMaintenanceRecordItem initItem(String typeValue, String zone) {
        MesMaintenanceRecordItem item = new MesMaintenanceRecordItem();
        item.setTypeValue(typeValue);
        item.setAccountingMethod(zone);
        return item;
    }

    /**
     * 创建主表信息
     *
     * @param maintenanceRecordVo
     * @param inspectionDate
     * @return
     */
    private MesMaintenanceRecordMain addMaintenanceRecordMain(MaintenanceRecordVo maintenanceRecordVo, String inspectionDate) throws ParseException {
        MesMaintenanceRecordMain mesMaintenanceRecordMain = new MesMaintenanceRecordMain();
        mesMaintenanceRecordMain.setId(null);
        mesMaintenanceRecordMain.setTableNameCode(maintenanceRecordVo.getTableNameCode());//表名称编码
        mesMaintenanceRecordMain.setInspectionDate(new SimpleDateFormat("yyyy-MM").parse(inspectionDate));//检查日期
        mesMaintenanceRecordMain.setMaintainer(maintenanceRecordVo.getMaintainer());//保养人
        mesMaintenanceRecordMain.setReviewer(maintenanceRecordVo.getReviewer());//稽查人
        if ("safety_before_work_check".equals(maintenanceRecordVo.getTableNameCode())) {//下班前安全点检记录表
            mesMaintenanceRecordMain.setEquipmentName("下班安全检查");//设备名称/检查项目
            mesMaintenanceRecordMain.setEquipmentModel("门窗、水、电、气路");//设备型号/检查位置
            mesMaintenanceRecordMain.setDatePurchase(mesMaintenanceRecordMain.getDatePurchase());//购置日期/启动检查时间
            mesMaintenanceRecordMain.setInspectionMethod("检查者目测查看");//检查方法
            mesMaintenanceRecordMain.setInspectionCycle("每天");//检查周期
            mesMaintenanceRecordMain.setBeCareful("1、每日开机前必须实际正确操作点检  2、操作员必须按标准作业指导书作业");//注意事项
            mesMaintenanceRecordMain.setAccountingMethod("1、正常 √   2、异常 X    3、无此项 -");//计入方式
            mesMaintenanceRecordMain.setTableTypeCode("spot_inspection_record");//表类型编码
        } else {//其他点检记录表
            MesChiefdataDevicearchive mesChiefdataDevicearchive = systemClient.queryByDeviceSn(maintenanceRecordVo.getEquipmentSn());
            if (ObjectHelper.isEmpty(mesChiefdataDevicearchive)) {
                return null;
            }
            mesMaintenanceRecordMain.setLineType(mesChiefdataDevicearchive.getQuery2());//线别
            mesMaintenanceRecordMain.setEquipmentName(mesChiefdataDevicearchive.getDeviceName());//设备名称/检查项目
            mesMaintenanceRecordMain.setEquipmentModel(mesChiefdataDevicearchive.getDeviceModel());//设备型号/检查位置
            mesMaintenanceRecordMain.setDatePurchase(mesChiefdataDevicearchive.getPurchaseDate());//购置日期/启动检查时间
            //mesMaintenanceRecordMain.setDocumentNumber(mesChiefdataDevicearchive.getQuery3());//文件编号/资产编码
            mesMaintenanceRecordMain.setEquipmentSn(maintenanceRecordVo.getEquipmentSn());//设备SN
            this.setMesMaintenanceRecordMain(mesMaintenanceRecordMain, maintenanceRecordVo.getTableNameCode());
        }
        mesMaintenanceRecordMainMapper.insert(mesMaintenanceRecordMain);
        return mesMaintenanceRecordMain;
    }

    /**
     * 设置常规数据
     *
     * @param mesMaintenanceRecordMain
     * @param tableNameCode
     */
    private void setMesMaintenanceRecordMain(MesMaintenanceRecordMain mesMaintenanceRecordMain, String tableNameCode) {
        mesMaintenanceRecordMain.setAccountingMethod("1、正常 √   2、异常 X    3、无此项 -");//计入方式
        if ("mounter_spot_check".equals(tableNameCode) ||
                "reflow_spot_check".equals(tableNameCode)) {//贴片机班前点检表  回流焊班前点检表
            mesMaintenanceRecordMain.setInspectionMethod("先看、测试、听");//检查方法
            mesMaintenanceRecordMain.setInspectionCycle("每天");//检查周期
            mesMaintenanceRecordMain.setBeCareful("1、每日开机前必须实际正确操作点检  2、操作员必须按标准作业指导书作业");//注意事项
            mesMaintenanceRecordMain.setTableTypeCode("spot_inspection_record");//表类型编码
            if (ObjectHelper.isEmpty(mesMaintenanceRecordMain.getDocumentNumber())) {
                if ("mounter_spot_check".equals(tableNameCode)) {
                    mesMaintenanceRecordMain.setDocumentNumber("YZY-EQ018");
                }
                if ("reflow_spot_check".equals(tableNameCode)) {
                    mesMaintenanceRecordMain.setDocumentNumber("YZY-EQ004");
                }
            }
        }
        if ("printing_machine_check".equals(tableNameCode) ||
                "spi_spot_check".equals(tableNameCode) ||
                "aoi_spot_check".equals(tableNameCode) ||
                "reflow_soldering_check".equals(tableNameCode)) {//印刷机日常维护点检表  SPI日常维护点检表  AOI日常维护点检表  回流焊日常点检记录表
            mesMaintenanceRecordMain.setBeCareful("1.日保养由产线作业员上班前进行确认,周保养及月保养由技术员进行定期确认  2.点检注意安全，点检完及时记录并签字确认  3.设备未生产时，做设备表面清洁");//注意事项
            mesMaintenanceRecordMain.setTableTypeCode("spot_inspection_record");//表类型编码
            if (ObjectHelper.isEmpty(mesMaintenanceRecordMain.getDocumentNumber())) {
                if ("printing_machine_check".equals(tableNameCode)) {
                    mesMaintenanceRecordMain.setDocumentNumber("YZY-QR-20");
                }
                if ("spi_spot_check".equals(tableNameCode)) {
                    mesMaintenanceRecordMain.setDocumentNumber("YZY-QR-21");
                }
                if ("aoi_spot_check".equals(tableNameCode)) {
                    mesMaintenanceRecordMain.setDocumentNumber("YZY-QR-25");
                }
                if ("reflow_soldering_check".equals(tableNameCode)) {
                    mesMaintenanceRecordMain.setDocumentNumber("YZY-QR-24");
                }
            }
        }
        if ("rx_7r_spot_check".equals(tableNameCode) ||
                "rs_1_spot_check".equals(tableNameCode)) {//RX-7R贴片机保养记录表  RS-1贴片机保养记录表
            mesMaintenanceRecordMain.setBeCareful("保养过程中如遇不可解决问题请及时联系技术部相关人员");//注意事项
            mesMaintenanceRecordMain.setTableTypeCode("maintenance_record");//表类型编码
            if (ObjectHelper.isEmpty(mesMaintenanceRecordMain.getDocumentNumber())) {
                if ("rx_7r_spot_check".equals(tableNameCode)) {
                    mesMaintenanceRecordMain.setDocumentNumber("YZY-QR-22");
                }
                if ("rs_1_spot_check".equals(tableNameCode)) {
                    mesMaintenanceRecordMain.setDocumentNumber("YZY-QR-23");
                }
            }
        }
        if ("wave_soldering_check".equals(tableNameCode) ||
                "sub_board_machine_check".equals(tableNameCode) ||
                "automatic_sub_board_machine".equals(tableNameCode) ||
                "dvd-rw_check".equals(tableNameCode)) {//波峰焊日常点检记录表  半自动分板机日常点检记录表  全自动分板机日常点检记录表  烧录机日常点检记录表
            mesMaintenanceRecordMain.setBeCareful("良好打√；不符合打X; 停机打Ｏ");//注意事项
            mesMaintenanceRecordMain.setTableTypeCode("spot_inspection_record");//表类型编码
            if (ObjectHelper.isEmpty(mesMaintenanceRecordMain.getDocumentNumber())) {
                if ("sub_board_machine_check".equals(tableNameCode)) {
                    mesMaintenanceRecordMain.setDocumentNumber("YZY-QRED-023");
                }
                if ("automatic_sub_board_machine".equals(tableNameCode)) {
                    mesMaintenanceRecordMain.setDocumentNumber("YZY-QRED-024");
                }
                if ("rw_check".equals(tableNameCode)) {
                    mesMaintenanceRecordMain.setDocumentNumber("YZY-QRED-025");
                }
            }
        }
        if ("soldering_iron_check".equals(tableNameCode)) {//烙铁温度点检表
            mesMaintenanceRecordMain.setBeCareful("1.每天点检，合格打“√” 不合格打“X”；2.烙铁温度设定标准：310-350°C；3.点检异常需要立即修正（维修或更换）");//注意事项
            mesMaintenanceRecordMain.setTableTypeCode("spot_inspection_record");//表类型编码
            if (ObjectHelper.isEmpty(mesMaintenanceRecordMain.getDocumentNumber())) {
                mesMaintenanceRecordMain.setDocumentNumber("QR-012");
            }
        }
        if ("tin_furnace_check".equals(tableNameCode)) {//锡炉温度点检表
            mesMaintenanceRecordMain.setBeCareful("1.小锡炉每天点检一次；2.小锡炉温度点检需写上实际温度，温度标准参考该机种对应的SOP要求，不符合要求需校正到范围内，并备注校正OK；3.小锡炉点检合格打“√”、不合格“×”; 4.更换机型，温度设定需按照SOP要求及时点检校正并备注。");//注意事项
            mesMaintenanceRecordMain.setTableTypeCode("spot_inspection_record");//表类型编码
            if (ObjectHelper.isEmpty(mesMaintenanceRecordMain.getDocumentNumber())) {
                mesMaintenanceRecordMain.setDocumentNumber("YZY-QRQC-132");
            }
        }
    }

    /**
     * 检查是否存在主表信息
     *
     * @param tableNameCode
     * @param inspectionDate
     * @return
     */
    private MesMaintenanceRecordMain checkMaintenanceRecordMain(String tableNameCode, String inspectionDate) {
        QueryWrapper<MesMaintenanceRecordMain> queryWrapperMain = new QueryWrapper<>();
        queryWrapperMain.eq("table_name_code", tableNameCode);
        queryWrapperMain.eq("inspection_date", inspectionDate);
        List<MesMaintenanceRecordMain> MesMaintenanceRecordMains = mesMaintenanceRecordMainMapper.selectList(queryWrapperMain);
        if (ObjectHelper.isEmpty(MesMaintenanceRecordMains)) {
            return null;
        }
        return MesMaintenanceRecordMains.get(0);
    }


    /**
     * 设置子表数据
     */
    public void setMesMaintenanceRecordItem(MesMaintenanceRecordItem mesMaintenanceRecordItem, MesMaintenanceRecordMain mesMaintenanceRecordMain) {
        mesMaintenanceRecordItem.setMainId(mesMaintenanceRecordMain.getId());//主表ID
        mesMaintenanceRecordItem.setTableTypeCode(mesMaintenanceRecordMain.getTableTypeCode());//表类型编码
        mesMaintenanceRecordItem.setTableType(mesMaintenanceRecordMain.getTableType());//表类型名称
        mesMaintenanceRecordItem.setTableNameCode(mesMaintenanceRecordMain.getTableNameCode());//表名编码
        mesMaintenanceRecordItem.setTableName(mesMaintenanceRecordMain.getTableName());//表名
        mesMaintenanceRecordItem.setInspectionDate(mesMaintenanceRecordMain.getInspectionDate());//检查日期
        mesMaintenanceRecordItem.setLineType(mesMaintenanceRecordMain.getLineType());//线别
        mesMaintenanceRecordItem.setEquipmentName(mesMaintenanceRecordMain.getEquipmentName());//设备名称/检查项目
        mesMaintenanceRecordItem.setEquipmentModel(mesMaintenanceRecordMain.getEquipmentModel());//设备型号/检查位置
        mesMaintenanceRecordItem.setEquipmentSn(mesMaintenanceRecordMain.getEquipmentSn());//设备SN
        mesMaintenanceRecordItem.setDatePurchase(mesMaintenanceRecordMain.getDatePurchase());//购置日期/启动检查时间
        mesMaintenanceRecordItem.setDocumentNumber(mesMaintenanceRecordMain.getDocumentNumber());//资产编码/文件编号
        mesMaintenanceRecordItem.setInspectionMethod(mesMaintenanceRecordMain.getInspectionMethod());//检查方法
        mesMaintenanceRecordItem.setInspectionCycle(mesMaintenanceRecordMain.getInspectionCycle());//检查周期
        mesMaintenanceRecordItem.setAccountingMethod(mesMaintenanceRecordMain.getAccountingMethod());//计入方式
        mesMaintenanceRecordItem.setBeCareful(mesMaintenanceRecordMain.getBeCareful());//注意事项/备注
    }
}
