package org.jeecg.modules.mes.produce.service;

import org.jeecg.modules.mes.produce.entity.TurnProduceRecord;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 转产记录表
 * @Author: jeecg-boot
 * @Date:   2021-04-21
 * @Version: V1.0
 */
public interface ITurnProduceRecordService extends IService<TurnProduceRecord> {

}
