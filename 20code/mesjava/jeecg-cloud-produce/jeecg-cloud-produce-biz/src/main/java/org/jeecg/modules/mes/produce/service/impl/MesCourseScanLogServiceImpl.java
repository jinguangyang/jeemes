package org.jeecg.modules.mes.produce.service.impl;

import org.jeecg.modules.mes.produce.entity.MesCourseScanLog;
import org.jeecg.modules.mes.produce.mapper.MesCourseScanLogMapper;
import org.jeecg.modules.mes.produce.service.IMesCourseScanLogService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 物料追踪记录
 * @Author: jeecg-boot
 * @Date:   2021-03-16
 * @Version: V1.0
 */
@Service
public class MesCourseScanLogServiceImpl extends ServiceImpl<MesCourseScanLogMapper, MesCourseScanLog> implements IMesCourseScanLogService {

}
