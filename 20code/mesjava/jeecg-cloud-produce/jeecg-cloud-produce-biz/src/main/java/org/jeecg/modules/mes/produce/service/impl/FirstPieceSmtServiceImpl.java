package org.jeecg.modules.mes.produce.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.jeecg.modules.mes.produce.entity.FirstPieceSmt;
import org.jeecg.modules.mes.produce.entity.FirstPieceSmtLog;
import org.jeecg.modules.mes.produce.entity.MesCommandbillInfo;
import org.jeecg.modules.mes.produce.mapper.FirstPieceSmtLogMapper;
import org.jeecg.modules.mes.produce.mapper.FirstPieceSmtMapper;
import org.jeecg.modules.mes.produce.mapper.MesCommandbillInfoMapper;
import org.jeecg.modules.mes.produce.service.IFirstPieceSmtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Description: SMT首件确认表
 * @Author: jeecg-boot
 * @Date:   2021-03-22
 * @Version: V1.0
 */
@Service
public class FirstPieceSmtServiceImpl extends ServiceImpl<FirstPieceSmtMapper, FirstPieceSmt> implements IFirstPieceSmtService {

    @Autowired
    private FirstPieceSmtMapper firstPieceSmtMapper;
    @Autowired
    private FirstPieceSmtLogMapper firstPieceSmtLogMapper;
    @Autowired
    private MesCommandbillInfoMapper mesCommandbillInfoMapper;

    @Transactional
    public void saveMain(FirstPieceSmt firstPieceSmt){
        //修改制令单的首件状态 productNum
        QueryWrapper<MesCommandbillInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("commandbill_code",firstPieceSmt.getProductNum());
        MesCommandbillInfo mesCommandbillInfo = mesCommandbillInfoMapper.selectOne(queryWrapper);
        if(mesCommandbillInfo!=null) {
            mesCommandbillInfo.setFirstState("已完成");
            mesCommandbillInfoMapper.updateById(mesCommandbillInfo);
        }

        firstPieceSmtMapper.insert(firstPieceSmt);
        List<FirstPieceSmtLog> firstPieceSmtlogs=firstPieceSmt.getFirstPieceSmtlogs();
        for(FirstPieceSmtLog firstPieceSmtlog:firstPieceSmtlogs) {
            firstPieceSmtlog.setFirstId(firstPieceSmt.getId());
            firstPieceSmtLogMapper.insert(firstPieceSmtlog);
        }

    }
}
