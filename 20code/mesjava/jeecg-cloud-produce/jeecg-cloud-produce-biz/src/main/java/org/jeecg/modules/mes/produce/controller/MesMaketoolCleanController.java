package org.jeecg.modules.mes.produce.controller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.modules.mes.produce.entity.MesMaketoolClean;
import org.jeecg.modules.mes.produce.service.IMesMaketoolCleanService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 制造中心-制具清洗
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Api(tags="制造中心-制具清洗")
@RestController
@RequestMapping("/produce/mesMaketoolClean")
@Slf4j
public class MesMaketoolCleanController extends JeecgController<MesMaketoolClean, IMesMaketoolCleanService> {
	@Autowired
	private IMesMaketoolCleanService mesMaketoolCleanService;
	
	/**
	 * 分页列表查询
	 *
	 * @param mesMaketoolClean
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "制造中心-制具清洗-分页列表查询")
	@ApiOperation(value="制造中心-制具清洗-分页列表查询", notes="制造中心-制具清洗-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(MesMaketoolClean mesMaketoolClean,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<MesMaketoolClean> queryWrapper = QueryGenerator.initQueryWrapper(mesMaketoolClean, req.getParameterMap());
		Page<MesMaketoolClean> page = new Page<MesMaketoolClean>(pageNo, pageSize);
		IPage<MesMaketoolClean> pageList = mesMaketoolCleanService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param mesMaketoolClean
	 * @return
	 */
	@AutoLog(value = "制造中心-制具清洗-添加")
	@ApiOperation(value="制造中心-制具清洗-添加", notes="制造中心-制具清洗-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody MesMaketoolClean mesMaketoolClean) {
		mesMaketoolCleanService.save(mesMaketoolClean);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param mesMaketoolClean
	 * @return
	 */
	@AutoLog(value = "制造中心-制具清洗-编辑")
	@ApiOperation(value="制造中心-制具清洗-编辑", notes="制造中心-制具清洗-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody MesMaketoolClean mesMaketoolClean) {
		mesMaketoolCleanService.updateById(mesMaketoolClean);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "制造中心-制具清洗-通过id删除")
	@ApiOperation(value="制造中心-制具清洗-通过id删除", notes="制造中心-制具清洗-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		mesMaketoolCleanService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "制造中心-制具清洗-批量删除")
	@ApiOperation(value="制造中心-制具清洗-批量删除", notes="制造中心-制具清洗-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.mesMaketoolCleanService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "制造中心-制具清洗-通过id查询")
	@ApiOperation(value="制造中心-制具清洗-通过id查询", notes="制造中心-制具清洗-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		MesMaketoolClean mesMaketoolClean = mesMaketoolCleanService.getById(id);
		if(mesMaketoolClean==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(mesMaketoolClean);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param mesMaketoolClean
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, MesMaketoolClean mesMaketoolClean) {
        return super.exportXls(request, mesMaketoolClean, MesMaketoolClean.class, "制造中心-制具清洗");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, MesMaketoolClean.class);
    }

}
