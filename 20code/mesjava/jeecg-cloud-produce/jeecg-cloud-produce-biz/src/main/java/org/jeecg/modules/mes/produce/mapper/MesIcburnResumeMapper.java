package org.jeecg.modules.mes.produce.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.produce.entity.MesIcburnResume;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 制造中心-IC烧录
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
public interface MesIcburnResumeMapper extends BaseMapper<MesIcburnResume> {

}
