package org.jeecg.modules.mes.client;

import org.jeecg.cloud.config.FeignConfig;
import org.jeecg.common.constant.ServiceNameConstants;
import org.jeecg.modules.mes.storage.entity.MesMaterielOccupy;
import org.jeecg.modules.mes.storage.entity.MesStockManage;
import org.jeecg.modules.mes.storage.entity.MesStorageWholesale;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Component
@FeignClient(contextId = "ProduceStockServiceClient", value = ServiceNameConstants.WAREHOUSE_SERVICE,configuration = FeignConfig.class)
public interface StockClient {

    @GetMapping("storage/mesStockManage/queryByMcode")
    public MesStockManage queryByMcode(@RequestParam(name = "mcode", required = true) String mcode);

    /**
     * 根据物料料号，增加库存数量
     * @param mcode
     * @return
     */
    @GetMapping("storage/mesStockManage/updateMcodeNum")
    public boolean updateMcodeNum(@RequestParam(name = "mcode", required = true) String mcode,
                                         @RequestParam(name = "num", required = true) String num);

    @GetMapping("storage/mesStorageWholesale/findById")
    public MesStorageWholesale findById(@RequestParam(name = "id", required = true) String id);

    @GetMapping("storage/mesStorageWholesale/findbaseCode")
    public List<MesStorageWholesale> baseCodefind(@RequestParam(name="baseCode") String baseCode);

    @GetMapping("storage/mesStorageWholesale/selectQuery4SL")
    public List<MesStorageWholesale> selectQuery4SL(@RequestParam(name="query4") String query4);

    @PostMapping("storage/mesStorageWholesale/scanCourseadd")
    public boolean scanCourseadd(@RequestBody MesStorageWholesale mesStorageWholesale);

    /**
     * 根据query4查询该制令单的上料记录 并根据原有上料记录去转产数量记录
     * @param query4 原制令单id
     * @param synum 已使用数量
     * @param mcode 物料料号
     * @param basecode 转产的制令单bom的id
     * @return
     */
    @GetMapping("storage/mesStorageWholesale/updateCommandbill")
    public String updateCommandbill(@RequestParam(name="query4",required=true) String query4,
                                    @RequestParam(name="synum",required=true) String synum,
                                    @RequestParam(name="mcode",required=true) String mcode,
                                    @RequestParam(name="basecode",required=true) String basecode,
                                    @RequestParam(name="newid",required=true) String newid);

    /**
     * 根据生产订单id查询领料清单  远程调用
     * @param orderId
     * @return
     */

    @GetMapping(value = "storage/mesMaterielOccupy/MoccupyByOrderId")
    public List<MesMaterielOccupy> MoccupyByOrderId(@RequestParam(name="orderId",required=true) String orderId);

    /**
     * 根据生产订单id和物料料号查询领料清单  远程调用
     * @param orderId
     * @param mCode
     * @return
     */

    @GetMapping(value = "storage/mesMaterielOccupy/queryByIdAndmCode")
    public MesMaterielOccupy queryByIdAndmCode(@RequestParam(name="orderId",required=true) String orderId,
                                               @RequestParam(name="mCode",required=true) String mCode);
    /**
     * 根据生产订单id和物料料号查询领料清单 并且更新领料表的转产数量  远程调用
     * @param orderId
     * @param mCode
     * @param realNum 转产数量
     * @return
     */
    @GetMapping(value = "storage/mesMaterielOccupy/queryByIdAndmCodeUpdateTransformNum")
    public MesMaterielOccupy queryByIdAndmCodeUpdateTransformNum(@RequestParam(name="orderId",required=true) String orderId,
                                               @RequestParam(name="mCode",required=true) String mCode,
                                               @RequestParam(name="realNum",required=true) String realNum);
    /**
     * 根据生产订单id和物料料号查询领料清单 并且更新领料表的转产数量  远程调用
     * @param orderId
     * @param mCode
     * @param sendNum  发料数量
     * @return
     */
    @GetMapping(value = "storage/mesMaterielOccupy/queryByIdAndmCodeUpdateSendNum")
    public MesMaterielOccupy queryByIdAndmCodeUpdateSendNum(@RequestParam(name="orderId",required=true) String orderId,
                                               @RequestParam(name="mCode",required=true) String mCode,
                                               @RequestParam(name="sendNum",required=true) String sendNum);
    /**
     * 根据生产订单id和物料料号查询领料清单 并且更新领料表的转产数量  远程调用
     * @param orderId
     * @param mCode
     * @param withdrawNum 退料数量
     * @return
     */
    @GetMapping(value = "storage/mesMaterielOccupy/queryByIdAndmCodeUpdatewithdrawNum")
    public MesMaterielOccupy queryByIdAndmCodeUpdatewithdrawNum(@RequestParam(name="orderId",required=true) String orderId,
                                               @RequestParam(name="mCode",required=true) String mCode,
                                               @RequestParam(name="withdrawNum",required=true) String withdrawNum);

}
