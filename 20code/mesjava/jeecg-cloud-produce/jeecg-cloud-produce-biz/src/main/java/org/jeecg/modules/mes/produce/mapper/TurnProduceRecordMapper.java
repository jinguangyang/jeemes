package org.jeecg.modules.mes.produce.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.produce.entity.TurnProduceRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 转产记录表
 * @Author: jeecg-boot
 * @Date:   2021-04-21
 * @Version: V1.0
 */
public interface TurnProduceRecordMapper extends BaseMapper<TurnProduceRecord> {

}
