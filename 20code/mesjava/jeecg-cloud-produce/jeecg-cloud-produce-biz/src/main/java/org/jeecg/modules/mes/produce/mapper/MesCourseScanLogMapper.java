package org.jeecg.modules.mes.produce.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.mes.produce.entity.MesCourseScanLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 物料追踪记录
 * @Author: jeecg-boot
 * @Date:   2021-03-16
 * @Version: V1.0
 */
public interface MesCourseScanLogMapper extends BaseMapper<MesCourseScanLog> {

}
