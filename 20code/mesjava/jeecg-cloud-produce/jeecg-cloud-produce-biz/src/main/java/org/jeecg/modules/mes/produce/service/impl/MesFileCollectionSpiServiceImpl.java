package org.jeecg.modules.mes.produce.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.jeecg.modules.mes.produce.entity.MesFileCollectionSpi;
import org.jeecg.modules.mes.produce.entity.MesFileCollectionSpiDetail;
import org.jeecg.modules.mes.produce.mapper.MesFileCollectionSpiMapper;
import org.jeecg.modules.mes.produce.service.IMesFileCollectionSpiDetailService;
import org.jeecg.modules.mes.produce.service.IMesFileCollectionSpiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description: mes_file_collection_spi
 * @Author: jeecg-boot
 * @Date:   2021-04-16
 * @Version: V1.0
 */
@Service
public class MesFileCollectionSpiServiceImpl extends ServiceImpl<MesFileCollectionSpiMapper, MesFileCollectionSpi> implements IMesFileCollectionSpiService {


    @Autowired
    private IMesFileCollectionSpiDetailService mesFileCollectionSpiDetailService;

    private static HashMap<String,String> BAD_TYPE = new HashMap<>();

    {
        BAD_TYPE.put("Excessice","多锡");
        BAD_TYPE.put("Insufficient","少锡");
        BAD_TYPE.put("Upward Area","面积超出");
        BAD_TYPE.put("Under Area","面积不足");
        BAD_TYPE.put("Bare","无锡");
        BAD_TYPE.put("BRIDGE","连锡");
        BAD_TYPE.put("OffsetX","X偏移");
        BAD_TYPE.put("OffsetY","Y偏移");
        BAD_TYPE.put("Pull","拉尖");
        BAD_TYPE.put("Shape","异性");
        BAD_TYPE.put("Under Height","高度不足");
        BAD_TYPE.put("Upward Height","高度超出");
        BAD_TYPE.put("GoldTab","金手指不良");
        BAD_TYPE.put("Subsidence","塌陷");
        BAD_TYPE.put("Warning","警告");
    }

    @Override
    public void receiveSpiFile(HttpServletRequest request,String line) {
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
        for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
            MultipartFile file = entity.getValue();
            try {
               readSpiFile(file,line);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    @Transactional
    public  void readSpiFile (MultipartFile file,String machineLine) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(file.getInputStream()));
        List<String> lineList = new ArrayList<>();

        while (true) {
            String line = null;
            MesFileCollectionSpi mesFileCollectionSpi = new MesFileCollectionSpi();
            mesFileCollectionSpi.setLine(machineLine);
            int count  = 1;
            List<MesFileCollectionSpiDetail> mesFileCollectionSpiDetailList = new ArrayList<>();
            while (StringUtils.isNotEmpty(line = bufferedReader.readLine())) {
                if (count == 1) {
                    mesFileCollectionSpi.setMachineName(line);
                }
                if (count == 2) {
                    mesFileCollectionSpi.setCode(line);
                }
                if (count == 3) {
                    mesFileCollectionSpi.setGroupLine(line);
                }
                if (count == 4) {
                    mesFileCollectionSpi.setNoMean(line);
                }
                if (count == 5) {
                    mesFileCollectionSpi.setMachineType(line);
                }
                if (count == 6) {
                    mesFileCollectionSpi.setWorkOrder(line);
                }
                if (count == 7) {
                    mesFileCollectionSpi.setTestTimeDay(line);
                }
                if (count == 8) {
                    mesFileCollectionSpi.setTestTimeHour(line);
                }
                if (count == 9) {
                    mesFileCollectionSpi.setTestResult(line);
                }
                if (count == 10) {
                    mesFileCollectionSpi.setTbFace(line);
                }
                if (count == 11) {
                    mesFileCollectionSpi.setTestNum(line);
                }
                if (count == 12) {
                    mesFileCollectionSpi.setBadNum(line);
                }
                if (count > 12) {
                    MesFileCollectionSpiDetail mesFileCollectionSpiDetail = new MesFileCollectionSpiDetail();
                    String[] strings = line.split(";");
                    mesFileCollectionSpiDetail.setBadLine(line);
                    String badStr = strings[3];
                    String[] badArray = badStr.split(",");
                    if (badArray.length > 0) {
                        StringBuilder builder = new StringBuilder("");
                        for (String type : badArray) {
                            builder.append(BAD_TYPE.get(type)).append(",");
                        }
                        mesFileCollectionSpiDetail.setBadType(builder.substring(0,builder.lastIndexOf(",")));
                    }
                    mesFileCollectionSpiDetailList.add(mesFileCollectionSpiDetail);
                }

              count++;
            }
            if (StringUtils.isNotEmpty(mesFileCollectionSpi.getMachineName())) {
                this.save(mesFileCollectionSpi);
            }
            if (mesFileCollectionSpiDetailList.size() > 0) {
                for (MesFileCollectionSpiDetail mesFileCollectionSpiDetail : mesFileCollectionSpiDetailList) {
                    mesFileCollectionSpiDetail.setMainId(mesFileCollectionSpi.getId());
                }
                mesFileCollectionSpiDetailService.saveBatch(mesFileCollectionSpiDetailList);
            }

            if (line == null) {
                break;
            }

        }
    }


}
