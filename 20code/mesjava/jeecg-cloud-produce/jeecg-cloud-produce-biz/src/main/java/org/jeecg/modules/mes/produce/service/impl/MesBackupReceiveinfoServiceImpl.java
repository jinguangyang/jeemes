package org.jeecg.modules.mes.produce.service.impl;

import org.jeecg.modules.mes.produce.entity.MesBackupReceiveinfo;
import org.jeecg.modules.mes.produce.entity.MesBackupReceiveitem;
import org.jeecg.modules.mes.produce.mapper.MesBackupReceiveitemMapper;
import org.jeecg.modules.mes.produce.mapper.MesBackupReceiveinfoMapper;
import org.jeecg.modules.mes.produce.service.IMesBackupReceiveinfoService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Collection;

/**
 * @Description: 备品领用-基本信息
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Service
public class MesBackupReceiveinfoServiceImpl extends ServiceImpl<MesBackupReceiveinfoMapper, MesBackupReceiveinfo> implements IMesBackupReceiveinfoService {

	@Autowired
	private MesBackupReceiveinfoMapper mesBackupReceiveinfoMapper;
	@Autowired
	private MesBackupReceiveitemMapper mesBackupReceiveitemMapper;
	
	@Override
	@Transactional
	public void saveMain(MesBackupReceiveinfo mesBackupReceiveinfo, List<MesBackupReceiveitem> mesBackupReceiveitemList) {
		mesBackupReceiveinfoMapper.insert(mesBackupReceiveinfo);
		if(mesBackupReceiveitemList!=null && mesBackupReceiveitemList.size()>0) {
			for(MesBackupReceiveitem entity:mesBackupReceiveitemList) {
				//外键设置
				entity.setBackupId(mesBackupReceiveinfo.getId());
				mesBackupReceiveitemMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void updateMain(MesBackupReceiveinfo mesBackupReceiveinfo,List<MesBackupReceiveitem> mesBackupReceiveitemList) {
		mesBackupReceiveinfoMapper.updateById(mesBackupReceiveinfo);
		
		//1.先删除子表数据
		mesBackupReceiveitemMapper.deleteByMainId(mesBackupReceiveinfo.getId());
		
		//2.子表数据重新插入
		if(mesBackupReceiveitemList!=null && mesBackupReceiveitemList.size()>0) {
			for(MesBackupReceiveitem entity:mesBackupReceiveitemList) {
				//外键设置
				entity.setBackupId(mesBackupReceiveinfo.getId());
				mesBackupReceiveitemMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void delMain(String id) {
		mesBackupReceiveitemMapper.deleteByMainId(id);
		mesBackupReceiveinfoMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			mesBackupReceiveitemMapper.deleteByMainId(id.toString());
			mesBackupReceiveinfoMapper.deleteById(id);
		}
	}
	
}
