package org.jeecg.modules.mes.produce.service.impl;

import org.jeecg.modules.mes.produce.entity.MesInvalidTime;
import org.jeecg.modules.mes.produce.mapper.MesInvalidTimeMapper;
import org.jeecg.modules.mes.produce.service.IMesInvalidTimeService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 制造中心-无效时间
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Service
public class MesInvalidTimeServiceImpl extends ServiceImpl<MesInvalidTimeMapper, MesInvalidTime> implements IMesInvalidTimeService {

}
