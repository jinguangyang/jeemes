package org.jeecg.modules.mes.produce.service;

import org.jeecg.modules.mes.produce.entity.MesProduceInstructitem;
import org.jeecg.modules.mes.produce.entity.MesProduceInstructinfo;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.beans.factory.annotation.Autowired;
import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * @Description: 生产指示单-基本信息
 * @Author: jeecg-boot
 * @Date:   2020-10-19
 * @Version: V1.0
 */
public interface IMesProduceInstructinfoService extends IService<MesProduceInstructinfo> {

	/**
	 * 删除一对多
	 */
	public void delMain (String id);
	
	/**
	 * 批量删除一对多
	 */
	public void delBatchMain (Collection<? extends Serializable> idList);


}
