package org.jeecg.modules.mes.produce.service;

import org.jeecg.modules.mes.produce.entity.FirstPieceSmt;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.mes.produce.entity.FirstPieceSmtLog;

import java.util.List;

/**
 * @Description: SMT首件确认表
 * @Author: jeecg-boot
 * @Date:   2021-03-22
 * @Version: V1.0
 */
public interface IFirstPieceSmtService extends IService<FirstPieceSmt> {

    public void saveMain(FirstPieceSmt firstPieceSmt) ;

}
