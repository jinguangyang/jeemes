package org.jeecg.modules.mes.produce.service;

import org.jeecg.modules.mes.produce.entity.MesMaketoolClean;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 制造中心-制具清洗
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
public interface IMesMaketoolCleanService extends IService<MesMaketoolClean> {

}
