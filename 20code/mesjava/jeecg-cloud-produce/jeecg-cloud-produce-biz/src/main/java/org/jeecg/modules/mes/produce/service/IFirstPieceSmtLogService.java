package org.jeecg.modules.mes.produce.service;

import org.jeecg.modules.mes.produce.entity.FirstPieceSmtLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: SMT首件确认表-检查内容
 * @Author: jeecg-boot
 * @Date:   2021-03-22
 * @Version: V1.0
 */
public interface IFirstPieceSmtLogService extends IService<FirstPieceSmtLog> {

}
