package org.jeecg.modules.mes.produce.service.impl;

import org.jeecg.modules.mes.produce.entity.MesManytoolMaintaininfo;
import org.jeecg.modules.mes.produce.entity.MesManytoolInfo;
import org.jeecg.modules.mes.produce.mapper.MesManytoolInfoMapper;
import org.jeecg.modules.mes.produce.mapper.MesManytoolMaintaininfoMapper;
import org.jeecg.modules.mes.produce.service.IMesManytoolMaintaininfoService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.Collection;

/**
 * @Description: 多个制具维保-保养信息
 * @Author: jeecg-boot
 * @Date:   2020-10-13
 * @Version: V1.0
 */
@Service
public class MesManytoolMaintaininfoServiceImpl extends ServiceImpl<MesManytoolMaintaininfoMapper, MesManytoolMaintaininfo> implements IMesManytoolMaintaininfoService {

	@Autowired
	private MesManytoolMaintaininfoMapper mesManytoolMaintaininfoMapper;
	@Autowired
	private MesManytoolInfoMapper mesManytoolInfoMapper;
	
	@Override
	@Transactional
	public void saveMain(MesManytoolMaintaininfo mesManytoolMaintaininfo, List<MesManytoolInfo> mesManytoolInfoList) {
		mesManytoolMaintaininfoMapper.insert(mesManytoolMaintaininfo);
		if(mesManytoolInfoList!=null && mesManytoolInfoList.size()>0) {
			for(MesManytoolInfo entity:mesManytoolInfoList) {
				//外键设置
				entity.setManytoolId(mesManytoolMaintaininfo.getId());
				mesManytoolInfoMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void updateMain(MesManytoolMaintaininfo mesManytoolMaintaininfo,List<MesManytoolInfo> mesManytoolInfoList) {
		mesManytoolMaintaininfoMapper.updateById(mesManytoolMaintaininfo);
		
		//1.先删除子表数据
		mesManytoolInfoMapper.deleteByMainId(mesManytoolMaintaininfo.getId());
		
		//2.子表数据重新插入
		if(mesManytoolInfoList!=null && mesManytoolInfoList.size()>0) {
			for(MesManytoolInfo entity:mesManytoolInfoList) {
				//外键设置
				entity.setManytoolId(mesManytoolMaintaininfo.getId());
				mesManytoolInfoMapper.insert(entity);
			}
		}
	}

	@Override
	@Transactional
	public void delMain(String id) {
		mesManytoolInfoMapper.deleteByMainId(id);
		mesManytoolMaintaininfoMapper.deleteById(id);
	}

	@Override
	@Transactional
	public void delBatchMain(Collection<? extends Serializable> idList) {
		for(Serializable id:idList) {
			mesManytoolInfoMapper.deleteByMainId(id.toString());
			mesManytoolMaintaininfoMapper.deleteById(id);
		}
	}
	
}
