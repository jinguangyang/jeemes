package org.jeecg.modules.mes.produce.service;

import org.jeecg.modules.mes.produce.entity.FirstPieceDipLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: DIP首件确认表-检查内容
 * @Author: jeecg-boot
 * @Date:   2021-03-23
 * @Version: V1.0
 */
public interface IFirstPieceDipLogService extends IService<FirstPieceDipLog> {

}
