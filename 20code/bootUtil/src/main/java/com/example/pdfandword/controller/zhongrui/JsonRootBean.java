
package com.example.pdfandword.controller.zhongrui;
import java.util.Date;
import java.util.List;


public class JsonRootBean {

    private String CustomerKey;
    private String AppNo;
    private String StoreName;
    private String SettleName;
    private String VinNumber;
    private String ContractNo;
    private String UserName;
    private String UserTel;
    private String CarBrandName;
    private String CarSeriesName;
    private String CarModelName;
    private String PlateNumber;
    private String EngineNumber;
    private String CarType;
    private String UserIdType;
    private String UserIdCard;
    private String UserCityName;
    private String UserAddress;
    private String ProductName;
    private int PayType;
    private String InstallCityName;
    private Date InstallTime;
    private String InstallPerson;
    private List<EquipInfo> EquipInfo;
    public void setCustomerKey(String CustomerKey) {
         this.CustomerKey = CustomerKey;
     }
     public String getCustomerKey() {
         return CustomerKey;
     }

    public void setAppNo(String AppNo) {
         this.AppNo = AppNo;
     }
     public String getAppNo() {
         return AppNo;
     }

    public void setStoreName(String StoreName) {
         this.StoreName = StoreName;
     }
     public String getStoreName() {
         return StoreName;
     }

    public void setSettleName(String SettleName) {
         this.SettleName = SettleName;
     }
     public String getSettleName() {
         return SettleName;
     }

    public void setVinNumber(String VinNumber) {
         this.VinNumber = VinNumber;
     }
     public String getVinNumber() {
         return VinNumber;
     }

    public void setContractNo(String ContractNo) {
         this.ContractNo = ContractNo;
     }
     public String getContractNo() {
         return ContractNo;
     }

    public void setUserName(String UserName) {
         this.UserName = UserName;
     }
     public String getUserName() {
         return UserName;
     }

    public void setUserTel(String UserTel) {
         this.UserTel = UserTel;
     }
     public String getUserTel() {
         return UserTel;
     }

    public void setCarBrandName(String CarBrandName) {
         this.CarBrandName = CarBrandName;
     }
     public String getCarBrandName() {
         return CarBrandName;
     }

    public void setCarSeriesName(String CarSeriesName) {
         this.CarSeriesName = CarSeriesName;
     }
     public String getCarSeriesName() {
         return CarSeriesName;
     }

    public void setCarModelName(String CarModelName) {
         this.CarModelName = CarModelName;
     }
     public String getCarModelName() {
         return CarModelName;
     }

    public void setPlateNumber(String PlateNumber) {
         this.PlateNumber = PlateNumber;
     }
     public String getPlateNumber() {
         return PlateNumber;
     }

    public void setEngineNumber(String EngineNumber) {
         this.EngineNumber = EngineNumber;
     }
     public String getEngineNumber() {
         return EngineNumber;
     }

    public void setCarType(String CarType) {
         this.CarType = CarType;
     }
     public String getCarType() {
         return CarType;
     }

    public void setUserIdType(String UserIdType) {
         this.UserIdType = UserIdType;
     }
     public String getUserIdType() {
         return UserIdType;
     }

    public void setUserIdCard(String UserIdCard) {
         this.UserIdCard = UserIdCard;
     }
     public String getUserIdCard() {
         return UserIdCard;
     }

    public void setUserCityName(String UserCityName) {
         this.UserCityName = UserCityName;
     }
     public String getUserCityName() {
         return UserCityName;
     }

    public void setUserAddress(String UserAddress) {
         this.UserAddress = UserAddress;
     }
     public String getUserAddress() {
         return UserAddress;
     }

    public void setProductName(String ProductName) {
         this.ProductName = ProductName;
     }
     public String getProductName() {
         return ProductName;
     }

    public void setPayType(int PayType) {
         this.PayType = PayType;
     }
     public int getPayType() {
         return PayType;
     }

    public void setInstallCityName(String InstallCityName) {
         this.InstallCityName = InstallCityName;
     }
     public String getInstallCityName() {
         return InstallCityName;
     }

    public void setInstallTime(Date InstallTime) {
         this.InstallTime = InstallTime;
     }
     public Date getInstallTime() {
         return InstallTime;
     }

    public void setInstallPerson(String InstallPerson) {
         this.InstallPerson = InstallPerson;
     }
     public String getInstallPerson() {
         return InstallPerson;
     }

    public void setEquipInfo(List<EquipInfo> EquipInfo) {
         this.EquipInfo = EquipInfo;
     }
     public List<EquipInfo> getEquipInfo() {
         return EquipInfo;
     }

}
