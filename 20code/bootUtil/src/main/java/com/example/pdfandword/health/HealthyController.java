package com.example.pdfandword.health;

import com.example.pdfandword.controller.Result;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Package com.example.pdfandword.health
 * @date 2021/11/29 4:20 下午
 * @description 探针
 */
@RestController()
public class HealthyController {


    @GetMapping("/health")
    public Result<?> health(){
        return Result.ok();
    }
}
