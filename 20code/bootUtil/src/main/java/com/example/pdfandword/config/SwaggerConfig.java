package com.example.pdfandword.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO
 *
 * @author xuhow
 * @version 1.0.0
 * @date 2020/6/10 14:26
 */
@Configuration
public class SwaggerConfig {
    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.example.pdfandword.controller"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("swagger2接口文档")
                .description("大数据接口")
                .version("1.0")
                .build();
    }

//    private List<ResponseMessage> customerResponseMessage() {
//        List<ResponseMessage> list = new ArrayList<>();
//        list.add(new ResponseMessageBuilder().code(200).message("请求成功").build());
//        list.add(new ResponseMessageBuilder().code(201).message("资源创建成功").build());
//        list.add(new ResponseMessageBuilder().code(204).message("服务器成功处理了请求，但不需要返回任何实体内容").build());
//        list.add(new ResponseMessageBuilder().code(400).message("请求失败,具体查看返回业务状态码与对应消息").build());
//        list.add(new ResponseMessageBuilder().code(401).message("请求失败,未经过身份认证").build());
//        list.add(new ResponseMessageBuilder().code(405).message("请求方法不支持").build());
//        list.add(new ResponseMessageBuilder().code(415).message("请求媒体类型不支持").build());
//        list.add(new ResponseMessageBuilder().code(500).message("服务器遇到了一个未曾预料的状况,导致了它无法完成对请求的处理").build());
//        list.add(new ResponseMessageBuilder().code(503).message("服务器当前无法处理请求,这个状况是临时的，并且将在一段时间以后恢复").build());
//        return list;
//    }
}